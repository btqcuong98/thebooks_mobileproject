//
//  ColorTemplate.swift
//  TheBooks
//
//  Created by Mojave on 7/7/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import UIKit

let colorTemplate = ColorTemplate.share
class ColorTemplate: NSObject {
    static let share = ColorTemplate()
    
    //MARK: - CHECK MODE
    
    let keyDarkLightMode = "keyDarkLightMode"
    
    private var _isDarkMode = true
    var isDarkMode : Bool {
        get {
            return _isDarkMode
        } set {
            _isDarkMode = newValue
            saveDarkLightModeToUserDefaults()
            processDarkLightChange()
            //notification.post(.DarkModeChange)
        }
        
    }
    
    func processDarkLightChange() {
        getDarkLightModeFromUserDefaults()
        defaultColor()
        
        if(_isDarkMode){
            lightColor()
        } else {
            darkColor()
        }
        
        notification.post(.DarkModeChange)
    }
    
    func getBackgroundDarkLight() -> String{
        if(_isDarkMode) {
            return "background_dark"
        } else {
            return "background_light"
        }
    }
    
    func getImageDarkLight(_ value : String) -> UIImage {
        var result = value
        if(_isDarkMode) {
            result += "_dark"
        } else {
            result += "_light" 
        }
        
        return result.image()
    }
    
    func saveDarkLightModeToUserDefaults() {
        UserDefaults.standard.set(_isDarkMode, forKey: keyDarkLightMode)
    }
    
    func checkDarkLightModeInUserDefaults() -> Bool {
        return (UserDefaults.standard.object(forKey: keyDarkLightMode) != nil)
    }
    
    func getDarkLightModeFromUserDefaults() {
        if let mode = UserDefaults.standard.value(forKey: keyDarkLightMode) {
            _isDarkMode = mode as! Bool
        } else {
            _isDarkMode = true
        }     
    }
    
    //MARK: - BASIC COLOR
    
    var borderColor = "a6b4c6".hexColor() //xam xanh
    var blackBackground = "191919".hexColor() //den
    var tabbarBackground = "2e2f33".hexColor() //xanh den dam
    var greenColor = "00FF00".hexColor() //xanh la
    var blueColor = "0B5A9D".hexColor() //xanh duong
    var redColor = "ff0000".hexColor() //do
    var whiteColor = "ffffff".hexColor()
    var yellowColor = "FFFF00".hexColor() //vàng
    var backgroundColCell = "e6e6e6".hexColor() //xams trang
    var purpleBlackColor = "000037".hexColor()
    
    //MARK: - CHANGE COLOR
    
    var tintTabbarColor = "880000".hexColor() //đỏ đô
    var unSelectTabbarColor = "0B5A9D".hexColor() //xanh dương
    var basketBackground = "d3d3d3".hexColor() //màu trắng xám
    var lineColor = UIColor.RGBAlp(red: 255, green: 255, blue: 255, alp: 0.12)
    var textColor = "ffffff".hexColor()
    var subtextColor = UIColor.RGBAlp(red: 0, green: 0, blue: 55, alp: 0.8)
    
    //MARK: - DEFAULT COLOR
    
    func defaultColor() {
        tabbarBackground = "2e2f33".hexColor()
        tintTabbarColor = "880000".hexColor()
        unSelectTabbarColor = "0B5A9D".hexColor()
        basketBackground = "d3d3d3".hexColor() //màu trắng xám
        lineColor = UIColor.RGBAlp(red: 255, green: 255, blue: 255, alp: 0.12)
        textColor = "ffffff".hexColor()
        subtextColor = UIColor.RGBAlp(red: 0, green: 0, blue: 55, alp: 0.8)
        backgroundColCell = "e6e6e6".hexColor()
    }
    
    //MARK: - DARK COLOR
    
    func darkColor() {
        
        //cùng màu sáng với background
    
        tabbarBackground = "d3d3d3".hexColor() //màu trắng xám
        backgroundColCell = "e6e6e6".hexColor() //xam sang
        
        //màu tối theo chức năng
        
        tintTabbarColor = "880000".hexColor() //đỏ đô
        unSelectTabbarColor = "0B5A9D".hexColor() //màu xanh đậm
        basketBackground = "0B5A9D".hexColor()
        lineColor = UIColor.RGBAlp(red: 0, green: 0, blue: 0, alp: 0.12)
        textColor = "000037".hexColor()
        subtextColor = UIColor.RGBAlp(red: 0, green: 0, blue: 55, alp: 0.8)
        
    }
    
    //MARK: - LIGHT COLOR
    
    func lightColor() {
        
        //cùng màu tối với background
        
        tabbarBackground = "24005e".hexColor()
        backgroundColCell = "180439".hexColor() //
        
        
        //màu sáng theo chức năng
        
        tintTabbarColor = "FFFF00".hexColor() //màu vàng
        unSelectTabbarColor = "ffffff".hexColor() //màu trắng
        basketBackground = "d3d3d3".hexColor() //màu trắng xám
        lineColor = UIColor.RGBAlp(red: 255, green: 255, blue: 255, alp: 0.12)
        textColor = "ffffff".hexColor()
        subtextColor = "a6b4c6".hexColor()
        
    }
    
    //MARK: - FONT/SIZE
    
    var size12 : CGFloat = 12.0
    var size16 : CGFloat = 16.0
    var size18 : CGFloat = 18.0
    var size20 : CGFloat = 20.0

    var size12Font = UIFont.systemFont(ofSize: 12.0)
    var size16Font = UIFont.systemFont(ofSize: 16.0)
    var size18Font = UIFont.systemFont(ofSize: 18.0)
    var size20Font = UIFont.systemFont(ofSize: 20.0)

    var boldSize16 = UIFont.systemFont(ofSize: 16.0, weight: .bold)
    var boldSize18 = UIFont.systemFont(ofSize: 18.0, weight: .bold)
    var boldSize20 = UIFont.systemFont(ofSize: 20.0, weight: .bold)
    
}
