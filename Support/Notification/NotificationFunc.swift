//
//  NotificationFunc.swift
//  TheBooks
//
//  Created by Mojave on 7/5/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import Foundation

enum NotifiType : String {
    case LanguageChange = "LanguageChange"
    case DarkModeChange =  "DarkModeChange"
    case LoginSucess = "LoginSucess"
    case LogoutSucess = "LogoutSucess"
    case LeftMenuShow = "LeftMenuShow"
    case ReloadData = "ReloadData"
    case AfterRating = "AfterRating"
}

let notification = NotificationFunc.share
class NotificationFunc: NSObject {
    static let share = NotificationFunc()
    
    func post(_ key : NotifiType) {
        NotificationCenter.default.post(name: Notification.Name(key.rawValue), object: nil)
    }
    
    func add(_ target : Any, _ selector : Selector, _ key : NotifiType) {
        NotificationCenter.default.addObserver(target, selector: selector, name: Notification.Name(key.rawValue), object: nil)
    }
    
    func remove(_ target : Any, _ key : NotifiType) {
        NotificationCenter.default.removeObserver(target, name: Notification.Name(key.rawValue), object: nil)
    }
}
