//
//  LaguageType.swift
//  TheBooks
//
//  Created by Mojave on 7/7/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import Foundation

enum LanguageType : Int{
    case vi = 1
    case en = 2
    case ja = 3
    
    func getLangString() -> String {
        switch self {
        case .vi:
            return "vi"
        case .en:
            return "en"
        case .ja:
            return "ja"
        }
    }
    
    static func getLangType(_ string : String) -> LanguageType {
        switch string {
        case LanguageType.vi.getLangString():
            return .vi
        case LanguageType.en.getLangString():
            return .en
        case LanguageType.ja.getLangString():
            return .ja
        default:
            return .vi
        }
    }
    
    static func getListLang() -> [String] {
        return [LanguageType.vi.getLangString(),
                LanguageType.en.getLangString(),
                LanguageType.ja.getLangString()
               ]
    }
    
    static func defaultLang() -> String {
        return LanguageType.vi.getLangString()
    }
    
    func getLangName() -> String {
        switch self {
        case .vi:
            return "Vietnamese".localized()
        case .en:
            return "English".localized()
        case .ja:
            return "Japanese".localized()
        }
    }
    
    func getLangIconName() -> String {
        switch self {
        case .vi:
            return "vi_flag"
        case .en:
            return "en_flag"
        default :
            return "vi_flag"
        }
    }
    
    func getLocaleLang() -> Locale {
        switch self {
        case .vi:
            return Locale.init(identifier: "vi_VI")
        case .en:
            return Locale.init(identifier: "en_US")
        case .ja:
            return Locale.init(identifier: "ja_JP")
        }
    }
    
}
