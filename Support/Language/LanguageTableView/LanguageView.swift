//
//  LanguageView.swift
//  TheBooks
//
//  Created by Mojave on 7/8/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import UIKit

class LanguageView: SkyView {

    //MARK: - OUTLET
    
    @IBOutlet weak var tbvView : UITableView!
    
    //MARK: - DECLARE
    
    private var lstData : [LanguageType] = [.vi, .en]
    
    //MARK: - INIT
    
    override func initView() {
        super.initView()
        
        register ()
    }

    //MARK: - REGISTER
    
    struct Indentifer {
        static let cell = "LanguageCell"
    }
    
    private func register () {
        tbvView.backgroundColor = colorTemplate.blackBackground
        
        tbvView.register(UINib.init(nibName: Indentifer.cell, bundle: ProjectBundle), forCellReuseIdentifier: Indentifer.cell)
        
        tbvView.delegate = self
        tbvView.dataSource = self
    }
    
    //MARK: - DARK/LIGHT MODE + CHANGE LANGUAGE
    
    override func darkLightMode() {
        super.darkLightMode()
    }
    
    override func changeLanguage() {
        super.changeLanguage()
    }
}

extension LanguageView : UITableViewDataSource {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40.0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return lstData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Indentifer.cell, for: indexPath) as! LanguageCell
        
        cell.setData(lstData[indexPath.row])
        
        return cell
    }
}

extension LanguageView : UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as! LanguageCell
        
        if(cell.type.getLangName() == configLanguage.currentLanguage) {
            self.superview?.removeFromSuperview()
            return
        }
        
        _ = configLanguage.processChangeLanguage(cell.type)
        self.superview?.removeFromSuperview()
    }
}
