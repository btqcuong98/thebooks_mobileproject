//
//  ConfigLanguage.swift
//  TheBooks
//
//  Created by Mojave on 7/7/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import Foundation


let configLanguage = ConfigLanguage.share
class ConfigLanguage: NSObject {
    static let share = ConfigLanguage()
    
    
    //MARK: - DECLEARE
    
    let DeviceLanguage = "DeviceLanguage"
    let keySaveLanguage = "kSaveLanguage"
    var listLanguageAvailble : Array<String>!
    var currentLanguage : String!
    var dictionaryLanguage : Dictionary<String, Any>!
    var defaults = UserDefaults.standard
    
    //MARK: - INIT
    
    override init() {
        super.init()
        
        listLanguageAvailble = LanguageType.getListLang()
        currentLanguage = DeviceLanguage
        
        let languageSaved = defaults.object(forKey: keySaveLanguage)
        
        if let language = languageSaved as? String, language != DeviceLanguage {
            _ = getDictionaryLanguage(newLanguage: language)
        } else {
            _ = getDictionaryLanguage(newLanguage: LanguageType.defaultLang())
        }
        
    }
    
    //MARK: - GET LANGUAGE FROM PATH
    
    private func getDictionaryLanguage(newLanguage : String) -> Bool {
        
        var languageFound = false
        let arrLanguage = newLanguage.components(separatedBy: "_")
        
        (arrLanguage as NSArray?)?.enumerateObjects({ obj, tdx, stop in //.
            let urlPath = Bundle(for: type(of: self)).url(forResource: "Localizable", withExtension: "strings", subdirectory: nil, localization: obj as? String)
            
            if FileManager.default.fileExists(atPath: urlPath?.path ?? "") {
                currentLanguage = newLanguage
                dictionaryLanguage = NSDictionary(contentsOfFile: urlPath?.path ?? "") as? Dictionary<String, Any>
                
                languageFound = true
            }
        })
        
        return languageFound
    }
    
    //MARK: - LOCALIZED
    
    func localized (forkey key: String) -> String {
        if (dictionaryLanguage == nil) {
            return NSLocalizedString(key, comment: key)
        } else {
            var languageString = dictionaryLanguage[key]
            
            if(languageString == nil) {
                languageString = key
            }
            
            return languageString as! String
        }
    }
    
    //MARK: - SET LANGUAGE
    
    func processChangeLanguage(_ type : LanguageType) -> Bool{
        let newLang = type.getLangString()
        
        if(newLang == "" || newLang == currentLanguage || !listLanguageAvailble.contains(newLang)) {
            saveLanguageToUserDefault(true)
            
            return false
        }
        
        if(newLang == DeviceLanguage) {
            currentLanguage = newLang
            dictionaryLanguage = nil
            
            notification.post(.LanguageChange)
            
            saveLanguageToUserDefault(true)
            
            return true
        } else {
            if ( getDictionaryLanguage(newLanguage: newLang) ){
                
                notification.post(.LanguageChange)
                
                if( checkLanguageInUserDefault() ) {
                    saveLanguageToUserDefault(true)
                }
                
                return true
            }
            
            saveLanguageToUserDefault(true)
            return false
        }
    }
    
    private func saveLanguageToUserDefault(_ flag : Bool) {
        if(flag) {
            defaults.set(currentLanguage, forKey: keySaveLanguage)
        } else {
            defaults.removeObject(forKey: keySaveLanguage)
        }
        
        defaults.synchronize()
    }
    
    private func checkLanguageInUserDefault() -> Bool {
        return defaults.object(forKey: keySaveLanguage) != nil
    }
    
    //MARK: - CHECK LANGUAGE
    
    func isVietnamese() -> Bool {
        if(currentLanguage == "vi") {
            return true
        } else {
            return false
        }
    }
    
    //MARK: - GET TYPE LANGUAGE
    
    func getLangType() -> LanguageType{
        return LanguageType.getLangType(currentLanguage)
    }
}
