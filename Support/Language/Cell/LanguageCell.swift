//
//  LanguageCell.swift
//  TheBooks
//
//  Created by Mojave on 7/8/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import UIKit

class LanguageCell: UITableViewCell {

    //MARK: - OULET
    
    @IBOutlet weak var lblText : UILabel!
    @IBOutlet weak var imgImage : UIImageView!
    
    //MARK: - DECLARE
    
    var type : LanguageType!
    
    //MARK: - INIT
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        self.backgroundColor = .clear
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    //MARK: - FUNC
    
    func setData(_ type : LanguageType) {
        self.type = type
        
        if(type.getLangString() == configLanguage.currentLanguage) {
            self.isUserInteractionEnabled = false
            self.contentView.alpha = 0.5
        } else {
            self.isUserInteractionEnabled = true
            self.contentView.alpha = 1.0
        }
        
        lblText.text = type.getLangName()
        imgImage.image = UIImage.init(named: type.getLangIconName())
    }
    
    
}
