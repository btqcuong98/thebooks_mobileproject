//
//  RolesType.swift
//  TheBooks
//
//  Created by Mojave on 11/20/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import Foundation

enum RolesType: Int {
    case docgia = 0
    case nvvc = 1
    
    func getRolesTypeString() -> String{
        switch self {
        case .docgia:
            return "DocGia"
        case .nvvc:
            return "NVVC"
        default:
            return "DocGia"
        }
    }
}
