//
//  DataManager.swift
//  TheBooks
//
//  Created by Mojave on 8/5/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import Foundation
import UIKit

let dataManager = DataManager.shareInstance()
class DataManager: NSObject {
    static var instance : DataManager!
    
    class func shareInstance() -> DataManager {
        if(instance == nil) {
            self.instance = (self.instance ?? DataManager())
        }
        instance.reload()
        return self.instance
    }
    
    //MARK: - INIT
    
    func reload() {
        TheLoai_GetAll()
        DauSach_GetAll()
        DauSach_SelectTop10()
        DocGia_SelectTop10()
        getLogin()
    }
    
    //MARK: - DECLARE
    
    var lstTheLoai : [TheLoai_DTO] = []
    var lstDauSach : [DauSach_DTO] = []
    var lstTop10DauSach : [DauSach_DTO] = []
    var lstTop10DocGia : [Top10DocGia_DTO] = []
    var lstBasket : [DauSach_DTO] = []
    var Basket : [Basket] = []
    var UserLogin : Login_DTO = Login_DTO()
    var loginName = ""
    var password = ""
    var roles = ""
    
    let kSaveLoginName = "kSaveLoginName"
    let kSavePassword = "kSavePassword"
    let kSaveBasket = "kSaveBasket"
    let userdefault = UserDefaults.standard
    
    //MARK: - FUNC

    private func TheLoai_GetAll() {
        let request = TheLoai_Request()
        DispatchQueue.global().async {
            ServiceRequest.theloai_getAll(request: request, success: {(data) in
                self.lstTheLoai = data
            })
        }
    }
    
    private func DauSach_GetAll() {
        let request = DauSach_Request()
        DispatchQueue.global().async {
            ServiceRequest.dausach(request: request, success: { (data) in
                self.lstDauSach = data
            })
        }
    }
    
    private func DauSach_SelectTop10() {
        let request = DauSach_Request()
        DispatchQueue.global().async {
            ServiceRequest.selecttop10ds(request: request, success: { (data) in
                self.lstTop10DauSach = data
            })
        }
    }
    
    private func DocGia_SelectTop10() {
        let request = Top10DocGia_Request()
        DispatchQueue.global().async {
            ServiceRequest.top10dg(request: request, success: { (data) in
                self.lstTop10DocGia = data
            })
        }
    }
    
    //MARK: - USERDEFAULT
    
    //MARK: - check data
    
    public func userDefaultCheck(_ key : String) -> Bool{
        return (userdefault.object(forKey: key) != nil)
    }
    
    //MARK: - set data for login
    
    public func saveLogin() {
        userdefault.set(loginName, forKey: kSaveLoginName)
        userdefault.set(password, forKey: kSavePassword)
    }
    
    //MARK: - get data for login
    
    public func getLogin() {
        if(userDefaultCheck(kSaveLoginName) && userDefaultCheck(kSavePassword)) {
            loginName = userdefault.value(forKey: kSaveLoginName) as! String
            password = userdefault.value(forKey: kSavePassword) as! String
        }
    }
    
    
    //MARK: - BASKET
    
    func getBasketProduct(_ key: String) -> [Basket] {
        if userDefaultCheck(key) {
            if let basketData = userdefault.object(forKey: key) as? NSData {
                let basket = NSKeyedUnarchiver.unarchiveObject(with: basketData as Data) as? [Basket]
                if let basket = basket {
                    return basket
                }
            }
        }
        return []
    }
    
    public func saveBasketProduct(_ lstBasket : [Basket], _ key: String) {
        var lstData = self.getBasketProduct(key)
        if lstData.count > 0 {
            for item in lstBasket {
                lstData.append(item)
            }
        } else {
            lstData = lstBasket
        }
        
        let placesData = NSKeyedArchiver.archivedData(withRootObject: lstData)
        userdefault.set(placesData, forKey: key)
        userdefault.synchronize()
    }
    
    func removeBasketProductLocal(_ key: String) {
        if userDefaultCheck(key) {
            userdefault.removeObject(forKey: key)
            userdefault.synchronize()
        }
    }
    
    //MARK : - CHECK COMMENTED
    
    func saveIsComment(_ keyIsMaDS : String, _ data : Bool) {
        let key = keyIsMaDS + dataManager.UserLogin.MaND
        UserDefaults.standard.set(data, forKey: key)
    }
    
    func checkExitIsCommentKey(_ key: String) -> Bool {
        return (UserDefaults.standard.object(forKey: key) != nil)
    }
    
    func getIsComment(_ keyIsMaDS : String) -> Bool {
        let key = keyIsMaDS + dataManager.UserLogin.MaND
        if(checkExitIsCommentKey(key)) {
            if let isComment = UserDefaults.standard.value(forKey: key) {
                return (isComment as! Bool) //có r
            } else {
                return false //user chưa comment trên ds
            }
        } else {
            return false
        }
    }
    
    //MARK : - AVATA
    
    func saveIsAvata(_ avata: String) {
        let key = dataManager.UserLogin.MaND + "Avata"
        UserDefaults.standard.set(avata, forKey: key)
    }
    
    func checkExitIsAvataKey(_ key : String) -> Bool {
        return (UserDefaults.standard.object(forKey: key) != nil)
    }
    
    func getIsAvata(_ keyGetAvata: String) -> String {
        let key = keyGetAvata + "Avata"
        if(checkExitIsAvataKey(key)) {
            if let avata = UserDefaults.standard.value(forKey: key){
                return (avata as? String ?? "")
            } else {
                return ""
            }
        } else {
            return ""
        }
    }
}
