//
//  AlertView.swift
//  TheBooks
//
//  Created by Mojave on 8/7/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import UIKit

var tvTextStr = ""
var star : [Bool] = [false, false, false, false, false]
let alertView = AlertView.shared
class AlertView: SkyView {

    static let shared = AlertView()
    
    //MARK: - IBOULET
    
    @IBOutlet weak var vContain: UIView!
    @IBOutlet weak var lblTitle: SkyLabelDiscript1!
    @IBOutlet weak var vImageContain: UIView!
    @IBOutlet weak var img1: UIButton!
    @IBOutlet weak var img2: UIButton!
    @IBOutlet weak var img3: UIButton!
    @IBOutlet weak var img4: UIButton!
    @IBOutlet weak var img5: UIButton!
    @IBOutlet weak var tvText: UITextView!
    @IBOutlet weak var lblText: SkyLabelDiscript2!
    @IBOutlet weak var btnCancel: SkyButtonCancel!
    @IBOutlet weak var btnAccept: SkyButton!
    @IBOutlet weak var heightTextView: NSLayoutConstraint!
    @IBOutlet weak var heightLable: NSLayoutConstraint!
    
    //MARK: - DECLARE
    var buttonBlock : ((Int)->Void)!
    
    //MARK: - INIT
    
    override func initView() {
        super.initView()
        changeLanguage()
        darkLightMode()
        vContain.drawBorderRadius(radius: 5.0, color: colorTemplate.lineColor, thickness: 1.5)
    }
    
    init(_ superView : UIView, _ title : String, _ discript : String, _ isAlert: Bool, _ isShowAcept : Bool, _ btnName : [(String, String)], _ indexButton: @escaping ((Int)->Void)) {
        
        super.init(frame: CGRect.zero)
        setUpView(isAlert)
        if(!isShowAcept) {
            btnAccept.hiddenView()
            
            if(btnName.count > 0) {
                self.btnCancel.setTitle(btnName[0].0.uppercased(), for: .normal)
            }
            
            
        } else {
            btnAccept.showView()
            
            if(btnName.count > 0) {
                self.btnCancel.setTitle(btnName[0].0.uppercased(), for: .normal)
                self.btnAccept.setTitle(btnName[0].1.uppercased(), for: .normal)
            }
        }
        
        lblTitle.title = title.uppercased()
        lblText.title = discript
        
        self.buttonBlock = indexButton
        showFrom(superView)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: - REGISTER
    //MARK: - DARK/LIGHT + CHANGLANGUAGE CHANGE
    
    override func darkLightMode() {
        super.darkLightMode()
        vContain.backgroundColor = colorTemplate.backgroundColCell
    }
    
    override func changeLanguage() {
        super.changeLanguage()
        
    }
    
    //MARK: - FUNC
    
    func setUpView(_ isAlert: Bool) {
        if(isAlert) {
            heightTextView.constant = 0
            vImageContain.hiddenView()
            heightLable.constant = 20
            tvText.text = ""
        } else {
            heightTextView.constant = 200
            vImageContain.showView()
            heightLable.constant = 0
            tvText.text = ""
        }
        lblText.textAlignment = .center
    }
    
    func showFrom(_ superView :UIView){
        superView.addSubview(self)
        superView.setAuthenticationLayout(self)
    }
    
    //MARK: - ACTION
    
    @IBAction func cancelTouch(_ sender: UIButton) {
        self.removeFromSuperview()
        self.buttonBlock?(0)
    }
    
    @IBAction func acceptTouch(_ sender: Any) {
        self.removeFromSuperview()
        tvTextStr = tvText.text
        self.buttonBlock?(1)
    }
    
    @IBAction func star1(_ sender: Any) {
        star[0] = !star[0]
        let imgStr = star[0] ? "star_true" : "star_false"
        img1.setImage(UIImage(named: imgStr), for: .normal)
    }
    
    @IBAction func star2(_ sender: Any) {
        if(star[0]) {
            star[1] = !star[1]
            let imgStr = star[1] ? "star_true" : "star_false"
            img2.setImage(UIImage(named: imgStr), for: .normal)
        }
    }
    
    @IBAction func star3(_ sender: Any) {
        if(star[0] && star[1]) {
            star[2] = !star[2]
            let imgStr = star[2] ? "star_true" : "star_false"
            img3.setImage(UIImage(named: imgStr), for: .normal)
        }
    }
    
    @IBAction func star4(_ sender: Any) {
        if(star[0] && star[1] && star[2]) {
            star[3] = !star[3]
            let imgStr = star[3] ? "star_true" : "star_false"
            img4.setImage(UIImage(named: imgStr), for: .normal)
        }
    }
    
    @IBAction func star5(_ sender: Any) {
        if(star[0] && star[1] && star[2] && star[3]) {
            star[4] = !star[4]
            let imgStr = star[4] ? "star_true" : "star_false"
            img5.setImage(UIImage(named: imgStr), for: .normal)
        }
    }
    
    //MARK: - DEINIT
    
    deinit {
        
    }

}

extension UIView {
    
    func errorDialog(_ superViewData: UIView, _ description : String, failure:@escaping ((Int)->Void)) {
        let alert  = AlertView.init(superViewData, AlertView.errorTitle(), description, true, false, []) { (index) in
            failure(index)
        }
        
        alert.btnCancel.setTitle(AlertView.closeBtnTitle(), for: .normal)
    }
    
    func successDialog(_ superViewData: UIView, _ description : String, success : @escaping ((Int) -> Void)) {
        let alert  = AlertView.init(superViewData, AlertView.successTitle(), description, true, false, []) { (index) in
            success(index)
        }
        
        alert.btnCancel.setTitle(AlertView.closeBtnTitle(), for: .normal)
    }
    
    func messageDialog(_ superViewData: UIView,_ description : String,_ block :@escaping ((Int)->Void)) {
        let alert  = AlertView.init(superViewData, AlertView.infoTitle(), description, true, false, []) { (index) in
            block(index)
        }
        alert.btnCancel.setTitle(AlertView.closeBtnTitle(), for: .normal)
    }
    
    func confirmDialog(_ superViewData: UIView,_ title : String, _ description : String, _ btnTitle : [(String, String)], block :@escaping ((Int)->Void)) {
        let _  = AlertView.init(superViewData, title, description, true, true, btnTitle) { (index) in
            block(index)
        }
    }
    
    func commentDialog(_ superViewData: UIView, block :@escaping ((Int)->Void)) {
        alertView.setUpView(false)
        let _  = AlertView.init(superViewData, AlertView.CommentTitle(), "", false, true, [(AlertView.CancelBtnTitle(), AlertView.acceptBtnTitle())]) { (index) in
            block(index)
        }
    }
}

extension UIView {
    func setAuthenticationLayout(_ innerView : UIView) { // full layout for sub view
        innerView.translatesAutoresizingMaskIntoConstraints = false
        self.addConstraint(NSLayoutConstraint(item: innerView, attribute: .top, relatedBy: .equal, toItem: self, attribute: .top, multiplier: 1.0, constant: 0))
        
        self.addConstraint(NSLayoutConstraint(item: innerView, attribute: .bottom, relatedBy: .equal, toItem: self, attribute: .bottom, multiplier: 1.0, constant: 0))
        
        self.addConstraint(NSLayoutConstraint(item: innerView, attribute: .trailing, relatedBy: .equal, toItem: self, attribute: .trailing, multiplier: 1.0, constant: 0))
        
        self.addConstraint(NSLayoutConstraint(item: innerView, attribute: .leading, relatedBy: .equal, toItem: self, attribute: .leading, multiplier: 1.0, constant: 0))
    }
}

extension AlertView {
    fileprivate class func successTitle() -> String {
        let langue = configLanguage.getLangType()
        switch langue {
        case .vi:  return successTitle_vi
        case .en:  return successTitle_en
        default: return infoTitle_en
        }
    }
    
    class func infoTitle() -> String {
        let langue = configLanguage.getLangType()
        switch langue {
        case .vi:  return infoTitle_vi
        case .en:  return infoTitle_en
        default: return infoTitle_en
        }
    }
    
    fileprivate class func errorTitle() -> String {
        let langue = configLanguage.getLangType()
        switch langue {
        case .vi:  return errorTitle_vi
        case .en:  return errorTitle_en
        default: return errorTitle_en
        }
    }
    
    fileprivate class func ConfirmTitle() -> String {
        let langue = configLanguage.getLangType()
        switch langue {
        case .vi:  return confirmTitle_vi
        case .en:  return confirmTitle_en
        default: return confirmTitle_en
        }
    }
    
    fileprivate class func CommentTitle() -> String {
        let langue = configLanguage.getLangType()
        switch langue {
        case .vi:  return commentTitle_vi
        case .en:  return commentTitle_en
        default: return commentTitle_en
        }
    }
    
    // button tilte
    
    class func closeBtnTitle() -> String {
        let langue = configLanguage.getLangType()
        switch langue {
        case .vi:  return Close_vi
        case .en:  return Close_en
        default: return Close_en
        }
    }
    
    class func acceptBtnTitle() -> String {
        let langue = configLanguage.getLangType()
        switch langue {
        case .vi:  return Accept_vi
        case .en:  return Accept_en
        default: return Close_en
        }
    }
    
    fileprivate class func CancelBtnTitle() -> String {
        let langue = configLanguage.getLangType()
        switch langue {
        case .vi:  return Cancel_vi
        case .en:  return Cancel_en
        default: return Cancel_en
        }
    }
    
}


fileprivate let successTitle_vi = "THÀNH CÔNG"
fileprivate let successTitle_en = "SUCCESS"

fileprivate let infoTitle_vi = "THÔNG BÁO"
fileprivate let infoTitle_en = "NOTICE MESSAGE"

fileprivate let errorTitle_vi = "ĐÃ XẢY RA LỖI"
fileprivate let errorTitle_en = "ERROR MESSAGE"

fileprivate let confirmTitle_vi = "XÁC NHẬN ĐĂNG KÝ"
fileprivate let confirmTitle_en = "CONFIRM REGISTER"

fileprivate let commentTitle_vi = "BÌNH LUẬN & ĐÁNH GIÁ"
fileprivate let commentTitle_en = "COMMENT & RATING"

fileprivate let Close_vi = "ĐÓNG"
fileprivate let Close_en = "CLOSE"

fileprivate let Accept_vi = "ĐỒNG Ý"
fileprivate let Accept_en = "ACCEPT"

fileprivate let Cancel_vi = "HUỶ"
fileprivate let Cancel_en = "CANCEL"
