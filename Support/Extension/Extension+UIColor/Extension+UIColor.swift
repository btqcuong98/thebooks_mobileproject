//
//  Extension+UIColor.swift
//  TheBooks
//
//  Created by Mojave on 7/7/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import UIKit

extension UIColor {
    
    class func RGB(red : CGFloat, green : CGFloat, blue : CGFloat) -> UIColor{
        return UIColor.init(red: red/255.0, green: green/255.0, blue: blue/255.0, alpha: 1.0)
    }
    
    class func RGBAlp(red : CGFloat, green : CGFloat, blue : CGFloat, alp : CGFloat) -> UIColor{
        return UIColor.init(red: red/255.0, green: green/255.0, blue: blue/255.0, alpha: alp)
    }
}
