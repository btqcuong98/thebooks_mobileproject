//
//  Extension+UIView.swift
//  TheBooks
//
//  Created by Mojave on 7/5/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import UIKit

extension UIView {

    //MARK: - HIDDEN
    
    func hiddenView() {
        self.isHidden = true
    }
    
    func showView() {
        self.isHidden = false
    }
    
    //MARK: - FULL LAYOUT
    
    func setFullLayout(_ view : UIView) {
        setFixLayout(view, 0, 0, 0, 0)
    }
    
    func setFixLayout(_ view : UIView, _ top : CGFloat, _ right : CGFloat, _ bottom : CGFloat, _ left : CGFloat) {
        
        self.addConstraint(NSLayoutConstraint(item: view, attribute: .top, relatedBy: .equal, toItem: self, attribute: .top, multiplier: 1.0, constant: top))

        self.addConstraint(NSLayoutConstraint(item: view, attribute: .right, relatedBy: .equal, toItem: self, attribute: .right, multiplier: 1.0, constant: right))

        self.addConstraint(NSLayoutConstraint(item: view, attribute: .bottom, relatedBy: .equal, toItem: self, attribute: .bottom, multiplier: 1.0, constant: bottom))

        self.addConstraint(NSLayoutConstraint(item: view, attribute: .left, relatedBy: .equal, toItem: self, attribute: .left, multiplier: 1.0, constant: left))
        
        view.translatesAutoresizingMaskIntoConstraints = false //bất cứ view nào thuộc skyview cũng phải tuân thủ theo sexFixLayout nếu có constraint khác sẽ gây xung đột => code này : cho phép thay đổi constraint sexFixLayout đã được cố định trước đó
    }
    
    //MARK: - RATIO LAYOUT
    
    func set065RatioLayout(_ view : UIView) {
        setRatioFixLayout(view, 0, 0, 0, 0)
    }
    
    func setRatioFixLayout(_ view : UIView, _ top : CGFloat, _ right : CGFloat, _ bottom : CGFloat, _ left : CGFloat) {
        
        self.addConstraint(NSLayoutConstraint(item: view, attribute: .top, relatedBy: .equal, toItem: self, attribute: .top, multiplier: 1.0, constant: top))

        self.addConstraint(NSLayoutConstraint(item: view, attribute: .right, relatedBy: .equal, toItem: self, attribute: .right, multiplier: 0.65, constant: right)) //(1 - right) * UIScreen.main.bounds.size.width

        self.addConstraint(NSLayoutConstraint(item: view, attribute: .bottom, relatedBy: .equal, toItem: self, attribute: .bottom, multiplier: 1.0, constant: bottom))

        self.addConstraint(NSLayoutConstraint(item: view, attribute: .left, relatedBy: .equal, toItem: self, attribute: .left, multiplier: 1.0, constant: left))
        
        view.translatesAutoresizingMaskIntoConstraints = false //bất cứ view nào thuộc skyview cũng phải tuân thủ theo sexFixLayout nếu có constraint khác sẽ gây xung đột => code này : cho phép thay đổi constraint sexFixLayout đã được cố định trước đó
    }
    
    
    //MARK: - SET LAYOUT FOR HEIGHT
    
    func setLayoutForHeight(_ view : UIView) {
        setFixLayoutForHeight(view, 100, 0, -100, 0)
    }
    
    func setFixLayoutForHeight(_ view : UIView, _ height : CGFloat, _ right : CGFloat, _ bottom : CGFloat, _ left : CGFloat) {
        
        self.addConstraint(NSLayoutConstraint(item: view, attribute: .height, relatedBy: .equal, toItem: view, attribute: .height, multiplier: 1.0, constant: height))
        
        self.addConstraint(NSLayoutConstraint(item: view, attribute: .right, relatedBy: .equal, toItem: self, attribute: .right, multiplier: 1.0, constant: right))
        
        self.addConstraint(NSLayoutConstraint(item: view, attribute: .bottom, relatedBy: .equal, toItem: self, attribute: .bottom, multiplier: 1.0, constant: bottom))
        
        self.addConstraint(NSLayoutConstraint(item: view, attribute: .left, relatedBy: .equal, toItem: self, attribute: .left, multiplier: 1.0, constant: left))
        
        view.translatesAutoresizingMaskIntoConstraints = false //bất cứ view nào thuộc skyview cũng phải tuân thủ theo sexFixLayout nếu có constraint khác sẽ gây xung đột => code này : cho phép thay đổi constraint sexFixLayout đã được cố định trước đó
    }
    
    
    
    
    //MARK: - SHADDOW
    
    func shadow(radius : CGFloat, width : CGFloat, height : CGFloat, opacity : CGFloat,
                 shadowRadius : CGFloat, shadowColor : UIColor) {
        self.layer.masksToBounds = false
        self.layer.shadowOffset = CGSize.init(width: width, height: height)
        self.layer.opacity = Float(opacity)
        self.layer.shadowRadius = shadowRadius
        self.layer.shadowColor = shadowColor.cgColor
        self.layer.cornerRadius = radius
    }
    
    func dropShadow () {
        shadow(radius: 2.0, width: -1, height: -1, opacity: 1, shadowRadius: 2.0, shadowColor: .black)
    }
    
    func dropShadow (_ color : UIColor) {
        shadow(radius: 2.0, width: -1, height: -1, opacity: 1, shadowRadius: 2.0, shadowColor: color)
    }
    
    
    func shadow(radius : CGFloat, width : CGFloat, height : CGFloat, opacity : CGFloat,
                shaddowRadius : CGFloat, color : UIColor){
        self.layer.masksToBounds = false;
        self.layer.shadowOffset = CGSize.init(width: width, height: height)
        self.layer.shadowOpacity = Float(opacity)
        self.layer.shadowRadius = shaddowRadius
        self.layer.cornerRadius = radius
        self.layer.shadowColor = color.cgColor
    }
    
    //MARK: - RADIUS
    
    func drawRadius() {
        self.layer.cornerRadius = self.bounds.size.height/2
        self.clipsToBounds = true //phần được giới hạn bởi đường tròn mới thao tác được, cạnh góc vuông k
    }
    
    func drawBorderRadius(radius : CGFloat) {
        self.clipsToBounds = true
        self.layer.masksToBounds = true
        self.layer.cornerRadius = radius
        self.layer.borderWidth = 0.5
        self.layer.borderColor = UIColor.clear.cgColor
    }
    
    func drawBorderRadius(radius : CGFloat, color : UIColor, thickness : CGFloat) {
        self.clipsToBounds = true
        self.layer.masksToBounds = true //phần được giới hạn bởi đường tròn mới thao tác được, cạnh góc vuông k
        self.layer.cornerRadius = radius
        self.layer.borderWidth = thickness
        self.layer.borderColor = color.cgColor
    }
    
    
    
}
