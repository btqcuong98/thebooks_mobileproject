//
//  Extension+String.swift
//  TheBooks
//
//  Created by Mojave on 7/7/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import UIKit

extension String {
    
    //MARK: - LOCALIZED
    
    func localized() -> String{
        return configLanguage.localized(forkey: self)
    }
    
    //MARK: - COLOR
    
    func getIntFromHex(_ hexStr : String) -> UInt32 {
        var hexInt : UInt32 = 0
        let scanner = Scanner(string: hexStr)
        scanner.charactersToBeSkipped = CharacterSet(charactersIn: "#")
        scanner.scanHexInt32(&hexInt)
        return hexInt
    }
    
    func hexColor() -> UIColor{
        let hextInt = Int(getIntFromHex(self))
        let red = CGFloat((hextInt & 0xff0000) >> 16) / 255.0
        let green = CGFloat((hextInt & 0xff00) >> 8) / 255.0
        let blue = CGFloat((hextInt & 0xff) >> 0) / 255.0
        let color = UIColor(red: red, green: green, blue: blue, alpha: 1.0)
        return color
    }
    
    //MARK: - IMAGE
    
    func image() -> UIImage {
        if(self.count == 0) {
            return UIImage.init()
        }
        
        if #available(iOS 13.0, *) {
            if(UIImage.init(named: self, in: ProjectBundle, compatibleWith: nil) != nil){
                return UIImage.init(named: self, in: ProjectBundle, compatibleWith: nil)!
            }
        } else {
            if(UIImage.init(named: self) != nil) {
                return UIImage.init(named: self)!
            }
        }
        
         return UIImage.init()
    }
}

//MARK: - CHANGE STRING TO IMAGE

extension String {
    func stringToImage() -> UIImage? {
        if let data = Data(base64Encoded: self, options: .ignoreUnknownCharacters) {
            return UIImage.init(data: data)
        }
        return nil
    }
    
    //MARK: - FORMAT STRING
    
    //chuẩn hoá chuỗi
    func trim() -> String{
        return self.trimmingCharacters(in: NSCharacterSet.whitespaces)
    }
    
    func standardizedString() -> String {
        var text = self
        while(text.contains("  ")) {
            text = text.replacingOccurrences(of: "  ", with: " ")
        }
        return text.trim()
    }
    
}
