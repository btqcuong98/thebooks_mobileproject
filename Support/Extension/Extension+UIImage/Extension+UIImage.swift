//
//  Extension+UIImage.swift
//  TheBooks
//
//  Created by Mojave on 7/8/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import UIKit

//MARK : - CHANGE IMAGE TO STRING

extension UIImage {
    func imageToString() -> String? {
        let data : Data? = self.pngData()
        return data?.base64EncodedString(options: .lineLength64Characters)
    }
}
