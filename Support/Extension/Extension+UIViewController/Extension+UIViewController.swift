//
//  Extension+UIController.swift
//  TheBooks
//
//  Created by Mojave on 7/7/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import UIKit

extension UIViewController {
    
    //MARK: - CONVENIENCE
    
    convenience init(_ nibName : String) { //thay đổi được param truyền vào của 1 hàm mặt định với cùng 1 tên hàm và gọi lại hàm init ngay bên trong nó
        self.init(nibName: nibName, bundle: ProjectBundle)
    }
    
    //MARK: - PUSH
    
    func push(_ target : UIViewController) {
        navigationController?.pushViewController(target, animated: true)
    }
    
    
    //MARK: - POP/DISMISS
    
    func actionBackNavigation() {
        if(navigationController != nil && (navigationController?.viewControllers.count)! > 1) {
            navigationController?.popViewController(animated: true)
        } else {
            navigationController?.dismiss(animated: true, completion: nil)
        }
    }
}
