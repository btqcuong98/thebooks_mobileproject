//
//  CellPlank.swift
//  TheBooks
//
//  Created by Mojave on 7/22/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import UIKit

class CellPlank: SkyView {
    
    //MARK: - IBOULET
    
    @IBOutlet weak var imgImage : UIImageView!
    @IBOutlet weak var lblTitle : SkyLabel!
    
    //MARK: - DECLARE
    //MARK: - INIT
    
    override func initView() {
        super.initView()
        
        lblTitle.textAlignment = .center
    }
    
    //MARK: - DARK/LIGHT + CHANGLANGUAGE CHANGE
    
    override func darkLightMode() {
        super.darkLightMode()
        
    }
    
    override func changeLanguage() {
        super.changeLanguage()
    }
    
    //MARK: - FUNC
    
    func setData(_ image : UIImage, _ text : String) {
        imgImage.image = image
        lblTitle.text = text
    }
    
    //MARK: - DEINIT
    deinit {
        
    }

}
