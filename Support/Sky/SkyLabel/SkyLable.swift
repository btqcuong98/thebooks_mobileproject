//
//  SkyLable.swift
//  TheBooks
//
//  Created by Mojave on 7/9/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import UIKit

class SkyLabel: UILabel {
    
    //MARK: - INIT
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        notification.add(self, #selector(darkLightModeChange), .DarkModeChange)
        notification.add(self, #selector(darkLightModeChange), .LanguageChange)
        darkLightModeChange()
        languageChange()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        notification.add(self, #selector(darkLightModeChange), .DarkModeChange)
        notification.add(self, #selector(darkLightModeChange), .LanguageChange)
        darkLightModeChange()
        languageChange()
    }
    
    
    //MARK: - DARK/LIGHT + LANGUAGE CHANGE
    
    @objc func darkLightModeChange() {
        self.textColor = colorTemplate.textColor
        self.backgroundColor = .clear
        self.numberOfLines = 0
    }
    
    @objc func languageChange() {
       updateText()
    }
    
    //MARK: - MOTHOD
    private var _title = ""
    @IBInspectable var title : String {
        get{
            
            return self.text ?? ""
            
        } set {
            
            _title = newValue
            updateText()
        }
    }
    
    
    private var _isUper = true
    @IBInspectable var isUper : Bool {
        get {
            return _isUper
        } set {
            _isUper = newValue
        }
        
    }
    
    private func updateText() {
        if(_title.count > 0 && _isUper) {
            _title = _title.uppercased().localized()
        }
        
        self.text = _title.localized()
    }
    
    //MARK: - DEINIT
    
    deinit {
        notification.remove(self, .DarkModeChange)
    }
    
}

//MARK: - CUSTOME

class SkyLableDiscript1 : SkyLabel {
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.isUper = false
        self.font = colorTemplate.boldSize16
        self.numberOfLines = 1
    }
}

class SkyLableDiscript2 : SkyLabel {
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.isUper = false
        self.font = colorTemplate.size16Font
        self.textColor = colorTemplate.subtextColor
        self.numberOfLines = 1
    }
}
