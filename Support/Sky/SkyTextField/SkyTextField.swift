//
//  SkyTextField.swift
//  TheBooks
//
//  Created by Mojave on 7/9/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import UIKit

class SkyTextFiled: UITextField {
    
    //MARK: - INIT
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        notification.add(self, #selector(darkLightChange), .DarkModeChange)
        darkLightChange()
        
        self.clearButtonMode = .whileEditing
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        notification.add(self, #selector(darkLightChange), .DarkModeChange)
        darkLightChange()
        
        self.clearButtonMode = .whileEditing
    }
    
    //MARK: - DARK/LIGHT + LANGUAGE CHANGE
    
    @objc func darkLightChange() {
        self.textColor = colorTemplate.textColor
        self.placeholderColor = colorTemplate.subtextColor
        initStyle ()
    }
    
    
    //MARK: - METHOD
    
    private var _isSecurity = false
    @IBInspectable var isSecurity : Bool {
        get {
            return _isSecurity
        } set {
            _isSecurity = newValue
            updateText()
        }
    }
    
    @IBInspectable var placeholderColor : UIColor? {
        get {
            return self.placeholderColor
        } set {
            self.attributedPlaceholder = NSAttributedString(string: self.placeholder != nil ? self.placeholder! : "", attributes: [NSAttributedString.Key.foregroundColor : newValue!])
        }
    }
    
    private func updateText() {
        if(_isSecurity) {
            self.isSecureTextEntry = true
        }
    }
    
    //MARK: - FUNC
    
    func setPlacehoder(_ text : String, _ color : UIColor = colorTemplate.subtextColor) {
        self.placeholder = text
        placeholderColor = color
    }
    
    func initStyle () {
        self.drawBorderRadius(radius: 5.0, color: colorTemplate.lineColor, thickness: 1.5)
    }
    
    //MARK: - KEYBOARD
    
    //MARK: - DENIT
    
    deinit {
        notification.remove(self, .DarkModeChange)
    }
}


