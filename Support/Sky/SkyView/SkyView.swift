//
//  SkyView.swift
//  TheBooks
//
//  Created by Mojave on 7/5/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import UIKit

let bundle = "com.BuiThaiQuocCuong.TheBooks"
let ProjectBundle = Bundle.init(identifier: bundle)

class SkyView: UIView {
    //MARK: - OUTLET
    
    @IBOutlet weak var view : UIView!
    
    
    //MARK: - INIT
    
    override init(frame: CGRect) { //gọi khi được khỏi tạo SkyView()
        super.init(frame: frame)
        
        self.backgroundColor = .clear
        xibSetUp()
        initView()
        addNotification()
    }
    
    required init?(coder: NSCoder) {//chạy đầu tiên khi được sửa dụng 'kế thừa'
        super.init(coder: coder)
    }
    
    override func awakeFromNib() {//chạy sau required khi được sửa dụng 'kế thừa'
        super.awakeFromNib()
        
        xibSetUp()
        initView()
        addNotification()
    }
    
    func initView() {}
    
    //MARK: - REGISTER NIB
    
    func getClassNameFromString(_ obj : Any) -> String {
        return String(describing: type(of: obj)).replacingOccurrences(of: "", with: ".Type")
    }
    
    func loadViewForNib(_ view : UIView) -> UIView{
        let nib = UINib(nibName: getClassNameFromString(self), bundle: ProjectBundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        return view
    }
    
    //MARK: - LOAD XIB

    func xibSetUp() {
        view = loadViewForNib(self)
        view.backgroundColor = .clear
        view.frame = bounds
        self.addSubview(view)
        self.setFullLayout(view)
    }
    
    //MARK: - NOTIFICATION
    
    func addNotification() {
        notification.add(self, #selector(changeLanguage), .LanguageChange)
        notification.add(self, #selector(darkLightMode), .DarkModeChange)
    }
    
    @objc func changeLanguage() {
        
    }
    
    @objc func darkLightMode() {
        
    }
    
    //MARK: - DEINIT
    
    deinit {
        print("deinit: \(getClassNameFromString(self))")
    }
}
