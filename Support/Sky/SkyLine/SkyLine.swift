//
//  SkyLine.swift
//  TheBooks
//
//  Created by Mojave on 7/9/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import UIKit

class SkyLine : UIView {
    
    //MARK: - INIT
    
    override init(frame: CGRect) { //chạy khi được gọi SkyLine()
        super.init(frame: frame)
        
        notification.add(self, #selector(darkLightModeChange), .DarkModeChange)
        darkLightModeChange()
    }
    
    required init?(coder: NSCoder) { //chạy đầu tiên khi được sửa dụng 'kế thừa'
        super.init(coder: coder)
    }
    
    override func awakeFromNib() { //chạy kế sau required khi được sửa dụng 'kế thừa'
        super.awakeFromNib()
        
        notification.add(self, #selector(darkLightModeChange), .DarkModeChange)
        darkLightModeChange()
    }
    
    //MARK: - DARK/LIGHT + LANGUAGE CHANGE
    
    @objc func darkLightModeChange() {
        self.backgroundColor = colorTemplate.lineColor
    }
    
    
    //MARK: - DEINIT
    
    deinit {
        notification.remove(self, .DarkModeChange)
    }
    
    
}
