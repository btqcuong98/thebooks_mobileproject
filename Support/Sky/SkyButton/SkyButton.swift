//
//  SkyButton.swift
//  TheBooks
//
//  Created by Mojave on 7/9/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import UIKit

class SkyButton : UIButton {
    
    //MARK: - INIT
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        initStyle()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        initStyle()
        registerDarkLightChange()
    }
    
    //MARK: - FUNC
    
    func initStyle() {
        self.drawBorderRadius(radius: 5)
        self.backgroundColor = colorTemplate.blueColor
        self.setTitleColor(colorTemplate.whiteColor, for: .normal)
        self.titleLabel?.lineBreakMode = .byWordWrapping
        self.titleLabel?.textAlignment = .center
        self.titleLabel?.font = colorTemplate.boldSize16
        
    }
    
    //MARK: - DARK/LIGHT + LANGUAGE CHANGE
    
    private func registerDarkLightChange() {
        notification.add(self, #selector(darkLightChange), .DarkModeChange)
    }
    
    @objc func darkLightChange() {
        
    }
    
    
    //MARK: - DEINIT
    
    deinit {
        
    }
}

class SkyButtonForgotPassword : SkyButton {
    
    //MARK: - INIT
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.backgroundColor = .clear
        self.setTitleColor(colorTemplate.subtextColor, for: .normal)
        self.titleLabel?.font = UIFont.systemFont(ofSize: colorTemplate.size16, weight: .regular)
    }
    
    //MARK: - DARK/LIGHT + LANGUAGE CHANGE
    
    @objc override func darkLightChange() {
        self.setTitleColor(colorTemplate.subtextColor, for: .normal)
    }
}

class SkyButtonComment : SkyButton {
    
    //MARK: - INIT
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.drawBorderRadius(radius: 4.0, color: colorTemplate.lineColor, thickness: 1.0)
        self.backgroundColor = .clear
        self.setTitleColor(colorTemplate.textColor, for: .normal)
        self.titleLabel?.font = colorTemplate.boldSize16
    }
    
    //MARK: - DARK/LIGHT + LANGUAGE CHANGE
    
    @objc override func darkLightChange() {
        self.titleLabel?.font = colorTemplate.boldSize16
    }
}

class SkyButtonCancel: SkyButton {
    //MARK: - INIT
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.backgroundColor = colorTemplate.redColor
    }
}
