//
//  FloatingMenu.swift
//  TheBooks
//
//  Created by Mojave on 7/8/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import UIKit

class FloatingMenu: SkyView {

    //MARK: - OUTLET
    
    @IBOutlet weak var containView : UIView!
    @IBOutlet weak var imgImage : UIImageView!
    @IBOutlet weak var vCount : UIView!
    @IBOutlet weak var lblCount : UILabel!
    
    //MARK: - DECLARE
    
    
    //MARK: - INIT
    
    override func initView() {
        super.initView()
        
        initStyle()
        vCount.drawRadius()
        vCount.hiddenView()
    }
    
    //MARK: - DARK/LIGHT MODE + LANGUAGE CHANGE
    
    override func darkLightMode() {
        super.darkLightMode()
        
        setUpColor()
    }
    
    override func changeLanguage() {
        super.changeLanguage()
    }
    
    //MARK: - FUNC

    func initStyle() {
        self.containView.drawRadius()
        setUpColor()
        
        let pan = UIPanGestureRecognizer.init(target: self, action: #selector(panIn))
        self.addGestureRecognizer(pan)
    }
    
    func setUpColor() {
        self.containView.backgroundColor = colorTemplate.basketBackground
        self.imgImage.image = colorTemplate.getImageDarkLight("cart")
    }
    
    //MARK: - MOVE FLOATING
    
    func silent(_ value : Bool) {
        if(!value) {
            alpha = 1
        } else {
            DispatchQueue.main.asyncAfter(deadline: .now() , execute: {
                UIView.animate(withDuration: 0.24, delay: 0.0, options: UIView.AnimationOptions.curveEaseOut, animations: {
                    self.alpha = 0.5
                }, completion: nil)
            })
        }
    }
    
    @objc func panIn(gesture : UIPanGestureRecognizer) {
        
        let point = gesture.translation(in: self)
        gesture.view?.center = CGPoint.init(x: gesture.view!.center.x + point.x, y: gesture.view!.center.y + point.y)
        gesture.setTranslation(CGPoint.init(), in: self)
        
        
        if(gesture.state == .ended) {
            processEdge()
        }
    }
    
    func processEdge(){
        let point = center
        
        let screenRect = UIScreen.main.bounds
        let screenWidth = screenRect.size.width
        let screenHeight = screenRect.size.height
        let poinScreen = CGPoint.init(x: screenWidth, y: screenHeight)
        
        let x = point.x
        
        let nativeX = poinScreen.x - x
        
        let y = point.y
        
        let nativeY = poinScreen.y - y
        
        let smallest = min(x, nativeX,y ,nativeY)
        var newsPoint = CGPoint.init()
        
        if(x == smallest) {
            newsPoint = CGPoint.init(x: 50, y: self.center.y)
        }
        
        if(nativeX == smallest) {
            newsPoint = CGPoint.init(x: screenWidth - 50, y: self.center.y)
        }
        
        if(y == smallest) {
            newsPoint = CGPoint.init(x: self.center.x, y: screenHeight - 120 )
        }
        
        if(nativeY == smallest) {
            newsPoint = CGPoint.init(x: self.center.x, y: screenHeight - 120 )
        }
        
        UIView.animate(withDuration: 0.24, animations: {
            self.center = newsPoint
        })
    }
    
    
}
