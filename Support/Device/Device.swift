//
//  Devide.swift
//  TheBooks
//
//  Created by Mojave on 7/6/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import UIKit
import SystemConfiguration

extension UIDevice {
    static let modelName: String = {
        var systemInfo = utsname() //đây là cấu trúc nằm trong mô-đun Darwin của iOS
        uname(&systemInfo) //uname là một hàm lấy utsname làm đầu vào và trả về Int32 làm đầu ra.
        let machineMirror = Mirror(reflecting: systemInfo.machine) //Mirror - Một đại diện của cấu trúc con và kiểu hiển thị của một thể hiện của bất kỳ loại nào.
        let identifier = machineMirror.children.reduce("") { identifier, element in
            guard let value = element.value as? Int8, value != 0 else {return identifier}
            return identifier + String(UnicodeScalar(UInt8(value)))
        }



        //J không có tai thỏ || T có tai thỏ

        func mapToDevice(identifier: String) -> String {
            #if os(iOS)
            switch identifier {
            case "iPod5,1":                             return "iphoneJ" //iPod touch (5th generation)
            case "iPod7,1":                             return "iphoneJ" //iPod touch (7th generation)
            case "iPhone3,1", "iPhone3,2", "iPhone3,3": return "iphoneJ" //iPhone 4
            case "iPhone4,1":                           return "iphoneJ" //iPhone 4s
            case "iPhone5,1", "iPhone5,2":              return "iphoneJ" //iPhone 5
            case "iPhone5,3", "iPhone5,4":              return "iphoneJ" //iPhone 5c
            case "iPhone6,1", "iPhone6,2":              return "iphoneJ" //iPhone 5s
            case "iPhone7,2":                           return "iphoneJ" //iPhone 6
            case "iPhone7,1":                           return "iphoneJ" //iPhone 6 Plus
            case "iPhone8,1":                           return "iphoneJ" //iPhone 6s
            case "iPhone8,2":                           return "iphoneJ" //iPhone 6s Plus
            case "iPhone9,1", "iPhone9,3":              return "iphoneJ" //iPhone 7
            case "iPhone9,2", "iPhone9,4":              return "iphoneJ" //iPhone 7 Plus
            case "iPhone8,4":                           return "iphoneJ" //iPhone SE
            case "iPhone10,1", "iPhone10,4":            return "iphoneJ" //iPhone 8
            case "iPhone10,2", "iPhone10,5":            return "iphoneJ" //iPhone 8 Plus
            case "iPhone10,3", "iPhone10,6":            return "iphoneT" //iPhone X
            case "iPhone11,2":                          return "iphoneT" //iPhone XS
            case "iPhone11,4", "iPhone11,6":            return "iphoneT" //iPhone XS Max
            case "iPhone11,8":                          return "iphoneT" //iPhone XR
            case "iPhone12,1":                          return "iphoneT" //iPhone 11
            case "iPhone12,3":                          return "iphoneT" //iPhone 11 Pro
            case "iPhone12,5":                          return "iphoneT" //iPhone 11 Pro Max
            case "iPhone12,8":                          return "iphoneT" //iPhone SE (2nd generation)
            case "i386", "x86_64":                      return "\(mapToDevice(identifier: ProcessInfo().environment["SIMULATOR_MODEL_IDENTIFIER"] ?? "iOS"))"
            default:                                    return identifier
            }
            #endif
        }
        return mapToDevice(identifier: identifier)
    }()
}


let devide = Device.share
class Device : NSObject {
    static let share = Device()
    
    //MARK: - TYPE DEVICE
    
    func is_iPhone() -> Bool{
        return UIDevice.current.userInterfaceIdiom == .phone
    }
    
    func is_iPad() -> Bool{
        return UIDevice.current.userInterfaceIdiom == .pad
    }
    
    //MARK: - ORIENTATION
    
    func is_Portrait() -> Bool{
        return UIDevice.current.orientation.isPortrait
    }
    
    func is_Landcapse() -> Bool{
        return UIDevice.current.orientation.isLandscape
    }
    
    //MARK: - RABIT
    
    func is_Rabit() -> Bool {
        let modelName = UIDevice.modelName
        
        if(is_Landcapse()) {
            return false
        } else {
            if(is_iPhone()) {
                if(modelName == "iphoneT") {
                    return true
                } else {
                    return false
                }
            } else {
                return false
            }
        }
        
    }
    
    
    
    
}
