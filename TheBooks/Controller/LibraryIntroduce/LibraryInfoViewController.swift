//
//  LibraryInfoViewController.swift
//  TheBooks
//
//  Created by Mojave on 7/8/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import UIKit
import CoreLocation

class LibraryInfoViewController: MasterViewController {

    //MARK: - IBOULET
    @IBOutlet weak var lblOpenContent: SkyLabel!
    @IBOutlet weak var lblCloseContent: SkyLabel!
    @IBOutlet weak var colView: UICollectionView!
    @IBOutlet weak var vPhone: UIView!
    @IBOutlet weak var vAddress: UIView!
    @IBOutlet weak var vOpen: UIView!
    @IBOutlet weak var vClose: UIView!
    @IBOutlet weak var lblPhone: SkyLabel!
    @IBOutlet weak var lblAddress: SkyLabel!
    @IBOutlet weak var btnPhone: UIButton!
    @IBOutlet weak var btnAddress: UIButton!
    @IBOutlet weak var btnUserInfo: UIButton!
    
    //MARK: - DECLARE
    
    var count = 0
    var timer : Timer!
    var locationManager = CLLocationManager()
    
    //MARK: - INIT
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        darkLightMode()
        setUpView()
        initLocationManager()
        self.setSimpleNaviTitle("THE BOOKS")
        register()
        timer = Timer.scheduledTimer(timeInterval: 2.0, target: self, selector: #selector(timerScroll), userInfo: nil, repeats: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        //notification.post(.RemoveFloatingMenu)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
    }
    
    //MARK: - REGISTER
    
    struct Indentifier {
        static let cell = "LibraryCell"
    }
    
    func register() {
        colView.register(UINib.init(nibName: Indentifier.cell, bundle: ProjectBundle), forCellWithReuseIdentifier: Indentifier.cell)
        colView.delegate = self
        colView.dataSource = self
        colView.showsHorizontalScrollIndicator = false
        colView.showsVerticalScrollIndicator = false
        colView.backgroundColor = .clear
        colView.contentOffset = CGPoint.zero
    }
    
    //MARK: - DARK/LIGHT + CHANGLANGUAGE CHANGE
    
    override func darkLightMode() {
        super.darkLightMode()
        btnUserInfo.setImage(colorTemplate.getImageDarkLight("profile"), for: .normal)
    }
    
    override func changeLanguage() {
        super.changeLanguage()
    }
    
    //MARK: - AUTO SCROLL
    
    private func scrollView(_ isAnimated : Bool) {
        let index = IndexPath.init(item: count, section: 0)
        colView.scrollToItem(at: index, at: .centeredHorizontally, animated: isAnimated)
    }
    
    @objc private func timerScroll() {
        if(count != 10) {
            scrollView(true)
            count += 1
        } else {
            count = 0
            scrollView(false)
        }
    }

    //MARK: - FUNC
    
    func initLocationManager() {
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        locationManager.requestWhenInUseAuthorization()
    }
    
    func setUpView() {
        lblOpenContent.title = "7 giờ sáng"
        vOpen.drawBorderRadius(radius: vOpen.bounds.height/2, color: colorTemplate.blueColor, thickness: 1.5)
        
        lblCloseContent.title = "22 giờ tối"
        vClose.drawBorderRadius(radius: vClose.bounds.height/2, color: colorTemplate.blueColor, thickness: 1.5)
        
        vPhone.drawBorderRadius(radius: vPhone.bounds.height/2, color: colorTemplate.blueColor, thickness: 1.5)
        vAddress.drawBorderRadius(radius: vPhone.bounds.height/2, color: colorTemplate.blueColor, thickness: 1.5)
        
        btnPhone.drawRadius()
        btnAddress.drawRadius()
        
        lblPhone.title = "+(84) 964 023 160"
        lblAddress.title = "182, Lã Xuân Oai, Phường Tăng Nhơn Phú A, Quận 9, Tp.HCM"
    }
    
    //MARK: - ACTION
    
    @IBAction func phoneTouch(_ sender : UIButton) {
        if let url = URL(string: "tel://+(84)964023160"),
            UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }
    
    @IBAction func addressTouch(_ sender : UIButton) {
       self.openGoogleMap()
    }
    
    @IBAction func profileTouch(_ sender : UIButton) {
        let vc = UserInfoViewController.init("UserInfoViewController")
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
    }
    
    //MARK: - DEINIT
    
    deinit {
        
    }
    

}

extension LibraryInfoViewController : UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Indentifier.cell, for: indexPath) as! LibraryCell
        cell.setImage("LogoBook_light".image())
        return cell
    }
    
    
}

extension LibraryInfoViewController : UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = collectionView.bounds.width
        //let height = collectionView.bounds.height
        return CGSize.init(width: width - 10 , height: width - 10)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.init(top: 0, left: 0, bottom: 0, right: 0)
    }
}

extension LibraryInfoViewController : UICollectionViewDelegate {
}

extension LibraryInfoViewController: CLLocationManagerDelegate {
    func openGoogleMap() {
        let geoCoder = CLGeocoder()
        geoCoder.geocodeAddressString(self.lblAddress.title) { (placemarks, err) in
            guard let placemarks = placemarks, let location = placemarks.first?.location else{
                print("No location Found")
                return
            }
            self.openGoogleDirectionMap("\(location.coordinate.latitude)", "\(location.coordinate.longitude)")
        }
    }
    
    func openGoogleDirectionMap(_ destinationLat: String, _ destinationLng: String) {
        let testURL: NSURL = NSURL(string: "comgooglemaps-x-callback://")!
        if UIApplication.shared.canOpenURL(testURL as URL) {
            if let address = lblAddress.title.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed) {
                let directionsRequest: String = "comgooglemaps-x-callback://" + "?daddr=\(address)" + "&x-success=sourceapp://?resume=true&x-source=AirApp"
                let directionsURL: NSURL = NSURL(string: directionsRequest)!
                let application:UIApplication = UIApplication.shared
                if (application.canOpenURL(directionsURL as URL)) {
                    application.open(directionsURL as URL, options: [:], completionHandler: nil)
                }
            }
        } else {
            print("Can't use comgooglemaps-x-callback:// on this device.")
        }
    }
}

