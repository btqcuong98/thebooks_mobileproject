//
//  LibraryCell.swift
//  TheBooks
//
//  Created by Mojave on 8/21/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import UIKit

class LibraryCell: UICollectionViewCell {

    @IBOutlet weak var img: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    func  setImage (_ img : UIImage) {
        self.img.image = img
    }
}
