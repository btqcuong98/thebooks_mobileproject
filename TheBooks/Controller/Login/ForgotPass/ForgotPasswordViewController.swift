//
//  ForgotPasswordViewController.swift
//  TheBooks
//
//  Created by Bui Thai Quoc Cuong on 12/20/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import UIKit

class ForgotPasswordViewController: MasterViewController {
    @IBOutlet weak var vContain: UIView!
    @IBOutlet weak var loadingView: UIActivityIndicatorView!
    @IBOutlet weak var lblUsername: SkyLabel!
    @IBOutlet weak var tfUsername: SkyTextFiled!
    @IBOutlet weak var lblEmail: SkyLabel!
    @IBOutlet weak var tfEmail: SkyTextFiled!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var btnChangePass: SkyButton!
    
    //MARK: - DECLARE
    
    var lstTextField : [UITextField] = []
    
    //MARK: - INIT
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        darkLightMode()
        changeLanguage()
        lstTextField = [tfUsername, tfEmail]
        self.tfUsername.delegate = self
        self.tfEmail.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.loadingView.hiddenView()
    }
    
    //MARK: - DARK/LIGHT + CHANGLANGUAGE CHANGE
    
    override func darkLightMode() {
        super.darkLightMode()
        vContain.drawBorderRadius(radius: 10.0, color: colorTemplate.basketBackground, thickness: 0.5)
        vContain.backgroundColor = colorTemplate.backgroundColCell
        btnBack.setImage(colorTemplate.getImageDarkLight("close"), for: .normal)
        
    }
    
    override func changeLanguage() {
        super.changeLanguage()
        
        self.setSimpleNaviTitle("ForgotPassword".localized())
        lblUsername.title = "UserName".localized()
        lblEmail.title = "Email".localized()
        btnChangePass.setTitle("Complete".localized(), for: .normal)
    }
    
    func alert(_ rs: String, _ code: Int) {
        self.view.messageDialog(self.view, rs) {(_) in
            if code == 200 {
                self.dismiss(animated: false)
            }
        }
        return
    }
    
    //MARK: - ACTION
    
    @IBAction func ForgotPassTouch(_ sender: UIButton) {
        
        if(checkEmpty()) {
            let request = ForgotPass_Request()
            request.MaND = tfUsername.text ?? ""
            request.toEmail = tfEmail.text ?? ""
            
            self.loadingView.showView()
            self.loadingView.startAnimating()
            DispatchQueue.global().async {
                ServiceRequest.forgotPass(request: request, sucess: { (response, code) in
                    DispatchQueue.main.async {
                        self.loadingView.hiddenView()
                        self.loadingView.stopAnimating()
                        self.alert(response, code)
                    }
                })
            }
        }
    }
    
    @IBAction func BackTouch(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func  checkEmpty() -> Bool {
        if(tfUsername.text == "") {
            self.view.errorDialog(self.view, "Không được để trống trường UserName") { (index) in
                self.tfUsername.becomeFirstResponder()
            }
            return false
        }
        
        if(tfEmail.text == "") {
            self.view.errorDialog(self.view, "Không được để trống trường Email") { (index) in
                self.tfEmail.becomeFirstResponder()
            }
            return false
        }
        
        return true
    }
    
    
    //MARK: - DEINIT
    
    deinit {
        
    }

}

extension ForgotPasswordViewController : UITextFieldDelegate {
    public func textFieldDidBeginEditing(_ textField: UITextField) {
    }
    
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if(textField == self.tfUsername) {
            self.tfUsername.becomeFirstResponder()
            return false
        } else if(textField == self.tfEmail) {
            self.tfEmail.becomeFirstResponder()
            return false
        }
        
        return true
    }
}
