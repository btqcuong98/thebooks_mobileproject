//
//  LoginViewController.swift
//  TheBooks
//
//  Created by Mojave on 7/6/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import UIKit
import SVProgressHUD

class LoginViewController: MasterViewController{

    //MARK: - OUTLET
    
    @IBOutlet weak var logoImage : UIImageView!
    @IBOutlet weak var btnChangeLanguage : UIButton!
    @IBOutlet weak var btnchangeDarkLight : UIButton!
    @IBOutlet weak var containView : UIView!
    @IBOutlet weak var lblUserName : SkyLabel!
    @IBOutlet weak var lblPassword : SkyLabel!
    @IBOutlet weak var tfUserName : SkyTextFiled!
    @IBOutlet weak var tfPassword : SkyTextFiled!
    @IBOutlet weak var btnShowPassword : UIButton!
    @IBOutlet weak var btnLogin : SkyButton!
    @IBOutlet weak var btnForgotPassword : SkyButton!
    @IBOutlet weak var spaceBot : NSLayoutConstraint!
    @IBOutlet weak var loadingView : UIActivityIndicatorView!
    
    //MARK: - DECLARE
    
    var usrName = ""
    var pswd = ""
    
    //MARK: - INIT
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        darkLightMode()
        changeLanguage()
        
        ratioView()
        
        self.tfPassword.delegate = self
        self.tfUserName.delegate = self
        
        ServiceRequest.dausach(request: DauSach_Request()) { (data) in
            
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        loadData()
        loadingView.isHidden = true
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        notification.remove(self, .LoginSucess)
    }
    
    //MARK: - DARK/LIGHT MODE + CHANGE LANGUAGE
    
    override func darkLightMode() {
        super.darkLightMode()
        
        setUpDarklight()
    }
    
    override func changeLanguage() {
        super.changeLanguage()
        
        btnChangeLanguage.setImage(UIImage.init(named: configLanguage.getLangType().getLangIconName()), for: .normal)
        setUpLanguage()
    }
    
    //MARK: - FUNC
    
    private func loadData() {
        if(dataManager.loginName != "" && dataManager.password != "") {
            tfUserName.text = dataManager.loginName
            tfPassword.text = dataManager.password
            usrName = tfUserName.text!
            pswd = tfPassword.text!
            
        }
    }
    
    @objc private func preLogin() {
        processLogin(usrName, pswd)
    }
    
    private func processLogin(_ username : String, _ password : String) {
        if checkEmpty(username, password) {
            let request = Login_Request()
            request.UserName = username
            request.Password = password
            
            loadingView.isHidden = false
            loadingView.startAnimating()
            ServiceRequest.login(request: request){ [weak self] (data) in
                self?.loadingView.stopAnimating()
                self?.loadingView.isHidden = false
                if(data.count != 0) {
                    dataManager.UserLogin = data[0]
                    dataManager.loginName = username
                    dataManager.password = password
                    dataManager.roles = data[0].MaNQ
                    dataManager.saveLogin()
                    dataManager.reload()
                    notification.post(.LoginSucess)
                } else {
                    self?.view.errorDialog(self?.view ?? UIView(), "Đăng Nhập Thất Bại !", failure: { (index) in
                        if(index == 0) {
                            self?.btnLogin.alpha = 1
                            self?.btnLogin.isUserInteractionEnabled = true
                            self?.loadingView.stopAnimating()
                            self?.loadingView.isHidden = true
                        }
                    })
                }
            }
        }
    }
    
    func  checkEmpty(_ username : String, _ password : String) -> Bool {
        if(username == "") {
            self.view.errorDialog(self.view, "Không được để trống Tên Đăng Nhập") { (index) in
                self.btnLogin.isUserInteractionEnabled = true
                self.btnLogin.alpha = 1
            }
            return false
        }
        if(password == "") {
            self.view.errorDialog(self.view, "Không được để trống Mật Khẩu") { (index) in
                self.btnLogin.isUserInteractionEnabled = true
                self.btnLogin.alpha = 1
            }
            return false
        }
        return true
    }
    
    private func setUpLanguage() {
        lblUserName.text = "UserName".localized()
        
        lblPassword.text = "Password".localized()
        
        tfPassword.setPlacehoder("Password_holder".localized())
        tfUserName.setPlacehoder("UserName_holder".localized())
        
        btnLogin.setTitle("Login".localized(), for: .normal)
        btnForgotPassword.setTitle("ForgotPassword".localized(), for: .normal)
    }
    
    private func setUpDarklight() {
        containView.drawBorderRadius(radius: 24, color: colorTemplate.basketBackground, thickness: 0.5)
        containView.backgroundColor = colorTemplate.backgroundColCell
        btnchangeDarkLight.setImage(colorTemplate.getImageDarkLight("template"), for: .normal)
        logoImage.image = colorTemplate.getImageDarkLight("LogoBook")
        btnShowPassword.setImage(colorTemplate.getImageDarkLight("eye"), for: .normal)
    }
    
    private func ratioView() {
        if(!Device.share.is_Rabit()) {
            spaceBot.constant = 120
        } else {
            spaceBot.constant = 250
        }
    }
    
    //MARK: - ACTION
    
    @IBAction private func ChangeLanguageTouch(_ sender : UIButton) {
        let vc = LanguageView()
        PopTip.openPopTipView("", message: "", contentSize: CGSize(width: 180, height: 80), myView: vc, targetView: self.btnChangeLanguage, containerView: self.view)
    }

    @IBAction private func ChangeDarkLightTouch(_ sender : UIButton) {
        colorTemplate.isDarkMode = !colorTemplate.isDarkMode
    }
    
    @IBAction private func LoginTouch(_ sender : UIButton) {
        
        processLogin(tfUserName.text ?? "", tfPassword.text ?? "")
        btnLogin.isUserInteractionEnabled = false
        btnLogin.alpha = 0.5
    }
    
    @IBAction private func ForgotPasswordTouch(_ sender : UIButton) {
        let vc = ForgotPasswordViewController.init("ForgotPasswordViewController")
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
    }
    
    
    @IBAction func hiddenPasswordTouch(_ sender: UIButton) {
        tfPassword.isSecureTextEntry = true
    }
    
    @IBAction func showPasswordTouch(_ sender: UIButton) {
        tfPassword.isSecureTextEntry = false
    }
    
    @IBAction func touch(_ sender: UIButton) {
       
    }
    
    //DEINIT
    
    deinit {
        
    }
}

extension LoginViewController : UITextFieldDelegate {
    public func textFieldDidBeginEditing(_ textField: UITextField) {
    }
    
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if(textField == self.tfUserName) {
            self.tfPassword.becomeFirstResponder()
            return false
        } else if(textField == self.tfPassword) {
            textField.resignFirstResponder()
        }
        
        return true
    }
}
