//
//  NotificationViewController.swift
//  TheBooks
//
//  Created by Mojave on 7/8/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import UIKit

class NotificationViewController: MasterViewController {

    //MARK: - IBOULETN
    
    @IBOutlet weak var tbvView : UITableView!
    @IBOutlet weak var vPlank : CellPlank!
    
    //MARK: - DECLARE
    
    var lstData : [String] = []
    
    //MARK: - INIT
    
    override func viewDidLoad() {
        super.viewDidLoad()
        register ()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        darkLightMode()
        changeLanguage()
        //notification.post(.RemoveFloatingMenu)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    //MARK: - REGISTER
    
    struct Indentifier {
        static let cell = "NotifiCell"
    }
    
    func register () {
        tbvView.register(UINib.init(nibName: Indentifier.cell, bundle: ProjectBundle), forCellReuseIdentifier: Indentifier.cell)
        tbvView.delegate = self
        tbvView.dataSource = self
        tbvView.separatorStyle = .none
    }
    
    //MARK: - DARK/LIGHT + CHANGLANGUAGE CHANGE
    
    override func darkLightMode() {
        super.darkLightMode()
        setUpData()
    }
    
    override func changeLanguage() {
        super.changeLanguage()
        self.setSimpleNaviTitle("Announce".localized())
        setUpData()
    }
    
    
    //MARK: - FUNC
    
    private func setUpData() {
        if(lstData.count < 1) {
            vPlank.showView()
            vPlank.setData(colorTemplate.getImageDarkLight("notification"), "NotePlank".localized().replacingOccurrences(of: "{noun}", with: "Announce".localized()))
        } else {
            vPlank.hiddenView()
        }
    }
    
    //MARK: - DEINIT
    
    deinit {
        
    }
    
    
    
}

extension NotificationViewController : UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return lstData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Indentifier.cell, for: indexPath) as! NotifiCell
        return cell
    }
    

}

extension NotificationViewController : UITableViewDelegate {

}
