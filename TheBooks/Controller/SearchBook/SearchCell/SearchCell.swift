//
//  SearchCell.swift
//  TheBooks
//
//  Created by Mojave on 8/21/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import UIKit

class SearchCell: UITableViewCell {

    @IBOutlet weak var vContain : UIView!
    @IBOutlet weak var imgBook : UIImageView!
    @IBOutlet weak var lblTitle1 : SkyLabel!
    @IBOutlet weak var lblTitle2 : SkyLabel!
    @IBOutlet weak var lblRating : SkyLabel!
    @IBOutlet weak var lblMoney : SkyLabel!
    
    @IBOutlet weak var img1 : UIImageView!
    @IBOutlet weak var img2 : UIImageView!
    @IBOutlet weak var img3 : UIImageView!
    @IBOutlet weak var img4 : UIImageView!
    @IBOutlet weak var img5 : UIImageView!
    @IBOutlet weak var imgMoney : UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.vContain.backgroundColor = colorTemplate.backgroundColCell
        self.vContain.shadow(radius: 15, width: 2, height: 2, opacity: 0.7, shaddowRadius: 1, color: .darkGray)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    
    func setData(_ lst : DauSach_DTO) {
        imgBook.image = lst.AnhBia.image()
        lblTitle1.title = lst.Ten
        switch lst.TacGia.count {
        case 0:
            lblTitle2.title = "Chưa rõ Tác Giả"
        case 1:
            lblTitle2.title = lst.TacGia[0].Ten
        case 2:
            lblTitle2.title = "\(lst.TacGia[0].Ten) + \(lst.TacGia[1].Ten)"
        default:
            lblTitle2.title = "Nhiều Tác Giả"
        }
        lblRating.title = "\(lst.Rating)"
        setRating(lst.MaxRating)
        lblMoney.title = "\(lst.GiaSach)"
    }
    
    func setRating(_ maxRating : Int) {
        switch maxRating {
        case 0:
            img1.image = "star_false".image()
            img2.image = "star_false".image()
            img3.image = "star_false".image()
            img4.image = "star_false".image()
            img5.image = "star_false".image()
        case 1:
            img1.image = "star_true".image()
            img2.image = "star_false".image()
            img3.image = "star_false".image()
            img4.image = "star_false".image()
            img5.image = "star_false".image()
        case 2:
            img1.image = "star_true".image()
            img2.image = "star_true".image()
            img3.image = "star_false".image()
            img4.image = "star_false".image()
            img5.image = "star_false".image()
        case 3:
            img1.image = "star_true".image()
            img2.image = "star_true".image()
            img3.image = "star_true".image()
            img4.image = "star_false".image()
            img5.image = "star_false".image()
        case 4:
            img1.image = "star_true".image()
            img2.image = "star_true".image()
            img3.image = "star_true".image()
            img4.image = "star_true".image()
            img5.image = "star_false".image()
        case 5:
            img1.image = "star_true".image()
            img2.image = "star_true".image()
            img3.image = "star_true".image()
            img4.image = "star_true".image()
            img5.image = "star_true".image()
            
        default: break
            
        }
        
        
    }
    
}
