//
//  SearchBookViewController.swift
//  TheBooks
//
//  Created by Mojave on 7/23/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import UIKit
import  SVProgressHUD

class SearchBookViewController: MasterViewController {

    //MARK: - IBOULET
    
    @IBOutlet weak var tfSearch: SkyTextFiled!
    @IBOutlet weak var btnSearch: UIButton!
    @IBOutlet weak var tbvView: UITableView!
    @IBOutlet weak var btnExit: UIButton!
    @IBOutlet weak var vPlank: CellPlank!
    
    //MARK: - DECLARE
    
    var lstData : [DauSach_DTO] = []
    var temp : [DauSach_DTO] = []
    var tfText = "" //duoc goi khi truyen tu left menu
    
    
    //MARK: - INIT
    
    override func viewDidLoad() {
        super.viewDidLoad()
        darkLightMode()
        changeLanguage()
        register()
        
        if(tfText != "") {
            tfSearch.text = tfText
            searchAction()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        temp = dataManager.lstDauSach
        lstData = temp
        setUpData()
    }
    
    //MARK: - REGISTER
    
    struct Indentifier {
        static let cell = "SearchCell"
    }
    
    private func register() {
        tbvView.register(UINib.init(nibName: Indentifier.cell, bundle: ProjectBundle), forCellReuseIdentifier: Indentifier.cell)
        tbvView.delegate = self
        tbvView.dataSource = self
        tbvView.separatorStyle = .none
        
    }
    
    //MARK: - DARK/LIGHT + CHANGLANGUAGE CHANGE
    
    override func darkLightMode() {
        super.darkLightMode()
        btnSearch.setImage(colorTemplate.getImageDarkLight("search"), for: .normal)
        btnExit.setImage(colorTemplate.getImageDarkLight("close"), for: .normal)
        setUpData()
    }
    
    override func changeLanguage() {
        super.changeLanguage()
        
        tfSearch.placeholder = "EnterBook".localized()
        tfSearch.placeholderColor = colorTemplate.subtextColor
        self.setSimpleNaviTitle("SearchBook".localized())
        
        setUpData()
    }
    
    
    //MARK: - FUNC
    
    private func setUpData() {
        if(lstData.count < 1) {
            vPlank.showView()
            vPlank.setData(colorTemplate.getImageDarkLight("error"), "NotePlank".localized().replacingOccurrences(of: "{noun}", with: "Data".localized()))
        } else {
            vPlank.hiddenView()
        }
        
    }
    
    private func reloadTableView() {
        self.tbvView.reloadData()
        
        if(lstData.count > 0) {
            vPlank.hiddenView()
        }
    }
    
    private func searchAction() {
        
        var lstSearch : [DauSach_DTO] = []
        
        guard !(tfSearch.text == "") else {
            lstData = temp
            reloadTableView()
            return
        }
        
        for item in temp {
            if(utils.removeAccentText(item.TheLoaiCon[0].TenTLC).contains(utils.removeAccentText(tfSearch.text!)) || utils.removeAccentText(item.Ten).contains(utils.removeAccentText(tfSearch.text!))) {
                lstSearch.append(item)
            }
        }
        
        lstData = lstSearch
        reloadTableView()
        return
    }
    
    
    
    //MARK: - ACTION
    
    @IBAction func exitTouch(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func searchTouch(_ sender: Any) {
        searchAction()
    }
    
    @IBAction func textFieldEditDid(_ sender: UITextField) {
        searchAction()
    }
    
    @IBAction func textFieldEditChanged(_ sender: UITextField) {
        searchAction()
    }
    
    //MARK: - DEINIT
    
    deinit {
    }
}

extension SearchBookViewController : UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if dataManager.roles == RolesType.docgia.getRolesTypeString() {
            let vc = BookDetailViewController.init("BookDetailViewController")
            vc.lstData = lstData[indexPath.row]
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: false, completion: nil)
        }
    }
}

extension SearchBookViewController : UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return lstData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Indentifier.cell, for: indexPath) as! SearchCell
        cell.selectionStyle = .none
        cell.setData(lstData[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 165
    }
    
}
