//
//  HistoryOrderViewController.swift
//  TheBooks
//
//  Created by Mojave on 7/8/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import UIKit

class HistoryOrderViewController: MasterViewController {

    //MARK: - IBOULET
    
    @IBOutlet weak var tbvView : UITableView!
    @IBOutlet weak var vPlank : CellPlank!
    @IBOutlet weak var loadingView : UIActivityIndicatorView!
    
    //MARK: - DECLARE
    
    var lstData : [DauSach_DTO] = []
    
    //MARK: - INIT
    
    override func viewDidLoad() {
        super.viewDidLoad()
        register()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        fetchHistory()
        darkLightMode()
        changeLanguage()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    //MARK: - REGISTER
    
    struct Indentifier {
        static let cell = "HistoryOrderCell"
    }
    
    func register() {
        tbvView.register(UINib.init(nibName: Indentifier.cell, bundle: ProjectBundle), forCellReuseIdentifier: Indentifier.cell)
        tbvView.delegate = self
        tbvView.dataSource = self
        tbvView.separatorStyle = .none
    }
    
    //MARK: - DARK/LIGHT + CHANGLANGUAGE CHANGE
    
    override func darkLightMode() {
        super.darkLightMode()
        setUpData()
    }
    
    override func changeLanguage() {
        super.changeLanguage()
        self.setSimpleNaviTitle("History".localized())
        setUpData()
    }
    
    //MARK: - FUNC
    
    private func setUpData() {
        if(lstData.count < 1) {
            vPlank.showView()
            vPlank.setData(colorTemplate.getImageDarkLight("history"), "NotePlank".localized().replacingOccurrences(of: "{noun}", with: "History".localized()))
        } else {
            vPlank.hiddenView()
        }
    }
    
    private func fetchHistory() {
        let request = LichSu_Request()
        request.MaDG = dataManager.UserLogin.MaND
        
        self.loadingView.startAnimating()
        DispatchQueue.global().async {
            ServiceRequest.lichsu(request: request, success: { [weak self] (response) in
                self?.loadingView.isHidden = true
                self?.lstData = response
                DispatchQueue.main.async {
                    self?.loadingView.hiddenView()
                    self?.loadingView.stopAnimating()
                    self?.setUpData()
                    self?.tbvView.reloadData()
                }
            })
        }
    }
    //MARK: - DEINIT
    
    deinit {
        
    }
    

}

extension HistoryOrderViewController : UITableViewDelegate {
    
}

extension HistoryOrderViewController : UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return lstData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Indentifier.cell, for: indexPath) as! HistoryOrderCell
        cell.setData(lstData[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 165
    }
    
}
