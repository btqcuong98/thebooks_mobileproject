//
//  MyInfoViewController.swift
//  TheBooks
//
//  Created by Mojave on 7/8/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import UIKit
import SVProgressHUD

class StatusOrderViewController: MasterViewController {

    //MARK: - IBOULET
    
    @IBOutlet weak var colView: UICollectionView!
    @IBOutlet weak var btn0: SkyButton! //đang chờ
    @IBOutlet weak var btn1: SkyButton! //đang mượn
    @IBOutlet weak var btn2: SkyButton! //quá hạn
    @IBOutlet weak var line0: UIView!
    @IBOutlet weak var line1: UIView!
    @IBOutlet weak var line2: UIView!
    
    //MARK: - DECLARE
    var lstData0 : [ThongTinMuonSach_DTO] = []
    var lstData1 : [ThongTinMuonSach_DTO] = []
    var lstData2 : [ThongTinMuonSach_DTO] = []
    var indexCurr = 0;

    //MARK: - INIT
    
    override func viewDidLoad() {
        super.viewDidLoad()
        darkLightMode()
        setUpView()
        register()
        self.setSimpleNaviTitle("BookInfo".localized())
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        colView.setNeedsLayout()
        colView.reloadData()
        line0.backgroundColor = colorTemplate.blueColor
        line1.backgroundColor = .clear
        line2.backgroundColor = .clear
    }
    
    //MARK: - REGISTER
    
    struct Indentifier {
        static let cell = "ContentCell"
    }
    
    func register() {
        colView.register(UINib.init(nibName: Indentifier.cell, bundle: ProjectBundle), forCellWithReuseIdentifier: Indentifier.cell)
        colView.delegate = self
        colView.dataSource = self
        colView.showsHorizontalScrollIndicator = true
        colView.showsVerticalScrollIndicator = false
        colView.backgroundColor = .clear
        colView.contentOffset = CGPoint.zero
    }
    
    //MARK: - DARK/LIGHT + CHANGLANGUAGE CHANGE
    
    override func darkLightMode() {
        super.darkLightMode()
    }
    
    override func changeLanguage() {
        super.changeLanguage()
    }
    
    //MARK: - FUNC
    
    private func setUpView() {
        if(dataManager.UserLogin.MaNQ == "DocGia") {
            btn0.setTitle("Đang Chờ", for: .normal)
            btn0.setTitleColor(colorTemplate.subtextColor, for: .normal)
            btn0.backgroundColor = .clear
            
            btn1.setTitle("Đang Mượn", for: .normal)
            btn1.setTitleColor(colorTemplate.subtextColor, for: .normal)
            btn1.backgroundColor = .clear
            
            btn2.setTitle("Quá Hạn", for: .normal)
            btn2.setTitleColor(colorTemplate.subtextColor, for: .normal)
            btn2.backgroundColor = .clear
        } else {
            btn0.setTitle("Giao Sách", for: .normal)
            btn0.setTitleColor(colorTemplate.subtextColor, for: .normal)
            btn0.backgroundColor = .clear
            btn1.hiddenView()
            btn2.setTitle("Thu Sách", for: .normal)
            btn2.setTitleColor(colorTemplate.subtextColor, for: .normal)
            btn2.backgroundColor = .clear
        }
    }
    
    //MARK: - IBACTION
    
    @IBAction func settingTouch(_ sender : UIButton) {
        let vc = ChangePassView.init("ChangePassView")
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func btn0Touch(_ sender : UIButton) { //Đang chờ
        line0.backgroundColor = colorTemplate.blueColor
        line1.backgroundColor = .clear
        line2.backgroundColor = .clear
        btn0.alpha = 1
        btn1.alpha = 0.5
        btn2.alpha = 0.5
        indexCurr = 0
        self.colView.reloadData()
    }
    
    @IBAction func btn1Touch(_ sender : UIButton) { //Đang mượn
        line0.backgroundColor = .clear
        line1.backgroundColor = colorTemplate.blueColor
        line2.backgroundColor = .clear
        btn1.alpha = 1
        btn0.alpha = 0.5
        btn2.alpha = 0.5
        indexCurr = 1
        self.colView.reloadData()
    }
    
    @IBAction func btn2Touch(_ sender : UIButton) { //Quá hạn
        line0.backgroundColor = .clear
        line1.backgroundColor = .clear
        line2.backgroundColor = colorTemplate.blueColor
        btn2.alpha = 1
        btn0.alpha = 0.5
        btn1.alpha = 0.5
        indexCurr = 2
        self.colView.reloadData()
    }
    
    //MARK: - DEINIT
}

//MARK: - DELEGATE DATASOURCE


extension StatusOrderViewController : UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    }
}

extension StatusOrderViewController : UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Indentifier.cell, for: indexPath) as! ContentCell
        
        cell.delegate = self
        
        if(dataManager.roles == "DocGia"){
            if(indexCurr == 0) {
                cell.index = 0
                cell.setData()
                
            } else if(indexCurr == 1) {
                cell.index = 1
                cell.setData()
                
            } else if(indexCurr == 2) {
                cell.index = 2
                cell.setData()
            }
        } else {
            if(indexCurr == 0) {
                cell.index = 0
                cell.setData()
                
            } else if(indexCurr == 2) {
                cell.index = 2
                cell.setData()
            }
        }
        return cell
    }
}

extension StatusOrderViewController : UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = collectionView.bounds.width
        let height = collectionView.bounds.height
        return CGSize.init(width: width , height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.init(top: 0, left: 0, bottom: 0, right: 0)
    }
}

extension StatusOrderViewController : ContentCellDelegate {
    
    func MyInfoCellToBookDetail(_ lst: GiaoSach_DTO, _ index: Int) {
        let vc = StaftViewController.init("StaftViewController")
        vc.lstData = lst
        vc.index = index
        self.present(vc, animated: true, completion: nil)
    }
    

}
