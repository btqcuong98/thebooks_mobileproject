//
//  StaftViewController.swift
//  TheBooks
//
//  Created by Mojave on 8/24/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import UIKit
import CoreLocation

class StaftViewController: MasterViewController{

    //MARK: - IBOULET
    
    @IBOutlet weak var lblTitle1 : SkyLabel!
    @IBOutlet weak var lblTitle2 : SkyLabel!
    @IBOutlet weak var vView1 : UIView!
    @IBOutlet weak var vView2 : UIView!
    @IBOutlet weak var btnConfirm : UIButton!
    @IBOutlet weak var btnDirection : UIButton!
    @IBOutlet weak var lblTileName : SkyLabel!
    @IBOutlet weak var lblTitleAdress : SkyLabel!
    @IBOutlet weak var lblTitlePhone : SkyLabel!
    @IBOutlet weak var tfTileName : SkyTextFiled!
    @IBOutlet weak var tfTitleAdress : SkyTextFiled!
    @IBOutlet weak var tfTitlePhone : SkyTextFiled!
    @IBOutlet weak var colView : UICollectionView!
    
    //MARK: - DECLARE
    
    var locationManager = CLLocationManager()
    var lstData : GiaoSach_DTO!
    var index = -1

    
    //MARK: - INIT
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setSimpleNaviTitle("THÔNG TIN CHI TIẾT")
        register()
        initStyle()
        initLocationManager()
    }
    
    //MARK: - REGISTER
    
    struct Indentifier {
        static let cell = "TopBookCell"
    }
    
    func register() {
        colView.register(UINib.init(nibName: Indentifier.cell, bundle: ProjectBundle), forCellWithReuseIdentifier: Indentifier.cell)
        colView.delegate = self
        colView.dataSource = self
        colView.showsHorizontalScrollIndicator = true
        colView.showsVerticalScrollIndicator = false
        colView.backgroundColor = .clear
        colView.contentOffset = CGPoint.zero
    }
    
    //MARK: - DARK/LIGHT + CHANGLANGUAGE CHANGE
    //MARK: - FUNC
    
    func initLocationManager() {
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        locationManager.requestWhenInUseAuthorization()
    }
    
    func initStyle() {
        
        lblTitle1.title = "THÔNG TIN KHÁCH HÀNG"
        lblTitle2.title = "THÔNG TIN SÁCH"
        
        lblTileName.title = "Họ Tên"
        lblTitleAdress.title = "Địa Chỉ Nhận"
        lblTitlePhone.title = "Số Điện Thoại"
        
        btnConfirm.setTitle("XÁC NHẬN", for: .normal)
        btnDirection.setTitle("CHỈ ĐƯỜNG", for: .normal)
        
        btnConfirm.drawRadius()
        btnDirection.drawRadius()
        
        tfTitlePhone.textColor = .blue
        
        vView1.drawBorderRadius(radius: 24, color: colorTemplate.basketBackground, thickness: 1)
        vView2.drawBorderRadius(radius: 24, color: colorTemplate.basketBackground, thickness: 1)
        
        tfTileName.text = lstData.Ten
        tfTitlePhone.text = lstData.Sdt
        tfTitleAdress.text = lstData.DiaChi
        
        tfTileName.isUserInteractionEnabled = false
        tfTitlePhone.isUserInteractionEnabled = false
        tfTitleAdress.isUserInteractionEnabled = false
    }
    
    //MARK: - IBACTION
    
    @IBAction func callTouch(_ sender : UIButton) {
        if let url = URL(string: "tel://\(tfTitlePhone.text!)"),
            UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }
    
    @IBAction func backTouch(_ sender : UIButton) {
        notification.post(.ReloadData)
        self.dismiss(animated: false, completion: nil)
    }
    
    @IBAction func confirmTouch(_ sender : UIButton) {
        var dis = ""
        if(index == 0) {
            dis = "Cập nhật trạng thái 'Đã Giao Sach Thành Công' ?"
        } else {
            dis = "Cập nhật trạng thái 'Đã Thu Sách Thành Công' ?"
        }
        
        var idArr : [Int] = []
        
        for item in lstData.DauSach {
            idArr.append(item.Id)
        }
        
        self.view.confirmDialog(self.view, "XÁC NHẬN", dis, [("ĐÓNG", "ĐỒNG Ý")]) { (dongy) in
            
            let request = XacNhanCuaNhanVien_Request()
            request.Id_CtMuon = idArr
            
            if(dongy == 1) {
                if(self.index == 0) {
                    DispatchQueue.global().async {
                        ServiceRequest.xacnhangiaosach(request: request, success: { (response) in
                            if(response == "success") {
                                self.view.successDialog(self.view, "Thành Công", success: { (index) in
                                    if(index == 0) {
                                        notification.post(.ReloadData)
                                        self.dismiss(animated: false, completion: nil)
                                    }
                                    
                                })
                            }
                            
                        })
                    }
                }else{
                    ServiceRequest.xacnhanthusach(request: request, success: { (response) in
                        if(response == "success") {
                            self.view.successDialog(self.view, "Thành Công", success: { (index) in
                                if(index == 0) {
                                    notification.post(.ReloadData)
                                    self.dismiss(animated: false, completion: nil)
                                }
                            })
                        }
                    })
                    
                }//end if
            } else {
            
            }//end if;
            
        }
    }
    
    @IBAction func directionTouch(_ sender : UIButton) {
        self.openGoogleMap()
    }
    
    //MARK: - DEINIT


}

extension StaftViewController : UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return lstData.DauSach.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Indentifier.cell, for: indexPath) as! TopBookCell
        cell.setData(lstData.DauSach[indexPath.item])
        return cell
    }
    
}

extension StaftViewController : UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = collectionView.bounds.width
        let height = collectionView.bounds.height
        return CGSize.init(width: width , height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.init(top: 5, left: 0, bottom: 5, right: 10)
    }
}

extension StaftViewController: CLLocationManagerDelegate {
    func openGoogleMap() {
        let geoCoder = CLGeocoder()
        geoCoder.geocodeAddressString(self.lstData.DiaChi) { (placemarks, err) in
            guard let placemarks = placemarks, let location = placemarks.first?.location else{
                print("No location Found")
                return
            }
            self.openGoogleDirectionMap("\(location.coordinate.latitude)", "\(location.coordinate.longitude)")
        }
    }
    
    func openGoogleDirectionMap(_ destinationLat: String, _ destinationLng: String) {
        let testURL: NSURL = NSURL(string: "comgooglemaps-x-callback://")!
        if UIApplication.shared.canOpenURL(testURL as URL) {
            if let address = lstData.DiaChi.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed) {
                let directionsRequest: String = "comgooglemaps-x-callback://" + "?daddr=\(address)" + "&x-success=sourceapp://?resume=true&x-source=AirApp"
                let directionsURL: NSURL = NSURL(string: directionsRequest)!
                let application:UIApplication = UIApplication.shared
                if (application.canOpenURL(directionsURL as URL)) {
                    application.open(directionsURL as URL, options: [:], completionHandler: nil)
                }
            }
        } else {
            print("Can't use comgooglemaps-x-callback:// on this device.")
        }
    }
}
