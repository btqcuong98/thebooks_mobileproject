//
//  StaftCell.swift
//  TheBooks
//
//  Created by Mojave on 8/24/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import UIKit

class StaftCell: UICollectionViewCell {
    //MARK: - OULET
    @IBOutlet weak var vBackground : UIView!
    @IBOutlet weak var image : UIImageView!
    @IBOutlet weak var lblTitle1 : SkyLabel!
    @IBOutlet weak var lblTitle2 : SkyLabel!
    @IBOutlet weak var lblTitleRating : SkyLabel!
    @IBOutlet weak var img1 : UIImageView!
    @IBOutlet weak var img2 : UIImageView!
    @IBOutlet weak var img3 : UIImageView!
    @IBOutlet weak var img4 : UIImageView!
    @IBOutlet weak var img5 : UIImageView!
    
    //MARK: - INIT
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        notification.add(self, #selector(darkLightChange), .DarkModeChange)
        setUpView()
    }
    
    //MARK: - DARK/LIGHT + LANGUGAE CHANGE
    
    @objc func darkLightChange() {
        vBackground.backgroundColor = colorTemplate.backgroundColCell
    }
    
    //MARK: - FUNC
    
    func setUpView() {
        vBackground.backgroundColor = colorTemplate.backgroundColCell
        vBackground.drawBorderRadius(radius: 15.0, color: .clear, thickness: 0)
        
        image.drawBorderRadius(radius: 15.0, color: .clear, thickness: 0)
        image.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
    }
    
    func setData(_ lstNewBook : DauSach_DTO) {
        lblTitle1.title = lstNewBook.Ten
        
        
        switch lstNewBook.TacGia.count {
        case 0:
            lblTitle2.title = "Chưa rõ Tác Giả"
        case 1:
            lblTitle2.title = lstNewBook.TacGia[0].Ten
        case 2:
            lblTitle2.title = "\(lstNewBook.TacGia[0].Ten) + \(lstNewBook.TacGia[1].Ten)"
        default:
            lblTitle2.title = "Nhiều Tác Giả"
        }
        
        image.image = lstNewBook.AnhBia.image()
        lblTitleRating.title = "\(lstNewBook.Rating)"
        setRating(lstNewBook.MaxRating)
        
    }
    
    func setRating(_ maxRating : Int) {
        switch maxRating {
        case 0:
            img1.image = "star_false".image()
            img2.image = "star_false".image()
            img3.image = "star_false".image()
            img4.image = "star_false".image()
            img5.image = "star_false".image()
        case 1:
            img1.image = "star_true".image()
            img2.image = "star_false".image()
            img3.image = "star_false".image()
            img4.image = "star_false".image()
            img5.image = "star_false".image()
        case 2:
            img1.image = "star_true".image()
            img2.image = "star_true".image()
            img3.image = "star_false".image()
            img4.image = "star_false".image()
            img5.image = "star_false".image()
        case 3:
            img1.image = "star_true".image()
            img2.image = "star_true".image()
            img3.image = "star_true".image()
            img4.image = "star_false".image()
            img5.image = "star_false".image()
        case 4:
            img1.image = "star_true".image()
            img2.image = "star_true".image()
            img3.image = "star_true".image()
            img4.image = "star_true".image()
            img5.image = "star_false".image()
        case 5:
            img1.image = "star_true".image()
            img2.image = "star_true".image()
            img3.image = "star_true".image()
            img4.image = "star_true".image()
            img5.image = "star_true".image()
            
        default: break
            
        }
        
        
    }
    
    //MARK: - DEINIT
    
    deinit {
        
    }
}
