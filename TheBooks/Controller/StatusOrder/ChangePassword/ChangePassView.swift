//
//  ChangePassView.swift
//  TheBooks
//
//  Created by Mojave on 8/21/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import UIKit

class ChangePassView: MasterViewController {
    
    //MARK: - IBOULET
    
    @IBOutlet weak var vContain: UIView!
    @IBOutlet weak var loadingView: UIActivityIndicatorView!
    @IBOutlet weak var lblCurrPass: SkyLabel!
    @IBOutlet weak var lblNewPass: SkyLabel!
    @IBOutlet weak var lblConfirmPass: SkyLabel!
    
    @IBOutlet weak var tfCurrPass: SkyTextFiled!
    @IBOutlet weak var tfNewPass: SkyTextFiled!
    @IBOutlet weak var tfConfirmPass: SkyTextFiled!
    
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var btnChangePass: SkyButton!
    
    //MARK: - DECLARE
    
    var lstTextField : [UITextField] = []
    
    //MARK: - INIT
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        darkLightMode()
        changeLanguage()
        lstTextField = [tfCurrPass, tfNewPass, tfConfirmPass]
        self.tfCurrPass.delegate = self
        self.tfNewPass.delegate = self
        self.tfConfirmPass.delegate = self
        self.tfCurrPass.isSecurity = true
        self.tfNewPass.isSecurity = true
        self.tfConfirmPass.isSecurity = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.loadingView.hiddenView()
    }
    
    //MARK: - DARK/LIGHT + CHANGLANGUAGE CHANGE
    
    override func darkLightMode() {
        super.darkLightMode()
        vContain.drawBorderRadius(radius: 10.0, color: colorTemplate.basketBackground, thickness: 0.5)
        vContain.backgroundColor = colorTemplate.backgroundColCell
        btnBack.setImage(colorTemplate.getImageDarkLight("close"), for: .normal)
        
    }
    
    override func changeLanguage() {
        super.changeLanguage()
        
        self.setSimpleNaviTitle("ChangePass".localized())
        lblCurrPass.title = "CurrPass".localized()
        lblNewPass.title = "NewPass".localized()
        lblConfirmPass.title = "ConfirmPass".localized()
        btnChangePass.setTitle("Complete".localized(), for: .normal)
    }
    
    func alert(_ rs: String, _ code: Int) {
        self.view.messageDialog(self.view, rs) {(_) in
            if code == 200 {
                self.dismiss(animated: false)
            }
        }
        return
    }
    
    //MARK: - ACTION
    
    @IBAction func ChangePassTouch(_ sender: UIButton) {
        
        if(checkEmpty()) {
            let request = ChangePass_Request()
            request.UserName = dataManager.UserLogin.MaND
            request.CurrentPass = tfCurrPass.text!
            request.NewPass = tfNewPass.text!
            request.ConfirmPass = tfConfirmPass.text!

            self.loadingView.showView()
            self.loadingView.startAnimating()
            DispatchQueue.global().async {
                ServiceRequest.changePass(request: request, sucess: { (response, code) in
                    DispatchQueue.main.async {
                        self.loadingView.hiddenView()
                        self.loadingView.stopAnimating()
                        self.alert(response, code)
                    }
                })
            }
        }
    }
    
    @IBAction func BackTouch(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func  checkEmpty() -> Bool {
        if(tfCurrPass.text == "") {
            self.view.errorDialog(self.view, "Không được để trống trường nhập Mật khẩu hiện tại") { (index) in
                
                self.tfCurrPass.becomeFirstResponder()
            }
            
            return false
        }
        
        if(tfNewPass.text == "") {
            self.view.errorDialog(self.view, "Không được để trống trường nhập Mật khẩu mới") { (index) in
                
                self.tfNewPass.becomeFirstResponder()
            }
            
            return false
        }
        
        if(tfConfirmPass.text == "") {
            self.view.errorDialog(self.view, "Không được để trống trường xác nhận Mật khẩu mới") { (index) in
                
                self.tfConfirmPass.becomeFirstResponder()
            }
            
            return false
        }
        
        return true
    }
    
    
    //MARK: - DEINIT
    
    deinit {
        
    }
    
}


extension ChangePassView : UITextFieldDelegate {
    public func textFieldDidBeginEditing(_ textField: UITextField) {
    }
    
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if(textField == self.tfCurrPass) {
            self.tfNewPass.becomeFirstResponder()
            return false
        } else if(textField == self.tfNewPass) {
            self.tfConfirmPass.becomeFirstResponder()
            return false
        } else if(textField == self.tfConfirmPass) {
            textField.resignFirstResponder()
            return false
        }
        
        return true
    }
}
