//
//  ContentCell.swift
//  TheBooks
//
//  Created by Mojave on 8/20/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import UIKit
import SVProgressHUD

protocol ContentCellDelegate {
    func  MyInfoCellToBookDetail(_ lst : GiaoSach_DTO, _ index : Int)
}


class ContentCell: UICollectionViewCell {

    //MARK: - IBOULET
    
    @IBOutlet weak var tbvView : UITableView!
    @IBOutlet weak var loadingView : UIActivityIndicatorView!
    @IBOutlet weak var vPlank : CellPlank!
    
    //MARK: - DECLARE
    
    var lstData : [ThongTinMuonSach_DTO] = []
    var lstPhanCong : [GiaoSach_DTO] = []
    var index = 0
    var delegate : ContentCellDelegate!
    
    //MARK: - INIT
    
    override func awakeFromNib() {
        super.awakeFromNib()
        register()
        reloadData()
        loadingView.isHidden = true
        notification.add(self, #selector(setData), .ReloadData)
        setData()
    }
    
    //MARK: - REGISTER
    
    struct Indentifier {
        static let cell = "MyInfoCell"
        static let cell2 = "CellNhanVien"
    }
    
    func register() {
        if(dataManager.roles == "DocGia") {
            tbvView.register(UINib(nibName: Indentifier.cell, bundle: ProjectBundle), forCellReuseIdentifier: Indentifier.cell)
        } else {
            tbvView.register(UINib(nibName: Indentifier.cell2, bundle: ProjectBundle), forCellReuseIdentifier: Indentifier.cell2)
        }
        tbvView.delegate = self
        tbvView.dataSource = self
        tbvView.separatorStyle = .none
    }
    
    //MARK: - DARK/LIGHT + CHANGLANGUAGE CHANGE
    
    //MARK: - FUNC
    
    @objc func setData() {
        loadingView.isHidden = false
        loadingView.startAnimating()
        
        if(dataManager.roles == "DocGia") {
            let request = ThongTinMuonSach_Request()
            request.MaDG = dataManager.UserLogin.MaND
            
            if(index == 0) {
                DispatchQueue.global().async {
                    ServiceRequest.thongtinsachchomuon(request: request, success: { [weak self] (response) in
                        self?.loadingView.isHidden = true
                        self?.lstData = response
                        DispatchQueue.main.async {
                            self?.loadingView.stopAnimating()
                            self?.setupView()
                            self?.reloadData()
                        }
                    })
                }
            } else if(index == 1) {
                DispatchQueue.global().async {
                    ServiceRequest.thongtinsachdangmuon(request: request, success: { [weak self] (response) in
                        self?.loadingView.isHidden = true
                        self?.lstData = response
                        DispatchQueue.main.async {
                            self?.loadingView.stopAnimating()
                            self?.setupView()
                            self?.reloadData()
                        }
                    })
                }
            } else if(index == 2) {
                DispatchQueue.global().async {
                    ServiceRequest.thongtinsachquahan(request: request, success: { [weak self] (response) in
                        self?.loadingView.isHidden = true
                        self?.lstData = response
                        DispatchQueue.main.async {
                            self?.loadingView.stopAnimating()
                            self?.setupView()
                            self?.reloadData()
                        }
                    })
                }
            }
        
        } else {
            let request = GiaoSach_Request()
            request.MaNV = dataManager.UserLogin.MaND
            
            if(index == 0) {
                DispatchQueue.global().async {
                    ServiceRequest.giaosach(request: request, success: { [weak self] (response) in
                        self?.loadingView.isHidden = true
                        self?.lstPhanCong = response
                        DispatchQueue.main.async {
                            self?.loadingView.stopAnimating()
                            self?.setupView()
                            self?.reloadData()
                        }
                    })
                }
            } else if(index == 2) {
                DispatchQueue.global().async {
                    ServiceRequest.thusach(request: request, success: { [weak self] (response) in
                        self?.loadingView.isHidden = true
                        self?.lstPhanCong = response
                        DispatchQueue.main.async {
                            self?.loadingView.stopAnimating()
                            self?.setupView()
                            self?.reloadData()
                        }
                    })
                }
            }
        
        }//end if;
    }

    private func setupView() {
        if(lstData.count < 1 && lstPhanCong.count < 1) {
            vPlank.showView()
            self.vPlank.setData(colorTemplate.getImageDarkLight("bookplank"), "NotePlank".localized().replacingOccurrences(of: "{noun}", with: "BookInfo".localized()))
        } else {
            vPlank.hiddenView()
        }
    }
    
    @objc func reloadData() {
        tbvView.reloadData()
    }
    
    //MARK: - DEINIT
    
    deinit {
        
    }
}

extension ContentCell : UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath)
        
        if(index == 1 && dataManager.roles == "DocGia") {
            self.confirmDialog(self, "YÊU CẦU TRẢ SÁCH", "Bạn muốn trả cuốn sách này không ?", [("ĐÓNG", "ĐỒNG Ý")]) { (response) in
                if(response == 1){
                    
                    let request = YeuCauTraSach_Request()
                    request.MaDG = dataManager.UserLogin.MaND
                    request.MaS = self.lstData[indexPath.row].MaS
                    
                    DispatchQueue.global().async {
                        ServiceRequest.yeucautrasach(request: request, sucess: { (rs) in
                            self.successDialog(self, "Yêu cầu của bạn đã được gửi đi !" , success: { (index) in
                                cell?.isUserInteractionEnabled = false
                                cell?.alpha = 0.5
                            })
                        })
                    }
                }
            }
            
        } //end if
        
        if(dataManager.roles != "DocGia") {
            delegate.MyInfoCellToBookDetail(self.lstPhanCong[indexPath.row], self.index)
        }
    }
}

extension ContentCell : UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(dataManager.roles == "DocGia") {
            return lstData.count
        } else {
            return lstPhanCong.count
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if(dataManager.roles == "DocGia") {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: Indentifier.cell, for: indexPath) as! MyInfoCell
            
            cell.setData(lstData[indexPath.row])
            cell.selectionStyle = .none

            if(lstData[indexPath.row].YeuCauTra == 1 && self.index == 1 && dataManager.roles == "DocGia"){
                cell.contentView.alpha = 0.5
                cell.isUserInteractionEnabled = false
            } else {
                cell.contentView.alpha = 1
                cell.isUserInteractionEnabled = true
            }
            
            return cell
        } else{
            
            let cell = tableView.dequeueReusableCell(withIdentifier: Indentifier.cell2, for: indexPath) as! CellNhanVien
            
            cell.setData(lstPhanCong[indexPath.row])
            cell.selectionStyle = .none

            return cell
        }
    
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 165
    }
}
