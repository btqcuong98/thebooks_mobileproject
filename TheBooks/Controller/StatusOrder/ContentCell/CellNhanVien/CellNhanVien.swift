//
//  CellNhanVien.swift
//  TheBooks
//
//  Created by Mojave on 8/24/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import UIKit

class CellNhanVien: UITableViewCell {

    @IBOutlet weak var vContain: UIView!
    @IBOutlet weak var lblTitle1 : SkyLabel!
    @IBOutlet weak var lblTitle2 : SkyLabel!
    @IBOutlet weak var lblTitle3 : SkyLabel!
    @IBOutlet weak var lblTitle4 : SkyLabel!
    
    @IBOutlet weak var lblContent1 : SkyLabel!
    @IBOutlet weak var lblContent2 : SkyLabel!
    @IBOutlet weak var lblContent3 : SkyLabel!
    @IBOutlet weak var lblContent4 : SkyLabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        lblTitle1.font = colorTemplate.boldSize20
        lblContent1.font = colorTemplate.boldSize20
        
        lblTitle1.title = "Mã Phiếu"
        lblTitle2.title = "Tên Khách"
        lblTitle3.title = "Số ĐT"
        lblTitle4.title = "Địa Chỉ"
        self.selectionStyle = .none
        self.vContain.backgroundColor = colorTemplate.backgroundColCell
        self.vContain.shadow(radius: 15, width: 2, height: 2, opacity: 0.7, shaddowRadius: 1, color: .darkGray)
    }

    func setData(_ lst : GiaoSach_DTO) {
        lblContent1.title = "\(lst.MaPM)"
        lblContent2.title = lst.Ten
        lblContent3.title = lst.Sdt
        lblContent4.title = lst.DiaChi
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
