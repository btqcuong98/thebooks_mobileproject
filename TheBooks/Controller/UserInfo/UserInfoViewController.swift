//
//  UserInfoViewController.swift
//  TheBooks
//
//  Created by Mojave on 12/11/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import UIKit

class UserInfoViewController: MasterViewController {
    //MARK: - IBOULET
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var loadingView: UIActivityIndicatorView!
    @IBOutlet weak var imgAvata: UIImageView!
    @IBOutlet weak var lblFullName: SkyLabel!
    @IBOutlet weak var vContain: UIView!
    @IBOutlet weak var lblUserName: SkyLabel!
    @IBOutlet weak var lblUserNameData: SkyLabel!
    @IBOutlet weak var lblAddress: SkyLabel!
    @IBOutlet weak var lblAddressData: SkyLabel!
    @IBOutlet weak var lblDate: SkyLabel!
    @IBOutlet weak var lblDateData: SkyLabel!
    @IBOutlet weak var lblEmail: SkyLabel!
    @IBOutlet weak var lblEmailData: SkyLabel!
    @IBOutlet weak var lblPhone: SkyLabel!
    @IBOutlet weak var lblPhoneData: SkyLabel!
    @IBOutlet weak var btnChangePassword: SkyButton!
    
    //MARK: - DECLARE
    var alertController: UIAlertController?
    var pickerController: UIImagePickerController?
    
    //MARK: - INIT
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        setData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        darkLightMode()
        changeLanguage()
        self.loadingView.hiddenView()
    }
    
    //MARK: - DARK/LIGHT + CHANGLANGUAGE CHANGE
    override func darkLightMode() {
        super.darkLightMode()
        self.btnBack.setImage(colorTemplate.getImageDarkLight("close"), for: .normal)
        self.vContain.drawBorderRadius(radius: 10.0, color: colorTemplate.basketBackground, thickness: 1.0)
        self.lblUserNameData.drawBorderRadius(radius: 5.0, color: colorTemplate.basketBackground, thickness: 0.5)
        self.lblAddressData.drawBorderRadius(radius: 5.0, color: colorTemplate.basketBackground, thickness: 0.5)
        self.lblDateData.drawBorderRadius(radius: 5.0, color: colorTemplate.basketBackground, thickness: 0.5)
        self.lblEmailData.drawBorderRadius(radius: 5.0, color: colorTemplate.basketBackground, thickness: 0.5)
        self.lblPhoneData.drawBorderRadius(radius: 5.0, color: colorTemplate.basketBackground, thickness: 0.5)
        self.vContain.backgroundColor = colorTemplate.backgroundColCell
        _ = dataManager.getIsAvata(dataManager.UserLogin.MaND) != "" ? (self.imgAvata.image = dataManager.getIsAvata(dataManager.UserLogin.MaND).stringToImage()) : (self.imgAvata.image = colorTemplate.getImageDarkLight("profile"))
    }
    
    override func changeLanguage() {
        super.changeLanguage()
        self.setSimpleNaviTitle("UserInfo".localized())
        self.lblUserName.title = "UserName".localized()
        self.lblAddress.title = "Address".localized()
        self.lblDate.title = "Birthday".localized()
        self.lblEmail.title = "Email".localized()
        self.lblPhone.title = "Phone".localized()
        self.btnChangePassword.setTitle("ChangePass".localized(), for: .normal)
    }
    
    //MARK: - FUNC
    private func setupView() {
        self.imgAvata.layer.cornerRadius = self.imgAvata.frame.height/2
    }
    
    private func setData() {
        self.lblFullName.title = "   " + dataManager.UserLogin.HoTen
        self.lblUserNameData.title = "   " + dataManager.UserLogin.MaND
        self.lblAddressData.title = "   " + dataManager.UserLogin.DiaChi
        self.lblDateData.title = "   " + dataManager.UserLogin.NgaySinh
        self.lblEmailData.title = "   " + dataManager.UserLogin.Email
        self.lblPhoneData.title = "   " + dataManager.UserLogin.Sdt
    }
    
    //MARK: - ACTION
    @IBAction func BackTouch(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func ChangeAvataTouch(_ sender: UIButton) {
        self.tapChangeAvatar()
    }
    
    @IBAction func ChangePassword(_ sender: UIButton) {
        let vc = ChangePassView.init("ChangePassView")
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true)
    }
    
    @IBAction func PayPalTouch(_ sender: UIButton) {
    }
    
    //MARK: - DEINIT
    deinit {
    }
}

//MARK: - CAMERA OR CHOOSE IMAGE

extension UserInfoViewController : UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    @objc func tapChangeAvatar() {
        alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let cameraAction = UIAlertAction(title: "Chụp ảnh", style: .default) { (action) in
            self.openCamera()
        }
        let photoAction = UIAlertAction(title: "Chọn ảnh từ thư viện", style: .default) { (action) in
            self.openPhoto()
        }
        let cancelAction = UIAlertAction(title: "Huỷ", style: .cancel, handler: nil)
        alertController?.addAction(cameraAction)
        alertController?.addAction(photoAction)
        alertController?.addAction(cancelAction)
        self.present(alertController!, animated: true, completion: nil)
    }
    
    func openCamera() {
        self.loadingView.showView()
        self.loadingView.startAnimating()
        pickerController = UIImagePickerController()
        // check xem thiet bi co ho tro camera hay ko
        if(UIImagePickerController.isSourceTypeAvailable(.camera) == false) {
            return
        }
        pickerController!.delegate = self
        pickerController!.allowsEditing = true
        pickerController!.sourceType = .camera
        self.present(pickerController!, animated: true, completion: nil)
    }
    
    func openPhoto() {
        self.loadingView.showView()
        self.loadingView.startAnimating()
        pickerController = UIImagePickerController()
        pickerController!.delegate = self
        pickerController!.allowsEditing = true
        pickerController!.sourceType = .photoLibrary
        self.present(pickerController!, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        self.loadingView.hiddenView()
        self.loadingView.stopAnimating()
        let chosenImage: UIImage = info[UIImagePickerController.InfoKey.editedImage] as! UIImage
        
        if let avata = chosenImage.imageToString() {
            dataManager.saveIsAvata(avata)
        } else {
            self.view.errorDialog(self.view, "Không thể đổi Avata") { (_) in }
        }
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
}

