//
//  NewBooksViewController.swift
//  TheBooks
//
//  Created by Mojave on 7/8/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import UIKit

class NewBooksViewController: MasterViewController {

    //MARK: - IBOUTLET
    
    @IBOutlet weak var btnShowLeftMenu : UIButton!
    @IBOutlet weak var lblTopBook : SkyLabel!
    @IBOutlet weak var lblNewBook : SkyLabel!
    @IBOutlet weak var lblTopUser : SkyLabel!
    @IBOutlet weak var vTopBook : TopBookView!
    @IBOutlet weak var vNewBook : NewBookView!
    @IBOutlet weak var btnSearch: UIButton!
    @IBOutlet weak var btnBasket: UIButton!
    @IBOutlet weak var vBasket: UIView!
    @IBOutlet weak var lblBasket: SkyLabel!
    @IBOutlet weak var vTopReader: TopReaderView!
    
    //MARK: - DECALRE
    //MARK: - INIT
    
    override func viewDidLoad() {
        super.viewDidLoad()

        darkLightMode()
        changeLanguage()
        
        vTopBook.delegate = self
        vNewBook.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setUpView()
        
        if let topbook = vTopBook, let newbook = vNewBook{
            topbook.colView.reloadData()
            newbook.colView.reloadData()
        }
    }

    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    //MARK: - DARK/LIGHT + LANGUGAE CHANGE
    
    override func darkLightMode() {
        super.darkLightMode()
        
        btnShowLeftMenu.setImage(colorTemplate.getImageDarkLight("menu"), for: .normal)
        btnSearch.setImage(colorTemplate.getImageDarkLight("search"), for: .normal)
        btnBasket.setImage(colorTemplate.getImageDarkLight("cart"), for: .normal)
    }
    
    override func changeLanguage() {
        super.changeLanguage()
        
        self.setSimpleNaviTitle("Home".localized())
        lblTopBook.title = "TopBook".localized()
        lblNewBook.title = "NewBook".localized()
        lblTopUser.title = "TopUser".localized()
    }
    
    //MARK: - FLOATING
    
    private func setUpView() {
        let countBasket = dataManager.getBasketProduct(dataManager.UserLogin.MaND).count
        if(countBasket > 0) {
            self.vBasket.showView()
            self.vBasket.drawRadius()
            self.vBasket.backgroundColor = colorTemplate.redColor
            self.lblBasket.text = "\(countBasket)"
            self.lblBasket.textColor = colorTemplate.whiteColor
        } else {
            self.vBasket.hiddenView()
        }
    }
    
    //MARK: - IBACTION
    
    @IBAction func showLeftMenuTouch(_ sender : UIButton) {
        notification.post(.LeftMenuShow)
    }
    
    @IBAction func searchTouch(_ sender: Any) {
        let vc = SearchBookViewController.init("SearchBookViewController")
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func basketTouch(_ sender: Any) {
        let vc = BasketOrderViewController.init("BasketOrderViewController")
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
    }
    
    //MARK: - DEINIT
    
    deinit {
        
    }
}

extension NewBooksViewController : TopBookViewDelegate {
    func pushViewFromTopBook(_ lstData : DauSach_DTO) {
        if(dataManager.roles == "DocGia"){
            let vc = BookDetailViewController.init("BookDetailViewController")
            vc.modalPresentationStyle = .fullScreen
            vc.lstData = lstData
            self.present(vc, animated: false, completion: nil)
        }
        
    }
}

extension NewBooksViewController : NewBookViewDelegate {
    func pushViewFromNewBook(_ lstData : DauSach_DTO) {
        if(dataManager.roles == "DocGia"){
            let vc = BookDetailViewController.init("BookDetailViewController")
            vc.lstData = lstData
            self.present(vc, animated: false, completion: nil)
        }
        
    }
}
