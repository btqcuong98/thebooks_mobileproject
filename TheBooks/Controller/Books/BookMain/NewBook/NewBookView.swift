//
//  NewBookView.swift
//  TheBooks
//
//  Created by Mojave on 7/17/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import UIKit

protocol NewBookViewDelegate {
    func pushViewFromNewBook(_ lstData : DauSach_DTO)
}

class NewBookView: SkyView {
    
    //MARK: - IBOULET
    
    @IBOutlet weak var colView : UICollectionView!
    
    //MARK: - DECLARE
    var delegate : NewBookViewDelegate!
    
//    let lstNewBook = ["NguoiGiauCoNhatThanhBabylon", "DiTimLoiSong", "NguoiBanHangViDaiNhatTheGioi2", "BiMatCuaMayMan", "QuangGanhLoDivaVuiSong", "HanhTrinhVePhuongDong_Cold", "HanhTrinhVePhuongDong", "CamXucLaKeThuSoMotCuaThanhCong", "NguoiBanHangViDaiNhatTheGioi", "NguoiNamCham"]
    var lstNewBook : [DauSach_DTO] = []
    //MARK: - INIT
    
    override func initView() {
        super.initView()
        
        register()
        
        lstNewBook = dataManager.lstDauSach.filter({
            $0.SachMoi == 1
        })
        
    }
    
    //MARK: - DARK/LIGHT + CHANGLANGUAGE CHANGE
    
    //MARK: - REGISTER
    
    struct Indentifier {
        static let cell = "TopBookCell"
    }
    
    private func register() {
        colView.register(UINib.init(nibName: Indentifier.cell, bundle: ProjectBundle), forCellWithReuseIdentifier: Indentifier.cell)
        colView.delegate = self
        colView.dataSource = self
        colView.showsVerticalScrollIndicator = false
        colView.showsHorizontalScrollIndicator = false
        colView.backgroundColor = .clear
        colView.contentOffset = CGPoint.zero
    }
    
    //MARK: - FUNC
    
    
    //MARK: - DEINIT
    
    deinit {
        
    }

}

extension NewBookView : UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return lstNewBook.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Indentifier.cell, for: indexPath) as! TopBookCell
        cell.setData(lstNewBook[indexPath.item])
        return cell
        
    }
    

}

extension NewBookView : UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = collectionView.bounds.width
        let height = collectionView.bounds.height
        return CGSize.init(width: width * 0.75, height: height - 10)
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.init(top: 5, left: 0, bottom: 5, right: 10)
    }
    
}

extension NewBookView : UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        delegate.pushViewFromNewBook(lstNewBook[indexPath.item])
    }
}
