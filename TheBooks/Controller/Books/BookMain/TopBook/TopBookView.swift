//
//  TopBookView.swift
//  TheBooks
//
//  Created by Mojave on 7/16/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import UIKit

protocol TopBookViewDelegate {
    func pushViewFromTopBook(_ lstData : DauSach_DTO)
}

class TopBookView: SkyView {
    
    //MARK: - OUTLET
    
    @IBOutlet weak var colView : UICollectionView!
    
    //MARK: - DECLARE
    
    var lstTopBook : [DauSach_DTO] = []
    
    
    var count = 0
    var timer : Timer!
    
    var delegate : TopBookViewDelegate!
    
    //MARK: - INIT
    
    override func initView() {
        super.initView()
        
        self.backgroundColor = .clear
        register()
        
        timer = Timer.scheduledTimer(timeInterval: 2.0, target: self, selector: #selector(timerScroll), userInfo: nil, repeats: true)
        
        lstTopBook = dataManager.lstTop10DauSach
    }
    
    
    //MARK: - REGISTER
    
    struct Indentifier {
        static let cell = "TopBookCell"
    }
    
    func register() {
        colView.register(UINib.init(nibName: Indentifier.cell, bundle: ProjectBundle), forCellWithReuseIdentifier: Indentifier.cell)
        colView.delegate = self
        colView.dataSource = self
        colView.showsHorizontalScrollIndicator = false
        colView.showsVerticalScrollIndicator = false
        colView.backgroundColor = .clear
        colView.contentOffset = CGPoint.zero
    }
    
    //MARK: - DARK/LIGHT + LANGUGAE CHANGE
    
    override func darkLightMode() {
        super.darkLightMode()
        
        colView.reloadData()
    }
    
    //MARK: - AUTO SCROLL
    
    private func scrollView(_ isAnimated : Bool) {
        let index = IndexPath.init(item: count, section: 0)
        colView.scrollToItem(at: index, at: .centeredHorizontally, animated: isAnimated)
    }
    
    @objc private func timerScroll() {
        if(count < lstTopBook.count) {
            scrollView(true)
            count += 1
        } else {
            count = 0
            scrollView(false)
        }
    }

    //MARK: - DEINIT
    

}

extension TopBookView : UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return lstTopBook.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Indentifier.cell, for: indexPath) as! TopBookCell
        cell.setData(lstTopBook[indexPath.item])
        return cell
    }
    
    
}

extension TopBookView : UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = collectionView.bounds.width
        let height = collectionView.bounds.height
        return CGSize.init(width: width * 0.75 , height: height - 10)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.init(top: 5, left: 0, bottom: 5, right: 10)
    }
}

extension TopBookView : UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        delegate.pushViewFromTopBook(lstTopBook[indexPath.item])
    }
}
