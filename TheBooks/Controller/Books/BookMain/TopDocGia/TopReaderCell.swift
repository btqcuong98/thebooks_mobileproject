//
//  TopReaderCell.swift
//  TheBooks
//
//  Created by Mojave on 7/18/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import UIKit

class TopReaderCell: UICollectionViewCell {

    //MARK: - IBOULET
    
    @IBOutlet weak var contenView : UIView!
    @IBOutlet weak var imgImage : UIImageView!
    @IBOutlet weak var btnButton : UIButton!
    @IBOutlet weak var lblTitle : SkyLabel!
    @IBOutlet weak var heightImageContraint : NSLayoutConstraint!
    //MARK: - DECLARE
    
    
    //MARK: - INIT
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    //MARK: - DARK/LIGHT + CHANGLANGUAGE CHANGE
    //MARK: - FUNC
    
    func initView() {
        contenView.drawRadius()
        imgImage.drawRadius()
        btnButton.drawRadius()
        lblTitle.textAlignment = .center
    }
    
    func setUser(_ lst : Top10DocGia_DTO) {
        initView()
        lblTitle.text = lst.HoTen
        if dataManager.getIsAvata(lst.MaDG) != "" {
            self.imgImage.image = dataManager.getIsAvata(lst.MaDG).stringToImage()
            self.heightImageContraint.constant = 100
        } else {
            self.imgImage.image = colorTemplate.getImageDarkLight("userAva")
            self.heightImageContraint.constant = 60
        }
        contenView.backgroundColor = colorTemplate.basketBackground
    }
    
    //MARK: - DEINIT
    
    deinit {
    }
}
