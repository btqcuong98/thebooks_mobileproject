//
//  TopReaderView.swift
//  TheBooks
//
//  Created by Mojave on 7/18/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import UIKit

class TopReaderView: SkyView {

    //MARK: - IBOULET
    
    @IBOutlet weak var colView : UICollectionView!
    
    //MARK: - DECLARE

    var lstTopUser : [Top10DocGia_DTO] = []
    
    //MARK: - INIT
    
    override func initView() {
        super.initView()
        register()
        lstTopUser = dataManager.lstTop10DocGia
        self.colView.reloadData()
        self.colView.setNeedsLayout()
    }

    //MARK: - REGISTER
    struct Indentifier {
        static let cell = "TopReaderCell"
    }
    
    private func register() {
        colView.register(UINib.init(nibName: Indentifier.cell, bundle: ProjectBundle), forCellWithReuseIdentifier: Indentifier.cell)
        colView.delegate = self
        colView.dataSource = self
        colView.showsVerticalScrollIndicator = false
        colView.showsHorizontalScrollIndicator = false
        colView.backgroundColor = .clear
        colView.contentOffset = CGPoint.zero
    }
    
    //MARK: - DARK/LIGHT + CHANGLANGUAGE CHANGE
    //MARK: - FUNC
    //MARK: - DEINIT
    
    deinit {
        
    }

}

extension TopReaderView : UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return lstTopUser.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Indentifier.cell, for: indexPath) as! TopReaderCell
        cell.setUser(lstTopUser[indexPath.item])
        return cell
        
    }
}

extension TopReaderView : UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize.init(width: 100, height: 160)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.init(top: 0, left: 0, bottom: 0, right: 0)
    }
}

extension TopReaderView : UICollectionViewDelegate {

}
