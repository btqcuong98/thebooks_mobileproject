//
//  Cell2.swift
//  TheBooks
//
//  Created by Mojave on 8/4/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import UIKit

class Cell2: UITableViewCell {

    @IBOutlet weak var lblTitle : SkyLabel!
    @IBOutlet weak var btnComment : SkyButton!
    
    var block : (()->Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        notification.add(self, #selector(changeLanguage), .LanguageChange)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setData() {
        lblTitle.title = "Comment".localized()
        changeLanguage()
    }
    
    func isEnableCommentButton(_ mads: String) {
        let isComment = dataManager.getIsComment(mads)
        btnComment.isEnabled = !isComment
        btnComment.alpha = isComment ? 0.5 : 1.0
    }
    
    @objc func changeLanguage() {
        btnComment.setTitle("EnterComment".localized(), for: .normal)
    }
    
    func blockTouch(_ block : @escaping(()->Void)) {
        self.block = block
    }
    
    @IBAction func commentTouch(_ sender : UIButton) {
        self.block?()
    }
}
