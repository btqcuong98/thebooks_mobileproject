//
//  CellComment.swift
//  TheBooks
//
//  Created by Mojave on 8/4/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import UIKit


class CellComment: UITableViewCell {

    @IBOutlet weak var imgAva : UIImageView!
    @IBOutlet weak var lblUser : SkyLabel!
    @IBOutlet weak var img1 : UIImageView!
    @IBOutlet weak var img2 : UIImageView!
    @IBOutlet weak var img3 : UIImageView!
    @IBOutlet weak var img4 : UIImageView!
    @IBOutlet weak var img5 : UIImageView!
    @IBOutlet weak var lblDispt : SkyLabel!
    @IBOutlet weak var lblTime : SkyLabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setData(_ lst: DanhGia_DTO) {
        lblUser.title = lst.HoTen
        lblTime.title = utils.convertDateToString(lst.Ngay) 
        lblDispt.title = lst.BinhLuan
        setRating(lst.DanhGia)
    }
    
    private func setRating(_ rating : Int) {
        switch rating {
        case 0:
            img1.image = "star_false".image()
            img2.image = "star_false".image()
            img3.image = "star_false".image()
            img4.image = "star_false".image()
            img5.image = "star_false".image()
        case 1:
            img1.image = "star_true".image()
            img2.image = "star_false".image()
            img3.image = "star_false".image()
            img4.image = "star_false".image()
            img5.image = "star_false".image()
        case 2:
            img1.image = "star_true".image()
            img2.image = "star_true".image()
            img3.image = "star_false".image()
            img4.image = "star_false".image()
            img5.image = "star_false".image()
        case 3:
            img1.image = "star_true".image()
            img2.image = "star_true".image()
            img3.image = "star_true".image()
            img4.image = "star_false".image()
            img5.image = "star_false".image()
        case 4:
            img1.image = "star_true".image()
            img2.image = "star_true".image()
            img3.image = "star_true".image()
            img4.image = "star_true".image()
            img5.image = "star_false".image()
        case 5:
            img1.image = "star_true".image()
            img2.image = "star_true".image()
            img3.image = "star_true".image()
            img4.image = "star_true".image()
            img5.image = "star_true".image()
            
        default: break
            
        }
    }
}
