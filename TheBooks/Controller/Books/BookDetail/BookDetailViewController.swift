//
//  BookDetailViewController.swift
//  TheBooks
//
//  Created by Mojave on 8/3/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import UIKit

class BookDetailViewController: MasterViewController {

    //MARK: - IBOULET
    
    @IBOutlet weak var tbvView : UITableView!
    @IBOutlet weak var vFooterOrder: FooterOrderView?
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var btnConfirm: UIButton?
    @IBOutlet weak var btnGo: UIButton?
    @IBOutlet weak var vStack: UIView?
    @IBOutlet weak var heightForFooter: NSLayoutConstraint?
    @IBOutlet weak var heightStack: NSLayoutConstraint?
    
    //MARK: - DECLARE

    var lstData : DauSach_DTO = DauSach_DTO()
    var lstCommentRating: [DanhGia_DTO] = []
    var index = -1 // truyền từ myinfo 0 : giao || 1: nhận
    
    //MARK: - INIT
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        register()
        changeLanguage()
        darkLightMode()
        
        if(dataManager.roles == "DocGia"){
            footerOderTouch()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if(dataManager.roles == "DocGia") {
            if(checkExitInBasket()) {
                self.vFooterOrder?.setTitle("Ordered".localized())
            }
            
            self.vFooterOrder?.showView()
            self.vStack?.hiddenView()
            
            //get rating
            let request = DanhGia_Request()
            request.MaDS = lstData.MaDS
            
            DispatchQueue.global().async {
                ServiceRequest.danhgia(request: request) { (data) in
                    self.lstCommentRating = data
                    self.tbvView.reloadData()
                }
            }
            
        } else {
            
            self.vFooterOrder?.hiddenView()
            self.vStack?.showView()
            
            if(index == 0){
                self.btnConfirm?.setTitle("XÁC NHẬN ĐÃ GIAO", for: .normal)
            } else {
                self.btnConfirm?.setTitle("XÁC NHẬN ĐÃ THU", for: .normal)
            }
            
            self.btnGo?.setTitle("CHỈ ĐƯỜNG", for: .normal)
            
            self.btnGo?.backgroundColor = .blue
            self.btnConfirm?.backgroundColor = .red
            self.btnGo?.setTitleColor(colorTemplate.whiteColor, for: .normal)
            self.btnConfirm?.setTitleColor(colorTemplate.whiteColor, for: .normal)
        
        }
        reloadTableView()
    }
    
    //MARK: - REGISTER
    
    struct Indentifier {
        static let cell1 = "Cell1"
        static let cell2 = "Cell2"
        static let cellComment = "CellComment"
    }
    
    private func register() {
        tbvView.register(UINib(nibName: Indentifier.cell1, bundle: ProjectBundle), forCellReuseIdentifier: Indentifier.cell1)
        tbvView.register(UINib(nibName: Indentifier.cell2, bundle: ProjectBundle), forCellReuseIdentifier: Indentifier.cell2)
        tbvView.register(UINib(nibName: Indentifier.cellComment, bundle: ProjectBundle), forCellReuseIdentifier: Indentifier.cellComment)
        
        tbvView.delegate = self
        tbvView.dataSource = self
        
        tbvView.separatorStyle = .none
    }
    
    //MARK: - DARK/LIGHT + CHANGLANGUAGE CHANGE
    
    override func changeLanguage() {
        super.changeLanguage()
        
        self.setSimpleNaviTitle("DetailBook".localized())
        vFooterOrder?.setTitle("Order".localized())
    }
    
    override func darkLightMode() {
        super.darkLightMode()
        
        btnBack.setImage(colorTemplate.getImageDarkLight("back"), for: .normal)
    }
    
    //MARK: - FUNC
    
    func reloadTableView() {
        self.tbvView.reloadData()
    }
    
    //MARK: - ACTION
    
    @IBAction func confirmTouch(_ sender : UIButton) {

    }
    
    @IBAction func dierectionTouch(_ sender : UIButton) {
    }
    
    func checkExitInBasket() -> Bool{
        for item in dataManager.Basket {
            if(item.MaDS == self.lstData.MaDS) {
                return true
            }
        }
        return false
    }
    
    func footerOderTouch() {
        
        weak var weakself = self
        self.vFooterOrder?.blockTouch(){
            
            //truoc khi them vao, kiem tra xem co ton tai chua
            
            if (!(weakself!.checkExitInBasket())) {
                
                //save to struct basket
                let basket = Basket(self.lstData.MaDS, self.lstData.Ten, self.lstData.AnhBia, self.lstData.MoTa, self.lstData.GiaSach, self.lstData.MaxRating, self.lstData.Rating, self.lstData.TacGia)
                
                dataManager.Basket.removeAll()
                dataManager.Basket.append(basket)
                
                dataManager.saveBasketProduct(dataManager.Basket, dataManager.UserLogin.MaND)
                
                
                //end
                
                weakself!.vFooterOrder?.setTitle("Ordered".localized())
                
            } else {
                weakself!.vFooterOrder?.setTitle("OrderBook".localized())
            }
            
            self.view.confirmDialog(self.view, "XÁC NHẬN", "Mời chọn một trong hai chức năng sau!", [("XEM THÊM", "ĐẾN GIỎ HÀNG")], block: { (index) in
                if(index == 0) {
                    self.dismiss(animated: true, completion: nil)
                } else if(index == 1) {
                    let vc = BasketOrderViewController.init("BasketOrderViewController")
                    vc.modalPresentationStyle = .fullScreen
                    self.present(vc, animated: true, completion: nil)
                }
            })
        }
    }
    
    @IBAction func backTouch(_ sender: UIButton) {
        self.view.window?.rootViewController?.dismiss(animated: false, completion: nil)
    }
    
    //MARK: - DEINIT
    
    deinit {
        
    }
}

extension BookDetailViewController : UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
}

extension BookDetailViewController : UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return lstCommentRating.count + 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if(indexPath.row == 0) {
            let cell = tableView.dequeueReusableCell(withIdentifier: Indentifier.cell1, for: indexPath) as! Cell1
            
            cell.setData(lstData)
            cell.selectionStyle = .none
            cell.blockTouch() {
                self.reloadTableView()
            }
            
            return cell
        } else if(indexPath.row == 1){
            let cell = tableView.dequeueReusableCell(withIdentifier: Indentifier.cell2, for: indexPath) as! Cell2
            cell.isEnableCommentButton(self.lstData.MaDS)
            //lắng nghe bình luận action
            cell.blockTouch(){
                _ = self.view.commentDialog(self.view, block: { (index) in
                    if(index == 1) {
                        
                        //gọi api save comment
                        let request = DanhGia_Request()
                        request.MaDG = dataManager.UserLogin.MaND
                        request.MaDS = self.lstData.MaDS
                        request.BinhLuan = tvTextStr
                        request.DanhGia = star.filter{$0 == true}.count
                        
                        DispatchQueue.global().async {
                            ServiceRequest.themdanhgia(request: request) { [weak self] (response) in
                                self?.lstCommentRating.insert(response[0], at: 0)
                                dataManager.saveIsComment(self?.lstData.MaDS ?? "", true)
                                //cập nhật lại data
                                for i in 0 ..< dataManager.lstDauSach.count {
                                    if dataManager.lstDauSach[i].MaDS == response[0].MaDS {
                                        dataManager.lstDauSach[i].MaxRating = response[0].MaxDanhGia
                                        dataManager.lstDauSach[i].Rating = response[0].CountMaxDanhGia
                                    }
                                }
                                self?.lstData.MaxRating = response[0].MaxDanhGia
                                self?.lstData.Rating = response[0].CountMaxDanhGia
                                DispatchQueue.main.async {
                                    self?.tbvView.reloadData()
                                }
                            }
                        }
                        star = [false, false, false, false, false]
                        
                    } else {
                        //xử lý tắt popup
                    }
                })
            }
            
            //set data
            cell.setData()
            cell.selectionStyle = .none
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: Indentifier.cellComment, for: indexPath) as! CellComment
            cell.setData(lstCommentRating[indexPath.row - 2])
            cell.selectionStyle = .none
            
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        if(lstCommentRating[indexPath.row - 2].MaDG == dataManager.UserLogin.MaND) {
            let deleteAction = UIContextualAction.init(style: .normal, title: "") { (action, view, completionHandler) in
                let request = DanhGia_Request()
                request.MaDG = dataManager.UserLogin.MaND
                request.MaDS = self.lstData.MaDS
                
                DispatchQueue.global().async {
                    ServiceRequest.xoadanhgia(request: request) { [weak self] (response, code) in
                        self?.lstCommentRating = self?.lstCommentRating.filter{$0.MaDG != dataManager.UserLogin.MaND} ?? [DanhGia_DTO]()
                        //cập nhật lại data
                        for i in 0 ..< dataManager.lstDauSach.count {
                            if dataManager.lstDauSach[i].MaDS == response[0].MaDS {
                                dataManager.lstDauSach[i].MaxRating = response[0].MaxDanhGia
                                dataManager.lstDauSach[i].Rating = response[0].CountMaxDanhGia
                            }
                        }
                        self?.lstData.MaxRating = response[0].MaxDanhGia
                        self?.lstData.Rating = response[0].CountMaxDanhGia
                        if code == 200 {
                            self?.view.successDialog(self?.view ?? UIView(), "Xoá Đánh Giá Thành Công !") { (_) in
                                dataManager.saveIsComment(self?.lstData.MaDS ?? "", false)
                                self?.tbvView.reloadData()
                            }
                        }
                    }
                }
            }
            
            deleteAction.backgroundColor = .red
            deleteAction.image = "bin_dark".image()
            let configuration = UISwipeActionsConfiguration(actions: [deleteAction])
            return configuration
        } else {
            let swipeAction = UISwipeActionsConfiguration(actions: [])
            return swipeAction
        }
    }
    
}

