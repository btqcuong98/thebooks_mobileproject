//
//  FooterOrderView.swift
//  TheBooks
//
//  Created by Mojave on 8/5/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import UIKit

class FooterOrderView: SkyView {

    @IBOutlet weak var btnOrder : SkyButton!
    var block : (()->Void)?
    
    override func initView() {
        super.initView()
        
        initStyle()
        //notification.post(.RemoveFloatingMenu)
    }

    func initStyle() {
        btnOrder.drawBorderRadius(radius: 0, color: .clear, thickness: 0)
    }
    
    func setTitle(_ title : String) {
        btnOrder.setTitle(title, for: .normal)
    }
    
    func blockTouch(_ block : @escaping(()->Void)) {
        self.block = block
    }
    
    @IBAction func btnOrderTouch(_ sender : UIButton) {
        self.block?()
    }
}
