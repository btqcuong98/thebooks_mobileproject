//
//  BasketOrderViewController.swift
//  TheBooks
//
//  Created by Mojave on 7/9/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import UIKit

class BasketOrderViewController: MasterViewController {

    //MARK: - OUTLET
    
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var tbvView: UITableView!
    @IBOutlet weak var btnDeleteAll: UIButton!
    @IBOutlet weak var vFooterOrder: FooterOrderView!
    
    //MARK: - DECLARE
    
    private var lstData : [Basket] = []
    
    //MARK: - INIT
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setSimpleNaviTitle("Basket".localized())
        darkLightMode()
        changeLanguage()
        register()
        footerOderTouch()
        self.hidesBottomBarWhenPushed = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        lstData = dataManager.getBasketProduct(dataManager.UserLogin.MaND)
        processOrderButton()
        reloadData()
    }
    
    
    //MARK : REGISTER
    
    struct Indentifier {
        static let cell = "BasketCell"
    }
    
    func register() {
        tbvView.register(UINib(nibName: Indentifier.cell, bundle: ProjectBundle), forCellReuseIdentifier: Indentifier.cell)
        tbvView.delegate = self
        tbvView.dataSource = self
        tbvView.separatorStyle = .none
    }
    
    //MARK: - DARK/LIGHT + LANGUGAE CHANGE
    
    override func changeLanguage() {
        super.changeLanguage()
        
        vFooterOrder.setTitle("OrderBook".localized())
    }
    
    override func darkLightMode() {
        super.darkLightMode()   
        
        btnBack.setImage(colorTemplate.getImageDarkLight("close"), for: .normal)
        btnDeleteAll.setImage(colorTemplate.getImageDarkLight("bin"), for: .normal)
    }
    
    //MARK: - FUNC

    func reloadData() {
        self.tbvView.reloadData()
        processOrderButton()
    }
    
    func processOrderButton(){
        
        if(lstData.count < 1) {
            vFooterOrder.isUserInteractionEnabled = false
            vFooterOrder.alpha = 0.5
        } else {
            vFooterOrder.isUserInteractionEnabled = true
            vFooterOrder.alpha = 1
        }
    }
    
    //MARK: - ACTION
    
    func footerOderTouch() {
        let vc = OrderBookViewController.init("OrderBookViewController")
        self.vFooterOrder.blockTouch(){
            vc.lstBasket = self.lstData
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: false, completion: nil)
        }
    }
    
    @IBAction func deleteAllTouch(_ sender: UIButton) {
        self.view.confirmDialog(self.view, "XÁC NHẬN", "Bạn thật sự muốn xoá TẤT CẢ trong giỏ hàng ?", [("ĐÓNG", "CHẤP NHẬN")]) { (index) in
            if(index == 1) {
                dataManager.Basket.removeAll()
                dataManager.removeBasketProductLocal(dataManager.UserLogin.MaND)
                self.lstData.removeAll()
                dataManager.lstBasket.removeAll()
                self.reloadData()
            }
        }
    }
    
    
    @IBAction func backTouch(_ sender: Any) {
        //self.dismiss(animated: true, completion: nil)
        self.view.window!.rootViewController?.dismiss(animated: false, completion: nil)
    }
    
}

//MARK: - EXTENSION

extension BasketOrderViewController : UITableViewDelegate {
    
}

extension BasketOrderViewController : UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return lstData.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 165
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Indentifier.cell, for: indexPath) as! BasketCell
        cell.selectionStyle = .none
        cell.setData(lstData[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let deleteAction = UIContextualAction.init(style: .normal, title: "") { (action, view, completionHandler) in
            //completionHandler(true)
            self.lstData.remove(at: indexPath.row)
            dataManager.removeBasketProductLocal(dataManager.UserLogin.MaND)
            dataManager.saveBasketProduct(self.lstData, dataManager.UserLogin.MaND)
            //dataManager.Basket = self.lstData
            self.reloadData()
        }
        
        deleteAction.backgroundColor = .red
        deleteAction.image = "bin_dark".image()
        let configuration = UISwipeActionsConfiguration(actions: [deleteAction])
        return configuration
    }
    
}

