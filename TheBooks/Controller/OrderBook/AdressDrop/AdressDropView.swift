//
//  AdressDropView.swift
//  TheBooks
//
//  Created by Mojave on 8/16/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import UIKit

class AdressDropView: SkyView {

    //MARK: - IBOULET
    
    @IBOutlet weak var btnDrop : UIButton!
    @IBOutlet weak var lblAddress : SkyLabel!
    
    //MARK: - DECLARE
    
    var block : ((UIView)->Void)!
    
    //MARK: - INIT
    
    override func initView() {
        super.initView()
    }
    
    //MARK: - DARK/LIGHT + CHANGLANGUAGE CHANGE
    //MARK: - FUNC
    
    func setBlock(_ block : @escaping((UIView)->Void)) {
        self.block = block
    }
    
    //MARK: - IBACTION
    
    @IBAction private func touchDrop(_ sender : UIButton) {
        self.block?(self)
    }
    
    //MARK: - DEINIT
   
    deinit {
        
    }

}
