//
//  DistrictItem.swift
//  TheBooks
//
//  Created by Mojave on 8/16/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import Foundation

class DistrictItem: NSObject {
    var district = ""
    var ward : [DistrictItem] = []
    
    override init() {
        super.init()
    }
    
    init(district : String) {
        self.district = district
    }
    
    func add ( _ ward : [DistrictItem]) {
        self.ward.append(contentsOf: ward)
    }
}
