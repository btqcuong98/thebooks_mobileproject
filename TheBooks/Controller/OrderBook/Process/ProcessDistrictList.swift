//
//  ProcessDistrictList.swift
//  TheBooks
//
//  Created by Mojave on 8/16/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import Foundation

class ProcessDistrictList: NSObject {
    
    class func getListDistrict() -> [DistrictItem] {
        var lstData : [DistrictItem] = []
        lstData.append(getDistrict1())
        lstData.append(getDistrict2())
        lstData.append(getDistrict3())
        lstData.append(getDistrict4())
        lstData.append(getDistrict5())
        lstData.append(getDistrict6())
        lstData.append(getDistrict7())
        lstData.append(getDistrict8())
        lstData.append(getDistrict9())
        lstData.append(getDistrict10())
        lstData.append(getDistrict11())
        lstData.append(getDistrict12())
        lstData.append(getDistrictTanPhu())
        lstData.append(getDistrictTanBinh())
        lstData.append(getDistrictBinhTan())
        lstData.append(getDistrictBinhThanh())
        lstData.append(getDistrictGoVap())
        lstData.append(getDistrictPhuNhuan())
        lstData.append(getDistrictThuDuc())
        
        return lstData
    }

    class func getDistrict1() -> DistrictItem {

        let result = DistrictItem.init(district: "Quận 1")

        var chils = [DistrictItem]()
        
        
        let item1 = DistrictItem.init(district: "Phường Bến Nghé")
        let item2 = DistrictItem.init(district: "Phường Cầu Kho")
        let item3 = DistrictItem.init(district: "Phường Cầu Ông Lãnh")
        let item4 = DistrictItem.init(district: "Phường Cô Giang")
        let item5 = DistrictItem.init(district: "Phường Đa Kao")
        let item6 = DistrictItem.init(district: "Phường Nguyễn Cư Trinh")
        let item7 = DistrictItem.init(district: "Phường Nguyễn Thái Bình")
        let item8 = DistrictItem.init(district: "Phường Phạm Ngũ Lão")
        let item9 = DistrictItem.init(district: "Phường Tân Định")
        let item10 = DistrictItem.init(district: "Phường Bến Thành")
        
        chils.append(item1); chils.append(item2); chils.append(item3); chils.append(item4)
        chils.append(item5); chils.append(item6); chils.append(item7); chils.append(item8)
        chils.append(item9); chils.append(item10)

        result.add(chils)

        return result

    }

    class func getDistrict2() -> DistrictItem {
        
        let result = DistrictItem.init(district: "Quận 2")
        
        var chils = [DistrictItem]()
        
        let item1 = DistrictItem.init(district: "Phường An Khánh")
        let item2 = DistrictItem.init(district: "Phường An Lợi Đông")
        let item3 = DistrictItem.init(district: "Phường An Phú")
        let item4 = DistrictItem.init(district: "Phường Bình An")
        let item5 = DistrictItem.init(district: "Phường Bình Khánh")
        let item6 = DistrictItem.init(district: "Phường Bình Trưng Đông")
        let item7 = DistrictItem.init(district: "Phường Bình Trưng Tây")
        let item8 = DistrictItem.init(district: "Phường Cát Lái")
        let item9 = DistrictItem.init(district: "Phường Thạnh Mỹ Lợi")
        let item10 = DistrictItem.init(district: "Phường Thảo Điền")
        let item11 = DistrictItem.init(district: "Phường Thủ Thiêm")
        
        chils.append(item1); chils.append(item2); chils.append(item3); chils.append(item4)
        chils.append(item5); chils.append(item6); chils.append(item7); chils.append(item8)
        chils.append(item9); chils.append(item10); chils.append(item11)
        
        result.add(chils)
        
        return result
    }
    
    class func getDistrict3() -> DistrictItem {
        
        let result = DistrictItem.init(district: "Quận 3")
        
        var chils = [DistrictItem]()
        
        let item1 = DistrictItem.init(district: "Phường 01")
        let item2 = DistrictItem.init(district: "Phường 02")
        let item3 = DistrictItem.init(district: "Phường 03")
        let item4 = DistrictItem.init(district: "Phường 04")
        let item5 = DistrictItem.init(district: "Phường 05")
        let item6 = DistrictItem.init(district: "Phường 06")
        let item7 = DistrictItem.init(district: "Phường 07")
        let item8 = DistrictItem.init(district: "Phường 08")
        let item9 = DistrictItem.init(district: "Phường 09")
        let item10 = DistrictItem.init(district: "Phường 10")
        let item11 = DistrictItem.init(district: "Phường 11")
        let item12 = DistrictItem.init(district: "Phường 12")
        let item13 = DistrictItem.init(district: "Phường 13")
        let item14 = DistrictItem.init(district: "Phường 14")
        
        chils.append(item1); chils.append(item2); chils.append(item3); chils.append(item4)
        chils.append(item5); chils.append(item6); chils.append(item7); chils.append(item8)
        chils.append(item9); chils.append(item10); chils.append(item11); chils.append(item12);  chils.append(item13); chils.append(item14);
    
        result.add(chils)
        
        return result
    }
    
    class func getDistrict4() -> DistrictItem {
        
        let result = DistrictItem.init(district: "Quận 4")
        
        var chils = [DistrictItem]()
        
        let item1 = DistrictItem.init(district: "Phường 01")
        let item2 = DistrictItem.init(district: "Phường 02")
        let item3 = DistrictItem.init(district: "Phường 03")
        let item4 = DistrictItem.init(district: "Phường 04")
        let item5 = DistrictItem.init(district: "Phường 05")
        let item6 = DistrictItem.init(district: "Phường 06")
        let item7 = DistrictItem.init(district: "Phường 07")
        let item8 = DistrictItem.init(district: "Phường 08")
        let item9 = DistrictItem.init(district: "Phường 09")
        let item10 = DistrictItem.init(district: "Phường 10")
        let item11 = DistrictItem.init(district: "Phường 11")
        let item12 = DistrictItem.init(district: "Phường 12")
        let item13 = DistrictItem.init(district: "Phường 13")
        let item14 = DistrictItem.init(district: "Phường 14")
        let item15 = DistrictItem.init(district: "Phường 15")
        let item16 = DistrictItem.init(district: "Phường 16")
        let item17 = DistrictItem.init(district: "Phường 17")
        let item18 = DistrictItem.init(district: "Phường 18")
        
        chils.append(item1); chils.append(item2); chils.append(item3); chils.append(item4)
        chils.append(item5); chils.append(item6); chils.append(item7); chils.append(item8)
        chils.append(item9); chils.append(item10); chils.append(item11);chils.append(item12);  chils.append(item13); chils.append(item14);chils.append(item15);  chils.append(item16); chils.append(item17);chils.append(item18)
        
        result.add(chils)
        
        return result
    }
    
    class func getDistrict5() -> DistrictItem {
        
        let result = DistrictItem.init(district: "Quận 5")
        
        var chils = [DistrictItem]()
        
        let item1 = DistrictItem.init(district: "Phường 01")
        let item2 = DistrictItem.init(district: "Phường 02")
        let item3 = DistrictItem.init(district: "Phường 03")
        let item4 = DistrictItem.init(district: "Phường 04")
        let item5 = DistrictItem.init(district: "Phường 05")
        let item6 = DistrictItem.init(district: "Phường 06")
        let item7 = DistrictItem.init(district: "Phường 07")
        let item8 = DistrictItem.init(district: "Phường 08")
        let item9 = DistrictItem.init(district: "Phường 09")
        let item10 = DistrictItem.init(district: "Phường 10")
        let item11 = DistrictItem.init(district: "Phường 11")
        let item12 = DistrictItem.init(district: "Phường 12")
        let item13 = DistrictItem.init(district: "Phường 13")
        let item14 = DistrictItem.init(district: "Phường 14")
        let item15 = DistrictItem.init(district: "Phường 15")
        
        chils.append(item1); chils.append(item2); chils.append(item3); chils.append(item4)
        chils.append(item5); chils.append(item6); chils.append(item7); chils.append(item8)
        chils.append(item9); chils.append(item10); chils.append(item11);chils.append(item12);  chils.append(item13); chils.append(item14);chils.append(item15)
        
        result.add(chils)
        
        return result
    }
    
    class func getDistrict6() -> DistrictItem {
        
        let result = DistrictItem.init(district: "Quận 6")
        
        var chils = [DistrictItem]()
        
        let item1 = DistrictItem.init(district: "Phường 01")
        let item2 = DistrictItem.init(district: "Phường 02")
        let item3 = DistrictItem.init(district: "Phường 03")
        let item4 = DistrictItem.init(district: "Phường 04")
        let item5 = DistrictItem.init(district: "Phường 05")
        let item6 = DistrictItem.init(district: "Phường 06")
        let item7 = DistrictItem.init(district: "Phường 07")
        let item8 = DistrictItem.init(district: "Phường 08")
        let item9 = DistrictItem.init(district: "Phường 09")
        let item10 = DistrictItem.init(district: "Phường 10")
        let item11 = DistrictItem.init(district: "Phường 11")
        let item12 = DistrictItem.init(district: "Phường 12")
        let item13 = DistrictItem.init(district: "Phường 13")
        let item14 = DistrictItem.init(district: "Phường 14")
        
        chils.append(item1); chils.append(item2); chils.append(item3); chils.append(item4)
        chils.append(item5); chils.append(item6); chils.append(item7); chils.append(item8)
        chils.append(item9); chils.append(item10); chils.append(item11); chils.append(item12);  chils.append(item13); chils.append(item14);
        
        result.add(chils)
        
        return result
    }
    
    class func getDistrict7() -> DistrictItem { //chu
        
        let result = DistrictItem.init(district: "Quận 7")
        
        var chils = [DistrictItem]()
        
        let item1 = DistrictItem.init(district: "Phường An Khánh")
        let item2 = DistrictItem.init(district: "Phường An Lợi Đông")
        let item3 = DistrictItem.init(district: "Phường An Phú")
        let item4 = DistrictItem.init(district: "Phường Bình An")
        let item5 = DistrictItem.init(district: "Phường Bình Khánh")
        let item6 = DistrictItem.init(district: "Phường Bình Trưng Đông")
        let item7 = DistrictItem.init(district: "Phường Bình Trưng Tây")
        let item8 = DistrictItem.init(district: "Phường Cát Lái")
        let item9 = DistrictItem.init(district: "Phường Thạnh Mỹ Lợi")
        let item10 = DistrictItem.init(district: "Phường Thảo Điền")
        let item11 = DistrictItem.init(district: "Phường Thủ Thiêm")
        
        chils.append(item1); chils.append(item2); chils.append(item3); chils.append(item4)
        chils.append(item5); chils.append(item6); chils.append(item7); chils.append(item8)
        chils.append(item9); chils.append(item10); chils.append(item11)
        
        result.add(chils)
        
        return result
    }
    
    class func getDistrict8() -> DistrictItem {
        
        let result = DistrictItem.init(district: "Quận 8")
        
        var chils = [DistrictItem]()
        
        let item1 = DistrictItem.init(district: "Phường 01")
        let item2 = DistrictItem.init(district: "Phường 02")
        let item3 = DistrictItem.init(district: "Phường 03")
        let item4 = DistrictItem.init(district: "Phường 04")
        let item5 = DistrictItem.init(district: "Phường 05")
        let item6 = DistrictItem.init(district: "Phường 06")
        let item7 = DistrictItem.init(district: "Phường 07")
        let item8 = DistrictItem.init(district: "Phường 08")
        let item9 = DistrictItem.init(district: "Phường 09")
        let item10 = DistrictItem.init(district: "Phường 10")
        let item11 = DistrictItem.init(district: "Phường 11")
        let item12 = DistrictItem.init(district: "Phường 12")
        let item13 = DistrictItem.init(district: "Phường 13")
        let item14 = DistrictItem.init(district: "Phường 14")
        let item15 = DistrictItem.init(district: "Phường 15")
        let item16 = DistrictItem.init(district: "Phường 16")
        
        chils.append(item1); chils.append(item2); chils.append(item3); chils.append(item4)
        chils.append(item5); chils.append(item6); chils.append(item7); chils.append(item8)
        chils.append(item9); chils.append(item10); chils.append(item11);chils.append(item12);  chils.append(item13); chils.append(item14);chils.append(item15);  chils.append(item16)
        
        result.add(chils)
        
        return result
    }
    
    class func getDistrict9() -> DistrictItem { //ch
        
        let result = DistrictItem.init(district: "Quận 9")
        
        var chils = [DistrictItem]()
        
        let item1 = DistrictItem.init(district: "Phường An Khánh")
        let item2 = DistrictItem.init(district: "Phường An Lợi Đông")
        let item3 = DistrictItem.init(district: "Phường An Phú")
        let item4 = DistrictItem.init(district: "Phường Bình An")
        let item5 = DistrictItem.init(district: "Phường Bình Khánh")
        let item6 = DistrictItem.init(district: "Phường Bình Trưng Đông")
        let item7 = DistrictItem.init(district: "Phường Bình Trưng Tây")
        let item8 = DistrictItem.init(district: "Phường Cát Lái")
        let item9 = DistrictItem.init(district: "Phường Thạnh Mỹ Lợi")
        let item10 = DistrictItem.init(district: "Phường Thảo Điền")
        let item11 = DistrictItem.init(district: "Phường Thủ Thiêm")
        
        chils.append(item1); chils.append(item2); chils.append(item3); chils.append(item4)
        chils.append(item5); chils.append(item6); chils.append(item7); chils.append(item8)
        chils.append(item9); chils.append(item10); chils.append(item11)
        
        result.add(chils)
        
        return result
    }
    
    class func getDistrict10() -> DistrictItem {
        
        let result = DistrictItem.init(district: "Quận 10")
        
        var chils = [DistrictItem]()
        
        let item1 = DistrictItem.init(district: "Phường 01")
        let item2 = DistrictItem.init(district: "Phường 02")
        let item3 = DistrictItem.init(district: "Phường 03")
        let item4 = DistrictItem.init(district: "Phường 04")
        let item5 = DistrictItem.init(district: "Phường 05")
        let item6 = DistrictItem.init(district: "Phường 06")
        let item7 = DistrictItem.init(district: "Phường 07")
        let item8 = DistrictItem.init(district: "Phường 08")
        let item9 = DistrictItem.init(district: "Phường 09")
        let item10 = DistrictItem.init(district: "Phường 10")
        let item11 = DistrictItem.init(district: "Phường 11")
        let item12 = DistrictItem.init(district: "Phường 12")
        let item13 = DistrictItem.init(district: "Phường 13")
        let item14 = DistrictItem.init(district: "Phường 14")
        let item15 = DistrictItem.init(district: "Phường 15")
        
        chils.append(item1); chils.append(item2); chils.append(item3); chils.append(item4)
        chils.append(item5); chils.append(item6); chils.append(item7); chils.append(item8)
        chils.append(item9); chils.append(item10); chils.append(item11);chils.append(item12);  chils.append(item13); chils.append(item14);chils.append(item15)
        
        result.add(chils)
        
        return result
    }
    
    class func getDistrict11() -> DistrictItem {
        
        let result = DistrictItem.init(district: "Quận 11")
        
        var chils = [DistrictItem]()
        
        let item1 = DistrictItem.init(district: "Phường 01")
        let item2 = DistrictItem.init(district: "Phường 02")
        let item3 = DistrictItem.init(district: "Phường 03")
        let item4 = DistrictItem.init(district: "Phường 04")
        let item5 = DistrictItem.init(district: "Phường 05")
        let item6 = DistrictItem.init(district: "Phường 06")
        let item7 = DistrictItem.init(district: "Phường 07")
        let item8 = DistrictItem.init(district: "Phường 08")
        let item9 = DistrictItem.init(district: "Phường 09")
        let item10 = DistrictItem.init(district: "Phường 10")
        let item11 = DistrictItem.init(district: "Phường 11")
        let item12 = DistrictItem.init(district: "Phường 12")
        let item13 = DistrictItem.init(district: "Phường 13")
        let item14 = DistrictItem.init(district: "Phường 14")
        let item15 = DistrictItem.init(district: "Phường 15")
        let item16 = DistrictItem.init(district: "Phường 16")
        
        chils.append(item1); chils.append(item2); chils.append(item3); chils.append(item4)
        chils.append(item5); chils.append(item6); chils.append(item7); chils.append(item8)
        chils.append(item9); chils.append(item10); chils.append(item11);chils.append(item12);  chils.append(item13); chils.append(item14);chils.append(item15);  chils.append(item16)
        
        result.add(chils)
        
        return result
    }
    
    class func getDistrict12() -> DistrictItem { //ch tro  
        
        let result = DistrictItem.init(district: "Quận 12")
        
        var chils = [DistrictItem]()
        
        let item1 = DistrictItem.init(district: "Phường An Khánh")
        let item2 = DistrictItem.init(district: "Phường An Lợi Đông")
        let item3 = DistrictItem.init(district: "Phường An Phú")
        let item4 = DistrictItem.init(district: "Phường Bình An")
        let item5 = DistrictItem.init(district: "Phường Bình Khánh")
        let item6 = DistrictItem.init(district: "Phường Bình Trưng Đông")
        let item7 = DistrictItem.init(district: "Phường Bình Trưng Tây")
        let item8 = DistrictItem.init(district: "Phường Cát Lái")
        let item9 = DistrictItem.init(district: "Phường Thạnh Mỹ Lợi")
        let item10 = DistrictItem.init(district: "Phường Thảo Điền")
        let item11 = DistrictItem.init(district: "Phường Thủ Thiêm")
        
        chils.append(item1); chils.append(item2); chils.append(item3); chils.append(item4)
        chils.append(item5); chils.append(item6); chils.append(item7); chils.append(item8)
        chils.append(item9); chils.append(item10); chils.append(item11)
        
        result.add(chils)
        
        return result
    }
    
    class func getDistrictBinhTan() -> DistrictItem {
        
        let result = DistrictItem.init(district: "Quận Bình Tân")
        
        var chils = [DistrictItem]()
        
        let item1 = DistrictItem.init(district: "Phường An Khánh")
        let item2 = DistrictItem.init(district: "Phường An Lợi Đông")
        let item3 = DistrictItem.init(district: "Phường An Phú")
        let item4 = DistrictItem.init(district: "Phường Bình An")
        let item5 = DistrictItem.init(district: "Phường Bình Khánh")
        let item6 = DistrictItem.init(district: "Phường Bình Trưng Đông")
        let item7 = DistrictItem.init(district: "Phường Bình Trưng Tây")
        let item8 = DistrictItem.init(district: "Phường Cát Lái")
        let item9 = DistrictItem.init(district: "Phường Thạnh Mỹ Lợi")
        let item10 = DistrictItem.init(district: "Phường Thảo Điền")
        let item11 = DistrictItem.init(district: "Phường Thủ Thiêm")
        
        chils.append(item1); chils.append(item2); chils.append(item3); chils.append(item4)
        chils.append(item5); chils.append(item6); chils.append(item7); chils.append(item8)
        chils.append(item9); chils.append(item10); chils.append(item11)
        
        result.add(chils)
        
        return result
    }
    
    class func getDistrictBinhThanh() -> DistrictItem {
        
        let result = DistrictItem.init(district: "Quận BìnhThạnh ")
        
        var chils = [DistrictItem]()
        
        let item1 = DistrictItem.init(district: "Phường An Khánh")
        let item2 = DistrictItem.init(district: "Phường An Lợi Đông")
        let item3 = DistrictItem.init(district: "Phường An Phú")
        let item4 = DistrictItem.init(district: "Phường Bình An")
        let item5 = DistrictItem.init(district: "Phường Bình Khánh")
        let item6 = DistrictItem.init(district: "Phường Bình Trưng Đông")
        let item7 = DistrictItem.init(district: "Phường Bình Trưng Tây")
        let item8 = DistrictItem.init(district: "Phường Cát Lái")
        let item9 = DistrictItem.init(district: "Phường Thạnh Mỹ Lợi")
        let item10 = DistrictItem.init(district: "Phường Thảo Điền")
        let item11 = DistrictItem.init(district: "Phường Thủ Thiêm")
        
        chils.append(item1); chils.append(item2); chils.append(item3); chils.append(item4)
        chils.append(item5); chils.append(item6); chils.append(item7); chils.append(item8)
        chils.append(item9); chils.append(item10); chils.append(item11)
        
        result.add(chils)
        
        return result
    }
    
    class func getDistrictGoVap() -> DistrictItem {
        
        let result = DistrictItem.init(district: "Quận Gò Vấp")
        
        var chils = [DistrictItem]()
        
        let item1 = DistrictItem.init(district: "Phường An Khánh")
        let item2 = DistrictItem.init(district: "Phường An Lợi Đông")
        let item3 = DistrictItem.init(district: "Phường An Phú")
        let item4 = DistrictItem.init(district: "Phường Bình An")
        let item5 = DistrictItem.init(district: "Phường Bình Khánh")
        let item6 = DistrictItem.init(district: "Phường Bình Trưng Đông")
        let item7 = DistrictItem.init(district: "Phường Bình Trưng Tây")
        let item8 = DistrictItem.init(district: "Phường Cát Lái")
        let item9 = DistrictItem.init(district: "Phường Thạnh Mỹ Lợi")
        let item10 = DistrictItem.init(district: "Phường Thảo Điền")
        let item11 = DistrictItem.init(district: "Phường Thủ Thiêm")
        
        chils.append(item1); chils.append(item2); chils.append(item3); chils.append(item4)
        chils.append(item5); chils.append(item6); chils.append(item7); chils.append(item8)
        chils.append(item9); chils.append(item10); chils.append(item11)
        
        result.add(chils)
        
        return result
    }
    
    class func getDistrictPhuNhuan() -> DistrictItem {
        
        let result = DistrictItem.init(district: "Quận Phú Nhuận")
        
        var chils = [DistrictItem]()
        
        let item1 = DistrictItem.init(district: "Phường An Khánh")
        let item2 = DistrictItem.init(district: "Phường An Lợi Đông")
        let item3 = DistrictItem.init(district: "Phường An Phú")
        let item4 = DistrictItem.init(district: "Phường Bình An")
        let item5 = DistrictItem.init(district: "Phường Bình Khánh")
        let item6 = DistrictItem.init(district: "Phường Bình Trưng Đông")
        let item7 = DistrictItem.init(district: "Phường Bình Trưng Tây")
        let item8 = DistrictItem.init(district: "Phường Cát Lái")
        let item9 = DistrictItem.init(district: "Phường Thạnh Mỹ Lợi")
        let item10 = DistrictItem.init(district: "Phường Thảo Điền")
        let item11 = DistrictItem.init(district: "Phường Thủ Thiêm")
        
        chils.append(item1); chils.append(item2); chils.append(item3); chils.append(item4)
        chils.append(item5); chils.append(item6); chils.append(item7); chils.append(item8)
        chils.append(item9); chils.append(item10); chils.append(item11)
        
        result.add(chils)
        
        return result
    }
    
    class func getDistrictTanBinh() -> DistrictItem {
        
        let result = DistrictItem.init(district: "Quận Tân Bình")
        
        var chils = [DistrictItem]()
        
        let item1 = DistrictItem.init(district: "Phường An Khánh")
        let item2 = DistrictItem.init(district: "Phường An Lợi Đông")
        let item3 = DistrictItem.init(district: "Phường An Phú")
        let item4 = DistrictItem.init(district: "Phường Bình An")
        let item5 = DistrictItem.init(district: "Phường Bình Khánh")
        let item6 = DistrictItem.init(district: "Phường Bình Trưng Đông")
        let item7 = DistrictItem.init(district: "Phường Bình Trưng Tây")
        let item8 = DistrictItem.init(district: "Phường Cát Lái")
        let item9 = DistrictItem.init(district: "Phường Thạnh Mỹ Lợi")
        let item10 = DistrictItem.init(district: "Phường Thảo Điền")
        let item11 = DistrictItem.init(district: "Phường Thủ Thiêm")
        
        chils.append(item1); chils.append(item2); chils.append(item3); chils.append(item4)
        chils.append(item5); chils.append(item6); chils.append(item7); chils.append(item8)
        chils.append(item9); chils.append(item10); chils.append(item11)
        
        result.add(chils)
        
        return result
    }
    
    class func getDistrictTanPhu() -> DistrictItem {
        
        let result = DistrictItem.init(district: "Quận Tân Phú")
        
        var chils = [DistrictItem]()
        
        let item1 = DistrictItem.init(district: "Phường An Khánh")
        let item2 = DistrictItem.init(district: "Phường An Lợi Đông")
        let item3 = DistrictItem.init(district: "Phường An Phú")
        let item4 = DistrictItem.init(district: "Phường Bình An")
        let item5 = DistrictItem.init(district: "Phường Bình Khánh")
        let item6 = DistrictItem.init(district: "Phường Bình Trưng Đông")
        let item7 = DistrictItem.init(district: "Phường Bình Trưng Tây")
        let item8 = DistrictItem.init(district: "Phường Cát Lái")
        let item9 = DistrictItem.init(district: "Phường Thạnh Mỹ Lợi")
        let item10 = DistrictItem.init(district: "Phường Thảo Điền")
        let item11 = DistrictItem.init(district: "Phường Thủ Thiêm")
        
        chils.append(item1); chils.append(item2); chils.append(item3); chils.append(item4)
        chils.append(item5); chils.append(item6); chils.append(item7); chils.append(item8)
        chils.append(item9); chils.append(item10); chils.append(item11)
        
        result.add(chils)
        
        return result
    }
    
    class func getDistrictThuDuc() -> DistrictItem {
        
        let result = DistrictItem.init(district: "Quận Thủ Đức")
        
        var chils = [DistrictItem]()
        
        let item1 = DistrictItem.init(district: "Phường An Khánh")
        let item2 = DistrictItem.init(district: "Phường An Lợi Đông")
        let item3 = DistrictItem.init(district: "Phường An Phú")
        let item4 = DistrictItem.init(district: "Phường Bình An")
        let item5 = DistrictItem.init(district: "Phường Bình Khánh")
        let item6 = DistrictItem.init(district: "Phường Bình Trưng Đông")
        let item7 = DistrictItem.init(district: "Phường Bình Trưng Tây")
        let item8 = DistrictItem.init(district: "Phường Cát Lái")
        let item9 = DistrictItem.init(district: "Phường Thạnh Mỹ Lợi")
        let item10 = DistrictItem.init(district: "Phường Thảo Điền")
        let item11 = DistrictItem.init(district: "Phường Thủ Thiêm")
        
        chils.append(item1); chils.append(item2); chils.append(item3); chils.append(item4)
        chils.append(item5); chils.append(item6); chils.append(item7); chils.append(item8)
        chils.append(item9); chils.append(item10); chils.append(item11)
        
        result.add(chils)
        
        return result
    }
    
        
}
