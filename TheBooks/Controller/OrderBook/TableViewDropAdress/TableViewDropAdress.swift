//
//  TableViewDropAdress.swift
//  TheBooks
//
//  Created by Mojave on 8/16/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import UIKit

class TableViewDropAdress: SkyView {
    
    //MARK: - IBOULET
    
    @IBOutlet weak var tbvView : UITableView!
    
    //MARK: - DECLARE
    
    var lstAddress : [DistrictItem] = []
    var block : ((String, Int)->Void)!
    
    
    //MARK: - INIT
    
    override func initView() {
        super.initView()
        
        changeLanguage()
        darkLightMode()
        register()
        
    }
    
    //MARK: - REGISTER
    
    struct Indentifier {
        static let cell = "AddressDropCell"
    }
    
    func register() {
        self.tbvView.register(UINib.init(nibName: Indentifier.cell, bundle: ProjectBundle), forCellReuseIdentifier: Indentifier.cell)
        tbvView.delegate = self
        tbvView.dataSource = self
        tbvView.separatorStyle = .none
    }
    
    //MARK: - DARK/LIGHT + CHANGLANGUAGE CHANGE
    
    override func changeLanguage() {
        super.changeLanguage()
    }
    
    override func darkLightMode() {
        super.darkLightMode()
        //self.view.backgroundColor = colorTemplate.tabbarBackground
    }
    
    //MARK: - FUNC
    
    func blockTouch(_ block : @escaping ((String, Int)->Void)) {
        self.block = block
    }
    
    
    //MARK: - DEINIT
    
    deinit {
        
    }
   
}


extension TableViewDropAdress : UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.block?(lstAddress[indexPath.row].district, indexPath.row)
        
    }
}

extension TableViewDropAdress : UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return lstAddress.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Indentifier.cell, for: indexPath) as! AddressDropCell
        cell.setData(lstAddress[indexPath.row].district)
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }
}
