//
//  AddressDropCell.swift
//  TheBooks
//
//  Created by Mojave on 8/16/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import UIKit

class AddressDropCell: UITableViewCell {

    @IBOutlet weak var lblText : SkyLabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        lblText.textColor = .white
    }
    
    
    func setData(_ str : String) {
        lblText.title = str
    }
    
}
