//
//  OrderBookViewController.swift
//  TheBooks
//
//  Created by Mojave on 8/13/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import UIKit
import SVProgressHUD

class OrderBookViewController: MasterViewController {

    //MARK: - IBOULET
    
    @IBOutlet weak var vContain: UIView!
    
    @IBOutlet weak var lblDistrict: SkyLabel!
    @IBOutlet weak var lblWard: SkyLabel!
    @IBOutlet weak var lblAddress: SkyLabel!
    @IBOutlet weak var lblName: SkyLabel!
    @IBOutlet weak var lblPhone: SkyLabel!
    @IBOutlet weak var lblNote: SkyLabel!
    @IBOutlet weak var btnOrderBook: SkyButton!
    
    @IBOutlet weak var targetDistrict: UIView!
    @IBOutlet weak var targetWard: UIView!
    
    @IBOutlet weak var vDistrictDrop: AdressDropView!
    @IBOutlet weak var vWardDrop: AdressDropView!
    @IBOutlet weak var tfAddress: SkyTextFiled!
    @IBOutlet weak var tfName: SkyTextFiled!
    @IBOutlet weak var tfPhone: SkyTextFiled!
    @IBOutlet weak var tfNote: SkyTextFiled!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var loadingView: UIActivityIndicatorView!
    
    //MARK: - DECLARE
    
    var lstBasket : [Basket] = []
    var lstDistrict : [DistrictItem] = []
    var index = -1
    var lstTextField : [UITextField] = []
    var districtText = ""
    var wardText = ""
    
    //MARK: - INIT
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        darkLightMode()
        changeLanguage()
        lstDistrict = ProcessDistrictList.getListDistrict()
        dropAddress()
        lstTextField = [tfAddress, tfName, tfPhone]
        self.tfAddress.delegate = self
        self.tfName.delegate = self
        self.tfPhone.delegate = self
        self.tfNote.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupCustomerInfo()
        self.loadingView.isHidden = true
    }
    
    //MARK: - DARK/LIGHT + CHANGLANGUAGE CHANGE
    
    override func darkLightMode() {
        super.darkLightMode()
        vContain.drawBorderRadius(radius: 10.0, color: colorTemplate.basketBackground, thickness: 0.5)
        vContain.backgroundColor = colorTemplate.backgroundColCell
        vDistrictDrop.drawBorderRadius(radius: 4.0, color: colorTemplate.lineColor, thickness: 1.5)
        vWardDrop.drawBorderRadius(radius: 4.0, color: colorTemplate.lineColor, thickness: 1.5)
        btnBack.setImage(colorTemplate.getImageDarkLight("close"), for: .normal)
    }
    
    override func changeLanguage() {
        super.changeLanguage()
        
        self.setSimpleNaviTitle("InfoOrder".localized())
        lblDistrict.title = "District".localized()
        lblWard.title = "Ward".localized()
        lblAddress.title = "Address".localized()
        lblName.title = "Name".localized()
        lblPhone.title = "Phone".localized()
        lblNote.title = "Note".localized()
        btnOrderBook.setTitle("Complete".localized(), for: .normal)
    }
    
    func alert(_ code: Int, _ rs : String) {
        self.view.messageDialog(self.view, rs) {(_) in
            if(code == 200) {
                self.view.window!.rootViewController?.dismiss(animated: false, completion: nil)
            } else {
                return
            }
        }
        return
    }
    
    private func setupCustomerInfo() {
        let userInfo = dataManager.UserLogin
        let addressArr = userInfo.DiaChi.components(separatedBy: ",")
        self.vDistrictDrop.lblAddress.title = addressArr[2]
        self.districtText = addressArr[2]
        self.vWardDrop.lblAddress.title = addressArr[1]
        self.wardText = addressArr[1]
        self.tfAddress.text = addressArr[0]
        self.tfName.text = userInfo.HoTen
        self.tfPhone.text = userInfo.Sdt
    }
    
    //MARK: - ACTION
    
    @IBAction func OrderTouch(_ sender: UIButton) {
        var lstMaDS : [String] = []
        
        for i in 0 ..< lstBasket.count {
            lstMaDS.append(lstBasket[i].MaDS)
        }
        
        if(checkEmpty()) {
            let request = MuonSach_Request()
            request.MaDS = lstMaDS
            request.MaDG = dataManager.UserLogin.MaND
            request.Ten = tfName.text!
            request.Sdt = tfPhone.text!
            request.DiaChi = ("\(tfAddress.text!), \(wardText) - \(districtText)")
            request.SoSachMuon = lstBasket.count
            
            self.loadingView.isHidden = false
            self.loadingView.startAnimating()
            DispatchQueue.global().async {
                ServiceRequest.muonsach(request: request, success: { [weak self] (code, response) in
                    DispatchQueue.main.async {
                        self?.loadingView.stopAnimating()
                        self?.alert(code, response)
                        self?.loadingView.isHidden = true
                        if(code == 200) {
                            dataManager.removeBasketProductLocal(dataManager.UserLogin.MaND)
                        }
                    }
                })
            }
        }
    }
    
    @IBAction func BackTouch(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
        //self.view.window!.rootViewController?.dismiss(animated: false, completion: nil)
    }
    
    //MARK: - FUNC
    
    func dropAddress() {
        
        let vc = TableViewDropAdress()
        
        vDistrictDrop.setBlock(){(view) in
            
            PopTip.openPopTipViewNoDismiss("", message: "", contentSize: CGSize(width: 150, height: 300), myView: vc, targetView: self.targetDistrict, containerView: self.view)
            
            vc.lstAddress = self.lstDistrict
            
            vc.blockTouch() {(text, index) in
                self.vDistrictDrop.lblAddress.title = text
                self.index = index
                self.districtText = text
            }
            
            
        }
        
        
        vWardDrop.setBlock() {(view) in

            let vc = TableViewDropAdress.init()
            
            PopTip.openPopTipViewNoDismiss("", message: "", contentSize: CGSize(width: 230, height: 250), myView: vc, targetView: self.targetWard, containerView: self.view)
            
            if(self.index >= 0) {
                vc.lstAddress = self.lstDistrict[self.index].ward
            }
            
            
            vc.blockTouch() {(text, index) in
                self.vWardDrop.lblAddress.title = text
                self.wardText = text
            }
        }
        
    }
    
    func checkEmptyTextField() -> Bool {
        for i in 0 ..< lstTextField.count {
            if(lstTextField[i].text == "") {
                self.view.errorDialog(self.view, "Không được để trống trường bắt buộc") { (index) in
                    if(index == 0) {
                        self.lstTextField[i].becomeFirstResponder()
                    }
                }
                return false
            }
        }
        
        return true
    }
    
    func  checkEmpty() -> Bool {
        if(districtText == "") {
            self.view.errorDialog(self.view, "Không được để trống Quận") { (index) in
            }
            return false
        }
        if(wardText == "") {
            self.view.errorDialog(self.view, "Không được để trống Phường") { (index) in
            }
            return false
        }
        return checkEmptyTextField()
    }
    
    
    //MARK: - DEINIT

    deinit {
        
    }

}

extension OrderBookViewController : UITextFieldDelegate {
    public func textFieldDidBeginEditing(_ textField: UITextField) {
    }
    
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if(textField == self.tfAddress) {
            self.tfName.becomeFirstResponder()
            return false
        } else if(textField == self.tfName) {
            self.tfPhone.becomeFirstResponder()
            return false
        } else if(textField == self.tfPhone) {
            self.tfNote.becomeFirstResponder()
            return false
        } else if(textField == self.tfNote) {
            textField.resignFirstResponder()
        }
        
        return true
    }
}

