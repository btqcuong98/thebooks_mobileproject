//
//  AppDelegate.swift
//  TheBooks
//
//  Created by Mojave on 7/5/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import GoogleMaps
import GooglePlaces

let appDelegate = AppDelegate.share
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    static let share = AppDelegate()
    
    var window: UIWindow?
    //var floating : FloatingMenu?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        window = UIWindow.init(frame: UIScreen.main.bounds)
        
        GMSServices.provideAPIKey("AIzaSyA4KheR7Cy1nFQKQZLlYSqEHco25vEb9vY")
        GMSPlacesClient.provideAPIKey("AIzaSyA4KheR7Cy1nFQKQZLlYSqEHco25vEb9vY")
        
        let vc = LoginViewController.init(nibName: String(describing: LoginViewController.self), bundle: nil)
        let nvc = UINavigationController.init(rootViewController: vc)
        nvc.navigationBar.isHidden = true
        
        _ = DataManager.shareInstance()
        
        self.window?.rootViewController = nvc
        self.window?.makeKeyAndVisible()
        
        notification.add(self, #selector(languageChange), .LanguageChange)
        notification.add(self, #selector(darklightChange), .DarkModeChange)
        notification.add(self, #selector(LoginSucess), .LoginSucess)
        notification.add(self, #selector(LogoutSucess), .LogoutSucess)
        initStyle()
        setUpKeyBoard()
        return true
    }


    func initStyle() {
        colorTemplate.processDarkLightChange()
    }
    
    @objc func LoginSucess() {
        let vc = HomeMainViewController.init(nibName: String(describing: HomeMainViewController.self), bundle: nil)
        let nvc = UINavigationController.init(rootViewController: vc)
        nvc.navigationBar.isHidden = true
        self.window?.rootViewController = nvc
        self.window?.makeKeyAndVisible()
    }
    
    @objc func LogoutSucess() {
        let vc = LoginViewController.init()
        let nvc = UINavigationController.init(rootViewController: vc)
        nvc.navigationBar.isHidden = true
        
        nvc.view.removeFromSuperview()
        notification.remove(self, .LeftMenuShow)
        self.window?.rootViewController = nvc
        self.window?.makeKeyAndVisible()
    }
    
    func setUpKeyBoard() {
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.enableAutoToolbar = true
        darklightChange()
        languageChange()
        IQKeyboardManager.shared.keyboardDistanceFromTextField = 5.0
        IQKeyboardManager.shared.shouldResignOnTouchOutside = true
    }

    
    @objc func darklightChange() {
        if(colorTemplate.isDarkMode){
            IQKeyboardManager.shared.keyboardAppearance = .light
        } else {
            IQKeyboardManager.shared.keyboardAppearance = .dark
        }
    }
    
    @objc func languageChange() {
        IQKeyboardManager.shared.toolbarDoneBarButtonItemText = "Done".localized()
    }
}



