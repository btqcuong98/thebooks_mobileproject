//
//  TabbarViewController.swift
//  TheBooks
//
//  Created by Mojave on 7/8/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import UIKit

class TabbarViewController: MasterViewController {

    //MARK: - OUTLET
    
    //MARK: - DECLARE
    
    var tabbar = UITabBarController()
    
    var vc1 = UINavigationController.init(rootViewController: NewBooksViewController.init("NewBooksViewController"))
    var vc2 = UINavigationController.init(rootViewController: HistoryOrderViewController.init("HistoryOrderViewController"))
    var vc3 = UINavigationController.init(rootViewController: StatusOrderViewController.init("StatusOrderViewController"))
    var vc4 = UINavigationController.init(rootViewController: UserInfoViewController.init("UserInfoViewController"))
    var vc5 = UINavigationController.init(rootViewController: LibraryInfoViewController.init("LibraryInfoViewController"))
    
    //MARK: - INIT
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setUpTabbar()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    //MARK: - DARK/LIGHT MODE + CHANGE LANGUAGE
    
    override func darkLightMode() {
        super.darkLightMode()
        
        setColorForTabbar()
    }
    
    override func changeLanguage() {
        super.changeLanguage()
    }
    
    //MARK: - TABBAR
    
    private func setUpTabbar() {
        self.view.addSubview(tabbar.view)
        self.view.setFullLayout(tabbar.view)
        self.addChild(tabbar)
        
        tabbar.viewControllers = [vc1, vc3, vc4, vc2, vc5]
        tabbar.tabBar.isTranslucent = false
        setColorForTabbar()
        setIconForTabbar()
    }

    private func setIconForTabbar() {
        vc1.tabBarItem = UITabBarItem.init(title: "".localized(), image: "book".image(), selectedImage: "book".image())
        vc1.tabBarItem.imageInsets = UIEdgeInsets(top: 10, left: 0, bottom: -10, right: 0)
        
        vc2.tabBarItem = UITabBarItem.init(title: "", image: "history".image(), selectedImage: "history".image())
        vc2.tabBarItem.imageInsets = UIEdgeInsets(top: 10, left: 0, bottom: -10, right: 0)
        
        vc3.tabBarItem = UITabBarItem.init(title: "", image: "invoices".image(), selectedImage: "invoices".image())
        vc3.tabBarItem.imageInsets = UIEdgeInsets(top: 10, left: 0, bottom: -10, right: 0)
        
        vc4.tabBarItem = UITabBarItem.init(title: "", image: "profile".image(), selectedImage: "profile".image())
        vc4.tabBarItem.imageInsets = UIEdgeInsets(top: 10, left: 0, bottom: -10, right: 0)
        
        vc5.tabBarItem = UITabBarItem.init(title: "", image: "info".image(), selectedImage: "info".image())
        vc5.tabBarItem.imageInsets = UIEdgeInsets(top: 10, left: 0, bottom: -10, right: 0)
    }
    
    private func setColorForTabbar() {
        tabbar.tabBar.barTintColor = colorTemplate.tabbarBackground //mauf cua tabbar
        tabbar.tabBar.tintColor = colorTemplate.tintTabbarColor //khi chon vao
        tabbar.tabBar.unselectedItemTintColor =  colorTemplate.unSelectTabbarColor //khi bt, khong chon
    }
    
    //MARK: - DEINIT
    
    deinit {
    }
}

