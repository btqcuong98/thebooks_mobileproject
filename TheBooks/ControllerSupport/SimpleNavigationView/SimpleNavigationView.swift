//
//  SimpleNavigationView.swift => status 4 constraint and main 3 constraint (có 1 constraint trùng)
//  TheBooks
//
//  Created by Mojave on 7/5/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import UIKit

enum SimpleNaviType {
    case back
    case menu
    case none
}

class SimpleNavigationView: SkyView {

    //MARK: - OUTLET
    
    @IBOutlet weak var containView : UIView!
    @IBOutlet weak var statusView : UIView!
    @IBOutlet weak var mainView : UIView!
    @IBOutlet weak var leftView : UIView!
    @IBOutlet weak var centerView : UIView!
    @IBOutlet weak var rightView : UIView!
    @IBOutlet weak var rightViewImage : UIImageView!
    @IBOutlet weak var lblTile : UILabel!
    @IBOutlet weak var btnLeft : UIButton!
    @IBOutlet weak var btnRight : UIButton!
    @IBOutlet weak var spacingLeftlblTile : NSLayoutConstraint!
    @IBOutlet weak var widthLeftView : NSLayoutConstraint!
    @IBOutlet weak var widthRightView : NSLayoutConstraint!
    @IBOutlet weak var hightMainView : NSLayoutConstraint!
    @IBOutlet weak var hightStatusView : NSLayoutConstraint!
    
    //MARK: - DECLARE
    
    private var backNaviBlock: (()->Void)!
    
    //MARK: - INIT
    
    override func initView() {
        super.initView()
        
        self.sendSubviewToBack(view) //lấy skyview để ra phía sau navigation
        
        darkLightMode()
        loadNaviConstraint()
    }
    
    
    //MARK: - CONSTRAINT
    
    private func loadNaviConstraint() {
        loadStatusBar()
        processShowLeftView(false)
        processShowRightView(false)
        lblTile.textAlignment = .center
        lblTile.text = ""
    }
    
    private func loadStatusBar() {
        if(devide.is_Rabit()) {
            hightStatusView.constant = 35.0 //do không có safe area + 20
            hightMainView.constant = 44.0
        } else {
            hightStatusView.constant = 20.0 //do không có safe area
            hightMainView.constant = 44.0
        }
    }
    
    private func processShowLeftView(_ isShow : Bool) {
        if(isShow) {
            showNavi()
            showLeftView()
        } else {
            hiddenLeftView()
        }
    }
    
    private func processShowRightView(_ isShow : Bool) {
        if(isShow) {
            showNavi()
            showRightView()
        } else {
            hiddenRightView()
        }
    }
    
    //MARK: - METHOD
    
    private var _title = ""
    @IBInspectable var title : String {
        get{

            return lblTile.text ?? ""

        } set {

            _title = newValue
            updateText()
        }
    }
    
    private func updateText() {
        lblTile.text = _title.uppercased()
        lblTile.font = colorTemplate.boldSize18
    }
    
    func setNavi(_ type : SimpleNaviType, _ title : Any?, block: @escaping (()->Void)) {
        backNaviBlock = block
        switch type {
        case .none:
            processShowLeftView(false)
        default:
            processShowLeftView(true)
        }
        
        if(title is String) {
            self.title = title as? String ?? ""
        } else {
            if(title is NSMutableAttributedString) {
                self.lblTile.attributedText = title as! NSMutableAttributedString
            }
        }
    }
    
    func setUpDarkLight() {
        lblTile.textColor = colorTemplate.textColor
        btnLeft.setImage(colorTemplate.getImageDarkLight("back"), for: .normal)
    }
    
    
    //MARK: - HIDDEN
    
    func hiddenNavi() {
        containView.isHidden = true
    }
    
    
    func hiddenLeftView () {
        leftView.isHidden = true
        btnLeft.isUserInteractionEnabled = false
    }
    
    func hiddenRightView() {
        rightView.isHidden = true
        btnRight.isUserInteractionEnabled = false
    }
    
    //MARK: - SHOW
    
    func showNavi() {
        containView.isHidden = false
    }
    
    func showLeftView() {
        leftView.isHidden = false
        btnLeft.isUserInteractionEnabled = true
    }
    
    func showRightView() {
        rightView.isHidden = false
        btnRight.isUserInteractionEnabled = true
    }
    
    //MARK: - DARK/LIGHT MODE + CHANGE LANGUAGE
    
    override func darkLightMode() {
        super.darkLightMode()
        
        setUpDarkLight()
    }
    
    override func changeLanguage() {
        super.changeLanguage()
    }
    
    //MARK: - ACTION
    
    @IBAction private func BackNaviTouch(_ sender : UIButton) {
        backNaviBlock()
    }
    
    //MARK: - DEINIT
    
    deinit {
    }
    
}
