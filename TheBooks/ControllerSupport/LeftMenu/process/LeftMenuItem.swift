//
//  LeftMenuItem.swift
//  TheBooks
//
//  Created by Mojave on 7/23/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import Foundation

class LeftMenuItem: NSObject {
    var itemName = ""
    var viewcontroller : AnyClass?
    var chils : [LeftMenuItem] = []
    var isSelected = false
    
    override init() {
        super.init()
    }
    
    init(itemName : String, viewcontroller : AnyClass?) {
        self.itemName = itemName
        self.viewcontroller = viewcontroller
    }
    
    func add ( _ chils : [LeftMenuItem]) {
        self.chils.append(contentsOf: chils)
    }
}
