////
////  ProcessLeftMenuData.swift
////  TheBooks
////
////  Created by Mojave on 7/23/20.
////  Copyright © 2020 Mojave. All rights reserved.
////
//
//import Foundation
//
//class ProcessLeftMenuData: NSObject {
//
//    class func getChildStory() -> LeftMenuItem {
//
//        let TLSACH = dataManager.lstTheLoaiSach //BSC
//        let TLCON = dataManager.lstTheLoaiCon
//
//        let result = LeftMenuItem.init(itemName: "\(TLSACH[0].MaTLS)".localized(), viewcontroller: nil)
//
//        var chils = [LeftMenuItem]()
//
//        let item1 = LeftMenuItem.init(itemName: "\(TLCON[0].MaTLC)".localized(), viewcontroller: SearchBookViewController.self)
//        let item2 = LeftMenuItem.init(itemName: "\(TLCON[1].MaTLC)".localized(), viewcontroller: SearchBookViewController.self)
//        let item3 = LeftMenuItem.init(itemName: "\(TLCON[2].MaTLC)".localized(), viewcontroller: SearchBookViewController.self)
//
//        chils.append(item1)
//        chils.append(item2)
//        chils.append(item3)
//
//        result.add(chils)
//
//        return result
//
//    }
//
//    class func getEconomic() -> LeftMenuItem {
//
//        let TLSACH = dataManager.lstTheLoaiSach //ECO
//        let TLCON = dataManager.lstTheLoaiCon
//
//        let result = LeftMenuItem.init(itemName: "\(TLSACH[1].MaTLS)".localized(), viewcontroller: nil)
//
//        var chils = [LeftMenuItem]()
//
//        let item1 = LeftMenuItem.init(itemName: "\(TLCON[3].MaTLC)".localized(), viewcontroller: SearchBookViewController.self)
//        let item2 = LeftMenuItem.init(itemName: "\(TLCON[4].MaTLC)".localized(), viewcontroller: SearchBookViewController.self)
//        let item3 = LeftMenuItem.init(itemName: "\(TLCON[5].MaTLC)".localized(), viewcontroller: SearchBookViewController.self)
//        let item4 = LeftMenuItem.init(itemName: "\(TLCON[6].MaTLC)".localized(), viewcontroller: SearchBookViewController.self)
//        let item5 = LeftMenuItem.init(itemName: "\(TLCON[7].MaTLC)".localized(), viewcontroller: SearchBookViewController.self)
//        let item6 = LeftMenuItem.init(itemName: "\(TLCON[8].MaTLC)".localized(), viewcontroller: SearchBookViewController.self)
//        let item7 = LeftMenuItem.init(itemName: "\(TLCON[9].MaTLC)".localized(), viewcontroller: SearchBookViewController.self)
//        let item8 = LeftMenuItem.init(itemName: "\(TLCON[10].MaTLC)".localized(), viewcontroller: SearchBookViewController.self)
//
//        chils.append(item1)
//        chils.append(item2)
//        chils.append(item3)
//        chils.append(item4)
//        chils.append(item5)
//        chils.append(item6)
//        chils.append(item7)
//        chils.append(item8)
//
//        result.add(chils)
//
//        return result
//
//    }
//
//    class func getFrLiteature() -> LeftMenuItem {
//
//        let TLSACH = dataManager.lstTheLoaiSach //FL
//        let TLCON = dataManager.lstTheLoaiCon
//
//        let result = LeftMenuItem.init(itemName: "\(TLSACH[2].MaTLS)".localized(), viewcontroller: nil)
//
//        var chils = [LeftMenuItem]()
//
//        let item1 = LeftMenuItem.init(itemName: "\(TLCON[11].MaTLC)".localized(), viewcontroller: SearchBookViewController.self)
//        let item2 = LeftMenuItem.init(itemName: "\(TLCON[12].MaTLC)".localized(), viewcontroller: SearchBookViewController.self)
//        let item3 = LeftMenuItem.init(itemName: "\(TLCON[13].MaTLC)".localized(), viewcontroller: SearchBookViewController.self)
//
//        chils.append(item1)
//        chils.append(item2)
//        chils.append(item3)
//
//        result.add(chils)
//
//        return result
//
//    }
//
//    class func getKnowledge() -> LeftMenuItem {
//
//        let TLSACH = dataManager.lstTheLoaiSach //GK
//        let TLCON = dataManager.lstTheLoaiCon
//
//        let result = LeftMenuItem.init(itemName: "\(TLSACH[3].MaTLS)".localized(), viewcontroller: nil)
//
//        var chils = [LeftMenuItem]()
//
//        let item1 = LeftMenuItem.init(itemName: "\(TLCON[14].MaTLC)".localized(), viewcontroller: SearchBookViewController.self)
//        let item2 = LeftMenuItem.init(itemName: "\(TLCON[15].MaTLC)".localized(), viewcontroller: SearchBookViewController.self)
//        let item3 = LeftMenuItem.init(itemName: "\(TLCON[16].MaTLC)".localized(), viewcontroller: SearchBookViewController.self)
//        let item4 = LeftMenuItem.init(itemName: "\(TLCON[17].MaTLC)".localized(), viewcontroller: SearchBookViewController.self)
//        let item5 = LeftMenuItem.init(itemName: "\(TLCON[18].MaTLC)".localized(), viewcontroller: SearchBookViewController.self)
//        let item6 = LeftMenuItem.init(itemName: "\(TLCON[19].MaTLC)".localized(), viewcontroller: SearchBookViewController.self)
//
//        chils.append(item1)
//        chils.append(item2)
//        chils.append(item3)
//        chils.append(item4)
//        chils.append(item5)
//        chils.append(item6)
//
//        result.add(chils)
//
//        return result
//
//    }
//
//    class func getHistory() -> LeftMenuItem {
//
//        let TLSACH = dataManager.lstTheLoaiSach //HB
//        let TLCON = dataManager.lstTheLoaiCon
//
//        let result = LeftMenuItem.init(itemName: "\(TLSACH[4].MaTLS)".localized(), viewcontroller: nil)
//
//        var chils = [LeftMenuItem]()
//
//        let item1 = LeftMenuItem.init(itemName: "\(TLCON[20].MaTLC)".localized(), viewcontroller: SearchBookViewController.self)
//        let item2 = LeftMenuItem.init(itemName: "\(TLCON[21].MaTLC)".localized(), viewcontroller: SearchBookViewController.self)
//
//        chils.append(item1)
//        chils.append(item2)
//
//        result.add(chils)
//
//        return result
//
//    }
//
//    class func getRiseChild() -> LeftMenuItem {
//
//        let TLSACH = dataManager.lstTheLoaiSach //RUC
//        let TLCON = dataManager.lstTheLoaiCon
//
//        let result = LeftMenuItem.init(itemName: "\(TLSACH[5].MaTLS)".localized(), viewcontroller: nil)
//
//        var chils = [LeftMenuItem]()
//
//        let item1 = LeftMenuItem.init(itemName: "\(TLCON[22].MaTLC)".localized(), viewcontroller: SearchBookViewController.self)
//        let item2 = LeftMenuItem.init(itemName: "\(TLCON[23].MaTLC)".localized(), viewcontroller: SearchBookViewController.self)
//        let item3 = LeftMenuItem.init(itemName: "\(TLCON[24].MaTLC)".localized(), viewcontroller: SearchBookViewController.self)
//
//        chils.append(item1)
//        chils.append(item2)
//        chils.append(item3)
//
//        result.add(chils)
//
//        return result
//
//    }
//
//    class func getBeautifullLife() -> LeftMenuItem {
//
//        let TLSACH = dataManager.lstTheLoaiSach //SKB
//        let TLCON = dataManager.lstTheLoaiCon
//
//        let result = LeftMenuItem.init(itemName: "\(TLSACH[6].MaTLS)".localized(), viewcontroller: nil)
//
//        var chils = [LeftMenuItem]()
//
//        let item1 = LeftMenuItem.init(itemName: "\(TLCON[25].MaTLC)".localized(), viewcontroller: SearchBookViewController.self)
//        let item2 = LeftMenuItem.init(itemName: "\(TLCON[26].MaTLC)".localized(), viewcontroller: SearchBookViewController.self)
//
//        chils.append(item1)
//        chils.append(item2)
//
//        result.add(chils)
//
//        return result
//
//    }
//
//    class func getVNLiteature() -> LeftMenuItem {
//
//        let TLSACH = dataManager.lstTheLoaiSach //VNLI
//        let TLCON = dataManager.lstTheLoaiCon
//
//        let result = LeftMenuItem.init(itemName: "\(TLSACH[7].MaTLS)".localized(), viewcontroller: nil)
//
//        var chils = [LeftMenuItem]()
//
//        let item1 = LeftMenuItem.init(itemName: "\(TLCON[27].MaTLC)".localized(), viewcontroller: SearchBookViewController.self)
//        let item2 = LeftMenuItem.init(itemName: "\(TLCON[28].MaTLC)".localized(), viewcontroller: SearchBookViewController.self)
//        let item3 = LeftMenuItem.init(itemName: "\(TLCON[29].MaTLC)".localized(), viewcontroller: SearchBookViewController.self)
//        let item4 = LeftMenuItem.init(itemName: "\(TLCON[30].MaTLC)".localized(), viewcontroller: SearchBookViewController.self)
//        let item5 = LeftMenuItem.init(itemName: "\(TLCON[31].MaTLC)".localized(), viewcontroller: SearchBookViewController.self)
//
//        chils.append(item1)
//        chils.append(item2)
//        chils.append(item3)
//        chils.append(item4)
//        chils.append(item5)
//
//        result.add(chils)
//
//        return result
//
//    }
//
//
//}
