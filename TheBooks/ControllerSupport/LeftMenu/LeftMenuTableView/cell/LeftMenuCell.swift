//
//  LeftMenuCell.swift
//  TheBooks
//
//  Created by Mojave on 7/19/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import UIKit

class LeftMenuCell: UITableViewCell {

    @IBOutlet weak var lblTitle : SkyLabel?
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setData(_ theloai : TheLoaiCon_DTO) {
        lblTitle?.title = "\(theloai.MaTLC)".localized()
    }
    
    
}
