//
//  LefMenuHeader.swift
//  TheBooks
//
//  Created by Mojave on 7/23/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import UIKit

protocol LefMenuHeaderDelegate {
    func showDropDown(_ section : Int)
}

class LefMenuHeader: SkyView {

    @IBOutlet weak var lblTitle : SkyLabel?
    @IBOutlet weak var imgImage : UIImageView?
    
    var delegate : LefMenuHeaderDelegate?
    private var myRow : Int = -1
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }


    func setData(_ section : Int, _ data : TheLoai_DTO, _  string : String) {
        myRow = section
        imgImage?.image = colorTemplate.getImageDarkLight(string)
        lblTitle?.title = "\(data.MaTLS)".localized()
        
        if(data.isSelected) {
            imgImage?.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi)) //lật hình
        }
    }
    
    
    @IBAction func showDropDownTouch (_ sender : UIButton) {
        self.delegate?.showDropDown(myRow)
    }

}
