//
//  LeftMenuViewController.swift
//  TheBooks
//
//  Created by Mojave on 7/19/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import UIKit

class LeftMenuViewController: MasterViewController {

    
    //MARK: - IBOULET
    
    @IBOutlet weak var btnSearch : UIButton!
    @IBOutlet weak var btnLanguage : UIButton!
    @IBOutlet weak var btnDarkLight : UIButton!
    @IBOutlet weak var btnLogout : UIButton!
    @IBOutlet weak var tfSearch : SkyTextFiled!
    @IBOutlet weak var tbvView : UITableView!
    
    //MARK: - DECLARE

    var lstData : [TheLoai_DTO] = []
    var temp : [TheLoai_DTO] = []
    
    //MARK: - INIT
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        darkLightMode()
        changeLanguage()
        setUpView()
        register()
        //notification.post(.RemoveFloatingMenu)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    //MARK: - DARK/LIGHT + CHANGLANGUAGE CHANGE
    
    override func darkLightMode() {
        super.darkLightMode()
        
        setDarkLightIcon()
        tbvView.reloadData()
        
    }
    
    override func changeLanguage() {
        super.changeLanguage()
        setLanguageIcon()
        
        setUpView()
        tbvView.reloadData()
    }
    
    //MARK: - REGISTER
    
    struct Indentifier {
        static let cell = "LeftMenuCell"
    }
    
    func register() {
        tbvView.register(UINib.init(nibName: Indentifier.cell, bundle: ProjectBundle), forCellReuseIdentifier: Indentifier.cell)
        tbvView.delegate = self
        tbvView.dataSource = self
        
        tbvView.separatorStyle = .none
    }
    
    //MARK: - FUNC
    
    func setDarkLightIcon() {
        btnSearch.setImage(colorTemplate.getImageDarkLight("search"), for: .normal)
        btnDarkLight.setImage(colorTemplate.getImageDarkLight("template"), for: .normal)
    }
    
    func setLanguageIcon() {
        let iconLang = configLanguage.getLangType().getLangIconName()
        btnLanguage.setImage(iconLang.image(), for: .normal)
        
        tfSearch.setPlacehoder("SearchType".localized())
    }
    
    func setUpView() {
        temp = dataManager.lstTheLoai
        lstData = temp
    }
    
    private func reloadTableView() {
        self.tbvView.reloadData()
    }
    
    private func searchAction() {
        
        guard !(tfSearch.text == "") else {
            lstData = temp
            reloadTableView()
            return
        }
        
        lstData = temp.filter({ (item) -> Bool in
            item.TheLoaiCon[0].TenTLC.uppercased().contains(tfSearch.text!.uppercased())
        })
        
        reloadTableView()
        return
    }
    
    //MARK: - IBACTION
    
    
    @IBAction func textFieldEditChanged(_ sender: UITextField) {
        searchAction()
    }
    
    @IBAction func searchTouch(_ sender : UIButton) {
        searchAction()
    }
    
    @IBAction func logoutTouch(_ sender : UIButton) {
        self.view.confirmDialog(self.view, "XÁC NHẬN","Bạn có muốn đăng xuất không ?", [("HUỶ", "ĐỒNG Ý")]) { (index) in
            if(index == 1) {
                notification.remove(self, .LoginSucess)
                notification.post(.LogoutSucess)
            }
        }
    }
    
    @IBAction func changeLanguageTouch(_ sender : UIButton) {
        let vc = LanguageView()
        PopTip.openPopTipView("", message: "", contentSize: CGSize(width: 180, height: 100), myView: vc, targetView: btnLanguage, containerView: self.view)
    }
    
    @IBAction func darkLightTouch(_ sender : UIButton) {
        colorTemplate.isDarkMode = !colorTemplate.isDarkMode
    }
    
    //MARK: - DEINIT
    
    deinit {
        
    }

}


extension LeftMenuViewController : UITableViewDelegate {

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = SearchBookViewController.init("SearchBookViewController")
        vc.tfText = lstData[indexPath.section].TheLoaiCon[indexPath.row].TenTLC
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
    }
}

extension LeftMenuViewController : UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return lstData.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if(lstData[section].isSelected) {
            return lstData[section].TheLoaiCon.count
        }
        
        return 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Indentifier.cell, for: indexPath) as! LeftMenuCell
        let childs = lstData[indexPath.section].TheLoaiCon
        cell.setData(childs[indexPath.row])
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = LefMenuHeader()
        header.delegate = self
        header.setData(section, lstData[section], "dropdown")
        return header
    }
}

extension LeftMenuViewController : LefMenuHeaderDelegate {
    func showDropDown(_ section: Int) {
        for index in 0 ..< lstData.count {
            if(index == section) {
                let currentSeclected = self.lstData[section].isSelected
                self.lstData[section].isSelected = !currentSeclected
            } else {
                self.lstData[index].isSelected = false
            }
        }
        
        self.tbvView.reloadData()
    }
    

}


