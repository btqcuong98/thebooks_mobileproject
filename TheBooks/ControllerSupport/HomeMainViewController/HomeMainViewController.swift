//
//  HomeMainViewController.swift
//  TheBooks
//
//  Created by Mojave on 8/9/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import UIKit

class HomeMainViewController: MasterViewController {

    //MARK: - DECLARE
    
    var leftVC = LeftMenuViewController()
    var leftMenuWidth : CGFloat = 0
    var wasShowLeftMenu : Bool = false
    let vc = TabbarViewController.init(nibName: String(describing: TabbarViewController.self), bundle: nil)
    
    var tabbar = UITabBarController()
    
    //MARK: - INIT
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpLeftView()
        notification.add(self, #selector(processLeftMenu), .LeftMenuShow)
        self.view.successDialog(self.view, "Đăng nhập thành công !") { (_) in}
    }

    //MARK: - LEFT SIDE MENU
    
    private func setUpLeftView() {
        //add rootvc
        
        self.view.addSubview(vc.view)
        self.view.setFullLayout(vc.view)
        self.addChild(vc)
        
        //set frame left menu
        
        self.view.addSubview(leftVC.view)
        leftMenuWidth = UIScreen.main.bounds.width * 0.75
        var leftMenuFrame = UIScreen.main.bounds
        leftMenuFrame.size.width = leftMenuWidth
        leftVC.view.frame = leftMenuFrame
        self.addChild(leftVC)
        
        //set position
        setPositionLeftMenu(false)
    }
    
    private func setPositionLeftMenu(_ flag : Bool) {
        if(flag) {
            leftVC.view.frame.origin.x = 0
            vc.view.frame.origin.x = leftMenuWidth
            vc.view.alpha = 0.5
            //tap
            let tap = UITapGestureRecognizer.init(target: self, action: #selector(processLeftMenu))
            vc.view.addGestureRecognizer(tap)
        } else {
            leftVC.view.frame.origin.x = -(UIScreen.main.bounds.width)
            vc.view.frame.origin.x = 0
            vc.view.alpha = 1
            
            //remove tap
            if let recognizers = vc.view.gestureRecognizers {
                for recognizer in recognizers {
                    vc.view.removeGestureRecognizer(recognizer)
                }
            }
        }
    }
    
    @objc func processLeftMenu() {
        showLeftMenu(!wasShowLeftMenu)
    }
    
    private func showLeftMenu(_ flag : Bool) {
        wasShowLeftMenu = flag
        
        if(flag) {
            setPositionLeftMenu(true)
        } else {
            disableLeftView()
        }
    }
    
    @objc func disableLeftView() {
        self.setPositionLeftMenu(false)
    }
}
