//
//  MasterViewController.swift
//  TheBooks
//
//  Created by Mojave on 7/5/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import UIKit

class MasterViewController: UIViewController {

    //MARK: - OUTLET
    
    @IBOutlet weak var simpleNavigationView : SimpleNavigationView?
    @IBOutlet weak var imgBackground : UIImageView?
    
    //MARK: - LOAD
    
    override func viewDidLoad() {
        super.viewDidLoad()

        addNotification()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.navigationBar.hiddenView() //navi của hệ thống
    }
    
    override func viewDidAppear(_ animated: Bool) {
        print("current controller : \(type(of: self))")
        print("current xib: \(self.nibName ?? "don't know")")
    }
    
    //MARK: - NAVIGATION FUNC
    
    func getSimpleNaviTitle() -> String{
        return simpleNavigationView?.title ?? ""
    }
    
    func setSimpleNaviTitle(_ title : Any) {
        simpleNavigationView?.showNavi()
        simpleNavigationView?.setNavi(.none, title) {}
    }
    
    func setActionBackWithTile(_ title : Any) {
        simpleNavigationView?.showNavi()
        weak var weakself = self
        simpleNavigationView?.setNavi(.back, title) {
            weakself?.actionBackNavigation()
        }
    }
    
    func setActionMenuWithTile(_ title : Any) {
        simpleNavigationView?.showNavi()
        simpleNavigationView?.setNavi(.menu, title) {
        }
    }
    
    //MARK: - DARK/LIGHT MODE + CHANGE LANGUAGE
    
    func addNotification() {
        notification.add(self, #selector(darkLightMode), .DarkModeChange)
        notification.add(self, #selector(changeLanguage), .LanguageChange)
    }
    
    @objc func darkLightMode() {
        imgBackground?.image = UIImage.init(named: colorTemplate.getBackgroundDarkLight())
    }
    
    @objc func changeLanguage() {
    }
    
    //MARK: - DEINIT
    
    deinit {
    }
    
    

}
