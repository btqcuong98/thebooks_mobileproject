//
//  Sach_Model.swift
//  TheBooks
//
//  Created by Mojave on 8/10/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import Foundation

class Sach_DTO: MI {
    @objc dynamic var MaS = 0
    @objc dynamic var Id_CtNhap = 0
    @objc dynamic var ViTri = ""
    
    class func list(value : [NSDictionary]) -> [Sach_DTO] {
        var result : [Sach_DTO] = []
        for item in value {
            result.append(Sach_DTO.init(dictionary: item))
        }
        return result
    }
}
