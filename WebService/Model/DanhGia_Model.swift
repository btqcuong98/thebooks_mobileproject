//
//  DanhGia_Model.swift
//  TheBooks
//
//  Created by Mojave on 11/22/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import Foundation

class DanhGia_Request: MI {
    @objc dynamic var MaDG = ""
    @objc dynamic var MaDS = ""
    @objc dynamic var BinhLuan = ""
    @objc dynamic var DanhGia = 0
}

class DanhGia_DTO: MI {
    @objc dynamic var MaDG = ""
    @objc dynamic var HoTen = ""
    @objc dynamic var MaDS = ""
    @objc dynamic var Ngay = ""
    @objc dynamic var BinhLuan = ""
    @objc dynamic var DanhGia = 0
    @objc dynamic var MaxDanhGia = 0
    @objc dynamic var CountMaxDanhGia = 0
    
    class func list(value : [NSDictionary]) -> [DanhGia_DTO] {
        var result : [DanhGia_DTO] = []
        for item in value {
            let dto = DanhGia_DTO()
            
            if let response = item["MaDG"] {
                dto.MaDG = response as? String ?? ""
            }
            
            if let response = item["HoTen"] {
                dto.HoTen = response as? String ?? ""
            }
            
            if let response = item["MaDS"] {
                dto.MaDS = response as? String ?? ""
            }
            
            if let response = item["Ngay"] {
                dto.Ngay = response as? String ?? ""
            }
            
            if let response = item["BinhLuan"] {
                dto.BinhLuan = response as? String ?? ""
            }
            
            if let response = item["DanhGia"] {
                dto.DanhGia = response as? Int ?? 0
            }
            
            if let response = item["MaxDanhGia"] {
                dto.MaxDanhGia = response as? Int ?? 0
            }
            
            if let response = item["CountMaxDanhGia"] {
                dto.CountMaxDanhGia = response as? Int ?? 0
            }
            
            result.append(dto)
        }
        return result
    }
}
