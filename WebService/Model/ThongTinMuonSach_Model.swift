//
//  SachQuaHan_Model.swift
//  TheBooks
//
//  Created by Mojave on 8/20/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import Foundation


class ThongTinMuonSach_Request : MI {
    @objc dynamic var MaDG = ""
}

class ThongTinMuonSach_DTO: MI {
    
    @objc dynamic var MaS = 0
    @objc dynamic var YeuCauTra = 0
    @objc dynamic var MaDS = ""
    @objc dynamic var Ten = ""
    @objc dynamic var AnhBia = ""
    @objc dynamic var GiaSach = 0.0
    @objc dynamic var MaxRating = 0
    @objc dynamic var Rating = 0
    @objc dynamic var TacGia : [TacGia_DTO] = []
    
    class func list(value : [NSDictionary]) -> [ThongTinMuonSach_DTO] {
        var result : [ThongTinMuonSach_DTO] = []
        for item in value {
            
            let dto = ThongTinMuonSach_DTO()
            
            if let response = item["MaS"] {
                dto.MaS = response as? Int ?? 0
            }
            
            if let response = item["YeuCauTra"] {
                dto.YeuCauTra = response as? Int ?? 0
            }
            
            if let response = item["MaDS"] {
                dto.MaDS = response as? String ?? ""
            }
            
            if let response = item["Ten"] {
                dto.Ten = response as? String ?? ""
            }
            
            if let response = item["GiaSach"] {
                dto.GiaSach = response as? Double ?? 0.0
            }
            
            if let response = item["AnhBia"] {
                dto.AnhBia = response as? String ?? ""
            }
            
            if let response = item["MaxRating"] {
                dto.MaxRating = response as? Int ?? 0
            }
            
            if let response = item["Rating"] {
                dto.Rating = response as? Int ?? 0
            }
            
            if let response = item["TacGia"] as? [NSDictionary] {
                dto.TacGia = TacGia_DTO.list(value: response)
            }
            
            result.append(dto)
        }
        
        return result
    }
    
    
}

