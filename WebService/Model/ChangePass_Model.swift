//
//  ChangePass_Model.swift
//  TheBooks
//
//  Created by Mojave on 8/21/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import Foundation

class ChangePass_Request: MI {
    
    @objc dynamic var UserName = ""
    @objc dynamic var CurrentPass = ""
    @objc dynamic var NewPass = ""
    @objc dynamic var ConfirmPass = ""
}

class ChangePass_DTO: MI {
    
}

class ForgotPass_Request: MI {
    
    @objc dynamic var MaND = ""
    @objc dynamic var toEmail = ""
}

class ForgotPass_DTO: MI {
    
}
