//
//  FormatData.swift
//  TheBooks
//
//  Created by Mojave on 11/22/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import Foundation

let utils = Utils.sharedInstance()
class Utils: NSObject {
    
    static var instance: Utils!
    private let moneyNumberFormater: NumberFormatter = NumberFormatter()
    private let serverMoneyNumberFormater: NumberFormatter = NumberFormatter()
    
    @objc public class func sharedInstance() -> Utils {
        if(self.instance == nil) {
            self.instance = (self.instance ?? Utils())
            self.instance.initData()
        }
        return self.instance
    }
    
    //MARK: - KHỎI TAỌ
    
    func getGroupingSeparatorText() -> String {
        var result = ""
        if(configLanguage.getLangType() == .en) {
            result = ","
            
        } else if(configLanguage.getLangType() == .vi) {
            result = "."
        }
        return result
    }
    
    func getDecimalSeparatorText() -> String {
        var result = ""
        if(configLanguage.getLangType() == .en) {
            result = "."
            
        } else if(configLanguage.getLangType() == .vi) {
            result = ","
        }
        return result
    }
    
    @objc func changeLanguage() {
        moneyNumberFormater.decimalSeparator = self.getDecimalSeparatorText() // xử lý dấu phân cách - nếu phần ngàn: EN là "," và VI là "." - nếu phần thập phân thì EN là "." và VI là ","
        moneyNumberFormater.groupingSeparator = self.getGroupingSeparatorText()
    }
    
    let moneyFraction = 3
    private func initData() {
        serverMoneyNumberFormater.numberStyle = .decimal
        serverMoneyNumberFormater.minimumFractionDigits = 0 // numberFrac: min số lượng số sau dấu thập phân
        serverMoneyNumberFormater.maximumFractionDigits = 3 // numberFrac: max số lượng số sau dấu thập phân
        serverMoneyNumberFormater.decimalSeparator = "."
        serverMoneyNumberFormater.usesGroupingSeparator = false // không xử lý cho serverMoneyNumberFormater(chỉ trả dấu thập phân là .)
        
        moneyNumberFormater.zeroSymbol = "0"
        moneyNumberFormater.numberStyle = .decimal
        moneyNumberFormater.minimumFractionDigits = 0 // numberFrac: min số lượng số sau dấu thập phân
        moneyNumberFormater.maximumFractionDigits = moneyFraction // numberFrac: max số lượng số sau dấu thập phân
        moneyNumberFormater.roundingMode = .halfUp // làm tròn >= .05 thì lên .1, còn lại thì về .0
        
        self.changeLanguage()
        
        notification.add(self, #selector(changeLanguage), .LanguageChange)
    }
    
    //MARK: - FORMAT CURRENCY
    
    public func df2so(_ price: Double) -> String{
        let numberFormatter = NumberFormatter()
        numberFormatter.groupingSeparator = ","
        numberFormatter.groupingSize = 3
        numberFormatter.usesGroupingSeparator = true
        numberFormatter.decimalSeparator = "."
        numberFormatter.numberStyle = .decimal
        numberFormatter.maximumFractionDigits = 2
        return numberFormatter.string(from: price as NSNumber)!
    }
    
    //MARK: - FORMAT
    
    func convertStringToDate(_ value: String?) -> Date {
        
        let isoDate = value!
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"//
        let date = dateFormatter.date(from:isoDate)!
        print(date)
        return date
        
    }
    
    func convertDateToString(_ value: String?) -> String {
        if(value == nil || value!.count < 8) {
            return ""
        }
        
        var newValue = value
        if(value?.count == 15) {
            newValue = String(value!.prefix(8))
        }
        
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss" // input format
        let date: Date! = formatter.date(from: newValue!)
        
        if(configLanguage.isVietnamese()) { // tiếng việt
            formatter.dateFormat = "dd/MM/yyyy HH:mm" // output format
        } else {
            formatter.dateFormat = "MM/dd/yyyy HH:mm" // output format
        }
        
        return formatter.string(from: date)
    }
    
    //MARK: - DOUBLE
    
    func serverValueConvertStringToDouble(_ val: String?) -> Double {
        if(val == nil || val?.count == 0) {
            return 0
        }
        return serverMoneyNumberFormater.number(from: val!)?.doubleValue ?? 0
    }
    
    
    //MARK: - STRING
    
    func serverMoneyConvertDoubleToString(_ val: Double?) -> String {
        return processServerMoneyConvertDoubleToString(val)
    }
    
    private func processServerMoneyConvertDoubleToString(_ val: Double?) -> String {
        return val != nil ? (serverMoneyNumberFormater.string(from: NSNumber.init(value: val!)) ?? "0") : "0"
    }
    
    //MARK: - DEFINE CHARACTER
    
    //định nghĩa ký tự có dấu
    func defineCharacter(_ text: Character) -> String{
        switch text {
        case "A", "Á", "À", "Ả", "Ã", "Ạ", "Ă", "Ặ", "Ẳ", "Ẵ", "Ắ", "Ằ", "Â", "Ấ", "Ầ", "Ẩ", "Ẫ", "Ậ":
            return "A"
        case "D", "Đ":
            return "D"
        case "E", "É", "È", "Ẽ", "Ẻ", "Ẹ", "Ê", "Ế", "Ề", "Ể", "Ệ", "Ễ":
            return "E"
        case "I", "Í", "Ì", "Ị", "Ỉ", "Ĩ":
            return "I"
        case "O", "Ó", "Ò", "Ỏ", "Õ", "Ọ", "Ô", "Ố", "Ồ", "Ổ", "Ỗ", "Ộ", "Ơ", "Ớ", "Ờ", "Ở", "Ỡ", "Ợ":
            return "O"
        case "U", "Ù", "Ú", "Ụ", "Ủ", "Ũ", "Ư", "Ứ", "Ừ", "Ự", "Ử", "Ữ":
            return "U"
        default:
            return String(text)
        }
    }
    
    //xử lý thay thế ký tự có dấu thành ký tự không dấu
    func removeAccentText(_ text: String) -> String {
        var result = ""
        let standardText = text.standardizedString().uppercased()
        for i in 0 ..< standardText.count {
            result += defineCharacter(standardText[standardText.index(standardText.startIndex, offsetBy: i)])
        }
        return result
    }
}
