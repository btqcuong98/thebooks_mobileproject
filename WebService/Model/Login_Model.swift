//
//  LoginModel.swift
//  TheBooks
//
//  Created by Mojave on 8/3/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import Foundation
import UIKit

class Login_Request : MI {
    @objc dynamic var UserName = ""
    @objc dynamic var Password = ""
}


class Login_DTO : MI {
    @objc dynamic var MaND = ""
    @objc dynamic var HoTen = ""
    @objc dynamic var DiaChi = ""
    @objc dynamic var NgaySinh = ""
    @objc dynamic var Email = ""
    @objc dynamic var Sdt = ""
    @objc dynamic var AnhDaiDien = ""
    @objc dynamic var MaNQ = ""
    //@objc dynamic var logo: Data?
    
    class func list(value : [NSDictionary]) -> [Login_DTO] {
        var result : [Login_DTO] = []
        for item in value {
            result.append(Login_DTO.init(dictionary: item))
        }
        return result
    }
    
}
