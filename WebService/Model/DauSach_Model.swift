//
//  DauSach_Model.swift
//  TheBooks
//
//  Created by Mojave on 8/9/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import Foundation

class PhanCongNhanVien_Request : MI {
    @objc dynamic var MaNV = ""
}

class DauSach_Request : MI {
    
}

class LichSu_Request : MI {
    @objc dynamic var MaDG = ""
}

class DauSach_DTO: MI {
    
    @objc dynamic var MaDS = ""
    @objc dynamic var MaS = -1
    @objc dynamic var Id = -1 //id ctmuon
    @objc dynamic var DiaChiNhan = ""
    @objc dynamic var Ten = ""
    @objc dynamic var AnhBia = ""
    @objc dynamic var MoTa = ""
    @objc dynamic var SachMoi = 0
    @objc dynamic var GiaSach = 0.0
    @objc dynamic var MaxRating = 0
    @objc dynamic var Rating = 0
    @objc dynamic var TheLoaiCon : [TheLoaiCon_DTO] = []
    @objc dynamic var TheLoaiSach : [TheLoaiSach_DTO] = []
    @objc dynamic var TacGia : [TacGia_DTO] = []
    
    class func list(value : [NSDictionary]) -> [DauSach_DTO] {
        var result : [DauSach_DTO] = []
        for item in value {
            
            let dto = DauSach_DTO()
            
            if let response = item["MaDS"] {
                dto.MaDS = response as? String ?? ""
            }
            
            if let response = item["Id"] {
                dto.Id = response as? Int ?? 0
            }
            
            if let response = item["MaS"] {
                dto.MaS = response as? Int ?? 0
            }
            
            if let response = item["DiaChiNhan"] {
                dto.DiaChiNhan = response as? String ?? ""
            }
            
            if let response = item["Ten"] {
                dto.Ten = response as? String ?? ""
            }
            
            if let response = item["AnhBia"] {
                dto.AnhBia = response as? String ?? "empty_image"
            }
            
            if let response = item["MoTa"] {
                dto.MoTa = response as? String ?? ""
            }
            
            if let response = item["SachMoi"] {
                dto.SachMoi = response as? Int ?? 0
            }
            
            if let response = item["GiaSach"] {
                dto.GiaSach = response as? Double ?? 0.0
            }
            
            if let response = item["MaxRating"] {
                dto.MaxRating = response as? Int ?? 0
            }
            
            if let response = item["Rating"] {
                dto.Rating = response as? Int ?? 0
            }
            
            if let response = item["TheLoaiCon"] as? [NSDictionary] {
                dto.TheLoaiCon = TheLoaiCon_DTO.list(value: response)
            }
            
            if let response = item["TheLoaiSach"] as? [NSDictionary] {
                dto.TheLoaiSach = TheLoaiSach_DTO.list(value: response)
            }
            
            if let response = item["TacGia"] as? [NSDictionary] {
                dto.TacGia = TacGia_DTO.list(value: response)
            }
            
            result.append(dto)
        }
        
        return result
    }
    
    
}

class Basket: NSObject, NSCoding {
    
    var MaDS = ""
    var Ten = ""
    var AnhBia = ""
    var MoTa = ""
    var GiaSach = 0.0
    var MaxRating = 0
    var Rating = 0
    var TacGia = ""
    
    init(_ MaDS : String, _ Ten : String, _ AnhBia : String, _ MoTa : String, _ GiaSach : Double, _ MaxRating : Int, _ Rating : Int, _ TacGia : [TacGia_DTO]) {
        
        self.MaDS = MaDS
        self.Ten = Ten
        self.AnhBia = AnhBia
        self.MoTa = MoTa
        self.GiaSach = GiaSach
        self.MaxRating = MaxRating
        self.Rating = Rating
        
        switch TacGia.count {
        case 0:
            self.TacGia = "Chưa rõ Tác Giả"
        case 1:
            self.TacGia = TacGia[0].Ten
        case 2:
            self.TacGia = "\(TacGia[0].Ten) + \(TacGia[1].Ten)"
        default:
            self.TacGia = "Nhiều Tác Giả"
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        self.MaDS = aDecoder.decodeObject(forKey: "MaDS") as! String
        self.Ten = aDecoder.decodeObject(forKey: "Ten") as! String
        self.AnhBia = aDecoder.decodeObject(forKey: "AnhBia") as? String ?? ""
        self.MoTa = aDecoder.decodeObject(forKey: "MoTa") as? String ?? ""
        self.GiaSach = aDecoder.decodeObject(forKey: "GiaSach") as? Double ?? 50000
        self.MaxRating = aDecoder.decodeObject(forKey: "MaxRating") as? Int ?? 4
        self.Rating = aDecoder.decodeObject(forKey: "Rating") as? Int ?? 5
        self.TacGia = aDecoder.decodeObject(forKey: "TacGia") as? String ?? ""
        
    }
    
    func initWithCoder(aDecoder: NSCoder) -> Basket {
        self.MaDS = aDecoder.decodeObject(forKey: "MaDS") as! String
        self.Ten = aDecoder.decodeObject(forKey: "Ten") as! String
        self.AnhBia = aDecoder.decodeObject(forKey: "AnhBia") as? String ?? ""
        self.MoTa = aDecoder.decodeObject(forKey: "MoTa") as? String ?? ""
        self.GiaSach = aDecoder.decodeObject(forKey: "GiaSach") as? Double ?? 50000
        self.MaxRating = aDecoder.decodeObject(forKey: "MaxRating") as? Int ?? 4
        self.Rating = aDecoder.decodeObject(forKey: "Rating") as? Int ?? 5
        self.TacGia = aDecoder.decodeObject(forKey: "TacGia") as? String ?? ""
        
        return self
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(MaDS, forKey: "MaDS")
        aCoder.encode(Ten, forKey: "Ten")
        aCoder.encode(AnhBia, forKey: "AnhBia")
        aCoder.encode(MoTa, forKey: "MoTa")
        aCoder.encode(GiaSach, forKey: "GiaSach")
        aCoder.encode(MaxRating, forKey: "MaxRating")
        aCoder.encode(Rating, forKey: "Rating")
        aCoder.encode(TacGia, forKey: "TacGia")
    }
}
