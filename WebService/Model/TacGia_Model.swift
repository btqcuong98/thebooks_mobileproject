//
//  TacGia_Model.swift
//  TheBooks
//
//  Created by Mojave on 8/10/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import Foundation

class TacGia_DTO : MI {
    @objc dynamic var MaTG = ""
    @objc dynamic var Ten = ""
    
    class func list(value : [NSDictionary]) -> [TacGia_DTO] {
        var result : [TacGia_DTO] = []
        for item in value {
            result.append(TacGia_DTO.init(dictionary: item))
        }
        return result
    }
}
