//
//  TheLoaiSach_Model.swift
//  TheBooks
//
//  Created by Mojave on 8/5/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import Foundation
import UIKit

class TheLoaiSach_Request : MI {
}

class TheLoaiSach_DTO : MI {
    @objc dynamic var MaTLS = ""
    @objc dynamic var TenTLS = ""
    
    class func list(value : [NSDictionary]) -> [TheLoaiSach_DTO] {
        var result : [TheLoaiSach_DTO] = []
        for item in value {
            result.append(TheLoaiSach_DTO.init(dictionary: item))
        }
        return result
    }
    
}
