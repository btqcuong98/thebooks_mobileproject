//
//  TheLoai_Model.swift
//  TheBooks
//
//  Created by Mojave on 8/10/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import Foundation

class TheLoai_Request : MI {
    
}

class TheLoai_DTO : MI {
    @objc dynamic var MaTLS = ""
    @objc dynamic var TenTLS = ""
    @objc dynamic var TheLoaiCon : [TheLoaiCon_DTO] = []
    
    //local
    @objc dynamic var isSelected = false
    
    
    class func list(value : [NSDictionary]) -> [TheLoai_DTO] {
        var result : [TheLoai_DTO] = []
        
        for item in value {
            
            let dto = TheLoai_DTO()
            
            if let response = item["MaTLS"] {
                dto.MaTLS = response as? String ?? ""
            }
            
            if let response = item["TenTLS"] {
                dto.TenTLS = response as? String ?? ""
            }
            
            if let response = item["TheLoaiCon"] as? [NSDictionary]{
                dto.TheLoaiCon = TheLoaiCon_DTO.list(value: response)
            }
            
            result.append(dto)
        }
        
        return result
    }
}
