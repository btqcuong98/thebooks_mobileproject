//
//  APIResponse.swift
//  TheBooks
//
//  Created by Mojave on 8/3/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import Foundation

class APIResponse: MI {
    @objc dynamic var code = 200
    @objc dynamic var message = "success"
    @objc dynamic var data : Any?
}
