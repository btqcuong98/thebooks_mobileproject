//
//  MuonSach_Model.swift
//  TheBooks
//
//  Created by Mojave on 8/16/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import Foundation

//enum ResponseMuonSach : String {
//    case success = "sucess"
//    case SoLuongSachQuaTai = "SoLuongSachQuaTai"
//    case TonTaiSachQuaHan = "TonTaiSachQuaHan"
//    case HetThoiGianMuon = "HetThoiGianMuon"
//    case HetSach = "HetSach"
//    case xx = "xx"
//
//    func getLangString() -> String {
//        switch self {
//        case .success:
//            return "sucess"
//        case .SoLuongSachQuaTai:
//            return "SoLuongSachQuaTai"
//        case .TonTaiSachQuaHan:
//            return "TonTaiSachQuaHan"
//        case .HetThoiGianMuon:
//            return "HetThoiGianMuon"
//        case .HetSach:
//            return "HetSach"
//        case .xx:
//            return "xx"
//        }
//    }
//
//    static func getMuonSachType(_ string : String) -> ResponseMuonSach {
//        switch string {
//        case ResponseMuonSach.success.getLangString():
//            return .success
//        case ResponseMuonSach.SoLuongSachQuaTai.getLangString():
//            return .SoLuongSachQuaTai
//        case ResponseMuonSach.TonTaiSachQuaHan.getLangString():
//            return .TonTaiSachQuaHan
//        case ResponseMuonSach.HetThoiGianMuon.getLangString():
//            return .HetThoiGianMuon
//        case ResponseMuonSach.HetSach.getLangString() :
//            return .HetSach
//        default:
//            return .xx
//        }
//    }
//
//    func getMuonSachString() -> String {
//        switch self {
//        case .success:
//            return "Giao dịch thành công !"
//        case .SoLuongSachQuaTai:
//            return "Số lượng sách bạn mượn đã vượt qúa giới hạn (3 cuốn sách) !"
//        case .TonTaiSachQuaHan:
//            return "Bạn phải trả những sách quá hạn trước khi tiếp tục mượn sách !"
//        case .HetThoiGianMuon:
//            return "Bạn vui lòng đóng lệ phí trước khi tiếp tục mượn sách !"
//        case .HetSach:
//            return "Xin lỗi sách bạn chọn đã hết !"
//        case .xx:
//            return "xx"
//        }
//    }
//}


class MuonSach_Request : MI {
    @objc dynamic var MaDS : [String] = []
    @objc dynamic var MaDG = ""
    @objc dynamic var DiaChi = ""
    @objc dynamic var Sdt = ""
    @objc dynamic var GhiChu = ""
    @objc dynamic var Ten = ""
    @objc dynamic var SoSachMuon = 1
}

class MuonSach_DTO : MI {
}
