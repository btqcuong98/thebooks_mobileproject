//
//  TheLoaiCon_Model.swift
//  TheBooks
//
//  Created by Mojave on 8/6/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import Foundation

class TheLoaiCon_Request : MI {
}

class TheLoaiCon_DTO : MI {
    @objc dynamic var MaTLC = ""
    @objc dynamic var TenTLC = ""
    @objc dynamic var MaTLS = ""
    
    class func list(value : [NSDictionary]) -> [TheLoaiCon_DTO] {
        var result : [TheLoaiCon_DTO] = []
        for item in value {
            result.append(TheLoaiCon_DTO.init(dictionary: item))
        }
        return result
    }
    
}
