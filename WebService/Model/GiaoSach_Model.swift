//
//  GiaoSach_Model.swift
//  TheBooks
//
//  Created by Mojave on 8/24/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import Foundation

class GiaoSach_Request : MI {
    @objc dynamic var MaNV = ""
}

class GiaoSach_DTO: MI {
    
    @objc dynamic var MaPM = -1 //MaPM, MaS
    @objc dynamic var DiaChi = ""
    @objc dynamic var Ten = ""
    @objc dynamic var Sdt = ""
    @objc dynamic var DauSach : [DauSach_DTO] = []
    
    class func list(value : [NSDictionary]) -> [GiaoSach_DTO] {
        var result : [GiaoSach_DTO] = []
        for item in value {
            
            let dto = GiaoSach_DTO()
            
            if let response = item["MaPM"] {
                dto.MaPM = response as? Int ?? 0
            }
            
            if let response = item["DiaChi"] {
                dto.DiaChi = response as? String ?? ""
            }
            
            if let response = item["Ten"] {
                dto.Ten = response as? String ?? ""
            }
            
            if let response = item["Sdt"] {
                dto.Sdt = response as? String ?? ""
            }
            
            if let response = item["DauSach"] as? [NSDictionary] {
                dto.DauSach = DauSach_DTO.list(value: response)
            }
            
            result.append(dto)
        }
        
        return result
    }
    
    
}
