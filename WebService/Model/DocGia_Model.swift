//
//  DocGia_Model.swift
//  TheBooks
//
//  Created by Mojave on 8/19/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import Foundation

class Top10DocGia_Request : MI {}

class Top10DocGia_DTO : MI {
    @objc dynamic var MaDG = ""
    @objc dynamic var HoTen = ""
    @objc dynamic var Solanmuon = 0
    @objc dynamic var AnhDaiDien = ""
    
    class func list(value : [NSDictionary]) -> [Top10DocGia_DTO] {
        var result : [Top10DocGia_DTO] = []
        for item in value {
            result.append(Top10DocGia_DTO.init(dictionary: item))
        }
        return result
    }
}
