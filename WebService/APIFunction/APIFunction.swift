//
//  APIFunction.swift
//  TheBooks
//
//  Created by Mojave on 8/3/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import Foundation

enum APIFunction : String {
    case login_login = "/login/login"
    
    case theloaisach_laythongtin = "/theloaisach/laythongtin"
    case theloaicon_laythongtin = "/theloaicon/laythongtin"
    case theloai_laytheloaiObject = "/theloai/laytheloaiObject"
    
    case dausach_laydausachObject = "/dausach/laydausachObject"
    case dausach_selecttop10ds = "/dausach/selecttop10ds"
    
    case muonsach_muonsach = "/muonsach/muonsach"
    
    case docgia_capnhatanhdaidien = "/docgia/capnhatanhdaidien"
    case docgia_sachquahancuadocgia = "/docgia/sachquahancuadocgia"
    case docgia_sachdangmuoncuadocgia = "/docgia/sachdangmuoncuadocgia"
    case docgia_sachdangyeucaumuoncuadocgia = "/docgia/sachdangyeucaumuoncuadocgia"
    case docgia_yeucautrasach = "/docgia/yeucautrasach"
    case docgia_selecttop10dg = "/docgia/selecttop10dg"
    case docgia_lichsu = "/docgia/lichsu"
    
    case nhanvienvc_capnhatanhdaidien = "/nhanvienvc/capnhatanhdaidien"
//    case nhanvienvc_laydausachtrenphieugiaodetheodoi = "/nhanvienvc/laydausachtrenphieugiaodetheodoi"
//    case nhanvienvc_laydausachtrenphieuthudetheodoi = "/nhanvienvc/laydausachtrenphieuthudetheodoi"
    case nhanvienvc_capnhatkhigiaosachxong = "/nhanvienvc/capnhatkhigiaosachxong"
    case nhanvienvc_capnhatkhithusachxong = "/nhanvienvc/capnhatkhithusachxong"
    case nhanvienvc_laytatcaphieugiaoduavaomanv = "/nhanvienvc/laytatcaphieugiaoduavaomanv"
    case nhanvienvc_laytatcaphieuthuduavaomanv = "/nhanvienvc/laytatcaphieuthuduavaomanv"

    case taikhoan_quenmatkhau = "/nhanvientt/sendMail"
    case taikhoan_doimatkhau = "/taikhoan/doimatkhau"
    case danhgia_laytatcadanhgiacuadausach = "/danhgia/laytatcadanhgiacuadausach"
    case danhgia_themdanhgia = "/danhgia/themdanhgia"
    case danhgia_xoadanhgia = "/danhgia/xoadanhgia"
}
