//
//  TheLoaiSach_MiddleService.swift
//  TheBooks
//
//  Created by Mojave on 8/5/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import Foundation

extension ServiceRequest {
    class func theloaisach_getAll(request : TheLoaiSach_Request, success: @escaping(([TheLoaiSach_DTO])->Void)) {
        
        serviceRequest.getRequest(apiFunc: .theloaisach_laythongtin, parameter: request, success: { (response) in
            
            let dto = TheLoaiSach_DTO.list(value: response.data as! [NSDictionary])
            success(dto)
            
        }) { (failure) in
        }
        
    }
}
