//
//  TheLoai_MiddelService.swift
//  TheBooks
//
//  Created by Mojave on 8/10/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import Foundation

extension ServiceRequest {
    class func theloai_getAll(request : TheLoai_Request, success: @escaping(([TheLoai_DTO])->Void)) {
        
        serviceRequest.postRequest(apiFunc: .theloai_laytheloaiObject, parameter: request, success: { (response) in
            
            let dto = TheLoai_DTO.list(value: response.data as! [NSDictionary])
            success(dto)
            
        }) { (failure) in
        }
        
    }
}
