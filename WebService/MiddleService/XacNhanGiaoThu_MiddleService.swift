//
//  XacNhanGiaoThu_MiddleService.swift
//  TheBooks
//
//  Created by Mojave on 8/23/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import Foundation

extension ServiceRequest {
    class func xacnhangiaosach(request : XacNhanCuaNhanVien_Request, success : @escaping ((String)->Void)) {
        serviceRequest.postRequest(apiFunc: .nhanvienvc_capnhatkhigiaosachxong, parameter: request, success: { (response) in
            success(response.message)
        }) { (err) in
            print(err)
        }
    }
    
    
    class func xacnhanthusach(request : XacNhanCuaNhanVien_Request, success : @escaping ((String)->Void)) {
        serviceRequest.postRequest(apiFunc: .nhanvienvc_capnhatkhithusachxong, parameter: request, success: { (response) in
            success(response.message)
        }) { (err) in
            print(err)
        }
    }
}
