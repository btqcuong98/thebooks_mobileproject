//
//  NhanVien_MiddleService.swift
//  TheBooks
//
//  Created by Mojave on 8/19/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import Foundation

extension ServiceRequest {
    class func AnhDaiDienUpdate(request : AnhDaiDien_Request, success : @escaping ((String)->Void)){
        if(dataManager.roles == "DocGia") {
            serviceRequest.postRequest(apiFunc: .docgia_capnhatanhdaidien, parameter: request, success: { (response) in
                success(response.message)
                
            }) { (failure) in
                print(failure)
            }
        } else {
            
            serviceRequest.postRequest(apiFunc: .nhanvienvc_capnhatanhdaidien, parameter: request, success: { (response) in
                success(response.message)
                
            }) { (failure) in
                print(failure)
            }
        
        }
    }
}
