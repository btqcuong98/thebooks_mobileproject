//
//  Login.swift
//  TheBooks
//
//  Created by Mojave on 8/3/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import Foundation

extension ServiceRequest {
    class func login(request : Login_Request, sucess:@escaping(([Login_DTO])->Void)) {
        serviceRequest.postRequest(apiFunc: .login_login, parameter: request, success: { (response) in
            
            guard let responseData = response.data as? [NSDictionary] else {
                sucess([Login_DTO]())
                return
            }
            let dto = Login_DTO.list(value: responseData)
            sucess(dto)
            
        }) { (failure) in
        }
    }
}
