//
//  MuonSach_MiddleService.swift
//  TheBooks
//
//  Created by Mojave on 8/16/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import Foundation

extension ServiceRequest {
    class func muonsach(request : MuonSach_Request, success: @escaping((Int, String)->Void)) {

        serviceRequest.postRequest(apiFunc: .muonsach_muonsach, parameter: request, success: { (response) in
            success(response.code, response.message)
        }) { (failure) in
            print(failure)
        }

    }
}
