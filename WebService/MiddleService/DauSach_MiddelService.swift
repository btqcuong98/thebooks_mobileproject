//
//  DauSach_MiddelService.swift
//  TheBooks
//
//  Created by Mojave on 8/10/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import Foundation

extension ServiceRequest {
    class func dausach(request : DauSach_Request, success : @escaping (([DauSach_DTO])->Void)) {
        serviceRequest.postRequest(apiFunc: .dausach_laydausachObject, parameter: request, success: { (response) in
            let dto = DauSach_DTO.list(value: response.data as! [NSDictionary])
            success(dto)
        }) { (err) in
            print(err)
        }
    }
    
    class func selecttop10ds(request : DauSach_Request, success : @escaping (([DauSach_DTO])->Void)) {
        serviceRequest.postRequest(apiFunc: .dausach_selecttop10ds, parameter: request, success: { (response) in
            let dto = DauSach_DTO.list(value: response.data as! [NSDictionary])
            success(dto)
        }) { (err) in
            print(err)
        }
    }
}
