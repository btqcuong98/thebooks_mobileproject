//
//  YeuCauTraSach_MiddleService.swift
//  TheBooks
//
//  Created by Mojave on 8/23/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import Foundation
extension ServiceRequest {
    class func yeucautrasach(request : YeuCauTraSach_Request, sucess:@escaping((String)->Void)) {
        serviceRequest.postRequest(apiFunc: .docgia_yeucautrasach, parameter: request, success: { (response) in
            
            sucess(response.message)
            
        }) { (failure) in
            print(failure)
        }
    }
}
