//
//  DanhGia_MiddleService.swift
//  TheBooks
//
//  Created by Mojave on 11/22/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import Foundation

extension ServiceRequest {
    class func danhgia(request : DanhGia_Request, success : @escaping (([DanhGia_DTO])->Void)) {
        serviceRequest.postRequest(apiFunc: .danhgia_laytatcadanhgiacuadausach, parameter: request, success: { (response) in
            let dto = DanhGia_DTO.list(value: response.data as! [NSDictionary])
            success(dto)
        }) { (err) in
            print(err)
        }
    }
    
    class func themdanhgia(request : DanhGia_Request, success : @escaping (([DanhGia_DTO])->Void)) {
        serviceRequest.postRequest(apiFunc: .danhgia_themdanhgia, parameter: request, success: { (response) in
            let dto = DanhGia_DTO.list(value: response.data as! [NSDictionary])
            success(dto)
        }) { (err) in
            print(err)
        }
    }
    
    class func xoadanhgia(request : DanhGia_Request, success : @escaping (([DanhGia_DTO], Int)->Void)) {
        serviceRequest.postRequest(apiFunc: .danhgia_xoadanhgia, parameter: request, success: { (response) in
            let dto = DanhGia_DTO.list(value: response.data as! [NSDictionary])
            success(dto, response.code)
        }) { (err) in
            print(err)
        }
    }
}
