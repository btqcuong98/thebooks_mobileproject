//
//  DocGia_MiddleService.swift
//  TheBooks
//
//  Created by Mojave on 8/19/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import Foundation

extension ServiceRequest {
    class func top10dg(request : Top10DocGia_Request, success: @escaping(([Top10DocGia_DTO])->Void)) {
        
        serviceRequest.postRequest(apiFunc: .docgia_selecttop10dg, parameter: request, success: { (response) in
            let dto = Top10DocGia_DTO.list(value: response.data as! [NSDictionary])
            success(dto)
            
        }) { (failure) in
            print(failure)
        }
        
    }
    
    class func lichsu(request : LichSu_Request, success: @escaping(([DauSach_DTO])->Void)) {
        serviceRequest.postRequest(apiFunc: .docgia_lichsu, parameter: request, success: { (response) in
            let dto = DauSach_DTO.list(value: response.data as! [NSDictionary])
            success(dto)
        }) { (failure) in
            print(failure)
        }
        
    }
}
