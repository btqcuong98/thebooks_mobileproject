//
//  ChangePass_MiddleService.swift
//  TheBooks
//
//  Created by Mojave on 8/21/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import Foundation

extension ServiceRequest {
    class func changePass(request : ChangePass_Request, sucess:@escaping((String, Int)->Void)) {
        serviceRequest.postRequest(apiFunc: .taikhoan_doimatkhau, parameter: request, success: { (response) in
            
            sucess(response.message, response.code)
            
        }) { (failure) in
            print(failure)
        }
    }
    
    class func forgotPass(request : ForgotPass_Request, sucess:@escaping((String, Int)->Void)) {
        serviceRequest.postRequest(apiFunc: .taikhoan_quenmatkhau, parameter: request, success: { (response) in
            
            sucess(response.message, response.code)
            
        }) { (failure) in
            print(failure)
        }
    }
}


