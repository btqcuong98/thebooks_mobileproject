//
//  GiaoSach_MiddleService.swift
//  TheBooks
//
//  Created by Mojave on 8/24/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import Foundation

extension ServiceRequest {
    class func giaosach(request : GiaoSach_Request, success : @escaping (([GiaoSach_DTO])->Void)) {
        serviceRequest.postRequest(apiFunc: .nhanvienvc_laytatcaphieugiaoduavaomanv, parameter: request, success: { (response) in
            let dto = GiaoSach_DTO.list(value: response.data as! [NSDictionary])
            success(dto)
        }) { (err) in
            print(err)
        }
    }
    
    class func thusach(request : GiaoSach_Request, success : @escaping (([GiaoSach_DTO])->Void)) {
        serviceRequest.postRequest(apiFunc: .nhanvienvc_laytatcaphieuthuduavaomanv, parameter: request, success: { (response) in
            let dto = GiaoSach_DTO.list(value: response.data as! [NSDictionary])
            success(dto)
        }) { (err) in
            print(err)
        }
    }
}
