//
//  TheLoaiCon_MiddleService.swift
//  TheBooks
//
//  Created by Mojave on 8/6/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import Foundation


extension ServiceRequest {
    class func theloaicon_getAll(request : TheLoaiCon_Request, success: @escaping(([TheLoaiCon_DTO])->Void)) {
        
        serviceRequest.getRequest(apiFunc: .theloaicon_laythongtin, parameter: request, success: { (response) in
            
            let dto = TheLoaiCon_DTO.list(value: response.data as! [NSDictionary])
            success(dto)
            
        }) { (failure) in
        }
        
    }
}
