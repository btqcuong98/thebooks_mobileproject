//
//  SachQuaHan_MiddleService.swift
//  TheBooks
//
//  Created by Mojave on 8/20/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import Foundation

extension ServiceRequest {
    class func thongtinsachquahan(request : ThongTinMuonSach_Request, success : @escaping (([ThongTinMuonSach_DTO])->Void)) {
        serviceRequest.postRequest(apiFunc: .docgia_sachquahancuadocgia, parameter: request, success: { (response) in
            let dto = ThongTinMuonSach_DTO.list(value: response.data as! [NSDictionary])
                success(dto)
            
        }) { (err) in
            print(err)
        }
    }
    
    class func thongtinsachdangmuon(request : ThongTinMuonSach_Request, success : @escaping (([ThongTinMuonSach_DTO])->Void)) {
        serviceRequest.postRequest(apiFunc: .docgia_sachdangmuoncuadocgia, parameter: request, success: { (response) in
            let dto = ThongTinMuonSach_DTO.list(value: response.data as! [NSDictionary])
                success(dto)
        }) { (err) in
            print(err)
        }
    }
    
    class func thongtinsachchomuon(request : ThongTinMuonSach_Request, success : @escaping (([ThongTinMuonSach_DTO])->Void)) {
        serviceRequest.postRequest(apiFunc: .docgia_sachdangyeucaumuoncuadocgia, parameter: request, success: { (response) in
            let dto = ThongTinMuonSach_DTO.list(value: response.data as! [NSDictionary])
            success(dto)
        }) { (err) in
            print(err)
        }
    }
    
    
}
