//
//  ServiceRquest.swift
//  TheBooks
//
//  Created by Mojave on 8/3/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import Foundation
import Alamofire

let serviceRequest = ServiceRequest.shared
class ServiceRequest: NSObject {
    static let shared = ServiceRequest()
    
    func  postRequest(apiFunc : APIFunction, parameter : MI, success :@escaping ((APIResponse)->Void), failure:@escaping ((String)->Void)) {
        
        //prepare
        
        let tempHeaders : HTTPHeaders = [
            "Content-Type": "application/json",
            "Accept": "application/json;charset=utf-8",
            "Accept-Encoding": "gzip"
        ]
        
        let requestAPI = "http://thebtqcuong.online/quanlythuvien/public/api" + apiFunc.rawValue
        //let requestAPI = "http://localhost/quanlythuvien/public/api" + apiFunc.rawValue
        
        let parameters = processData.prepareParameter(parameter: parameter.dictionary())
        
        //test
        
        do {
            let data =  try JSONSerialization.data(withJSONObject: parameters, options: [])
            let json = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
            if let json = json {
                #if DEBUG
                print("====API : \(apiFunc.rawValue) \n===JSON : \(json) \n===END===")
                #endif
            }
        } catch {
        }
        
        //request
        
        Alamofire.request(requestAPI, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: tempHeaders).responseJSON {(response) in
            let processResponse = processData.processResponse(response: response)
            
            switch (response.result) {
            case .success(_) :
                success(processResponse)
                
            case .failure(let error):
                if let data = response.data {
                    print("Print Server Error: " + String(data: data, encoding: String.Encoding.utf8)!)
                    failure(String(data: data, encoding: String.Encoding.utf8)!)
                }
                print(error)
            }
        }
        
    }
    
    
    
    func getRequest(apiFunc : APIFunction, parameter : MI, success :@escaping ((APIResponse)->Void), failure:@escaping ((String)->Void)) {
        
        //prepare
        
        let tempHeaders : HTTPHeaders = [
            "Content-Type": "application/json",
            "Accept": "application/json;charset=utf-8",
            "Accept-Encoding": "gzip"
        ]
        
        let requestAPI = "http://thebtqcuong.online/quanlythuvien/public/api" + apiFunc.rawValue
        //let requestAPI = "http://localhost/quanlythuvien/public/api" + apiFunc.rawValue
        
        let parameters = processData.prepareParameter(parameter: parameter.dictionary())
        
        //test
        
        do {
            let data =  try JSONSerialization.data(withJSONObject: parameters, options: [])
            let json = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
            if let json = json {
                #if DEBUG
                //print("====API : \(apiFunc.rawValue) \n===JSON : \(json) \n===END===")
                #endif
            }
        } catch {
        }
        
        //request
        
        Alamofire.request(requestAPI, method: .get, parameters: parameters, headers: tempHeaders).responseJSON {(response) in
            let processResponse = processData.processResponse(response: response)
            
            switch (response.result) {
            case .success(_) :
                success(processResponse)
                
            case .failure(let error):
                if let data = response.data {
                    print("Print Server Error: " + String(data: data, encoding: String.Encoding.utf8)!)
                    failure(String(data: data, encoding: String.Encoding.utf8)!)
                }
                print(error)
            }
        }
        
    }
    
    
    
    
    
    
}
