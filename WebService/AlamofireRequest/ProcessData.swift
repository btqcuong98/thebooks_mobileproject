//
//  ProcessData.swift
//  TheBooks
//
//  Created by Mojave on 8/3/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import Foundation
import Alamofire

let processData = ProcessData.shared
class ProcessData: NSObject {
    static let shared = ProcessData()
    
    //MARK: - REQUEST
    
    func prepareParameter (parameter : Dictionary<String, Any>) -> Parameters{
        var endParameter : Parameters = [:]
        
        for (k,v) in parameter {
            endParameter[k] = v
        }
        
        return endParameter
    }
    
    //MARK: - RESPONSE
    
    func processResponse(response : DataResponse<Any>) -> APIResponse {
        if(response.result.isSuccess) {
            return APIResponse.init(dictionary: response.result.value as! NSDictionary)
        } else {
            return APIResponse.init()
        }
    }
}
